# Errata 

This is the list of fixed incorrect or misleading questions. (Please, report any incorrect questions.)

## 0.9.0

- in 2022, the name of the capital city of Kazakhstan was revereted from Nur-Sultan back to Astana
