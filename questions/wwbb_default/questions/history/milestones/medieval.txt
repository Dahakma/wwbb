Milestones of the Medieval Era

TAG history milestones medieval

EASY
Odoacer deposes the Roman Emperor Romulus Augustulus; The conventional end of the Roman Empire
§100§476§>450§<1550§@y

Hijra; Muhammad migrates from Mecca to Medina; The first year of the Islamic calendar
§140§622§>450§<1550§@y

William the Conqueror, Duke of Normandy, invades England and becomes King after the Battle of Hastings
§140§1066§>450§<1550§@y

Pope Urban initiates the First Crusade to liberate the Holy Land and repel the Seljuk Turks from the Byzantine Empire
§180§1095§>450§<1550§@y

Sack of Constantinople during the Fourth Crusade; The beginning of the decline of the Byzantine Empire
§180§1204§>450§<1550§@y

The Hundred Years' War between England and France begins
§160§1337§>450§<1550§@y

Constantinople falls to the Ottoman Turks; End of the Byzantine Empire
§100§1453§>450§<1550§@y

Christopher Columbus reaches the New World
§100§1492§>450§<1550§@y



MEDIUM 
Rome is sacked by Genseric, King of the Vandals
§80§455§>450§<1550§@y

Benedict of Nursia founds monastery at Monte Cassino; The beginning the Order of Saint Benedict
§200§529§>450§<1550§@y

Nika riots in Constantinople. Nearly half the city being burned or destroyed and tens of thousands of people killed
§180§532§>450§<1550§@y

Death of Muhammad
§50§632§>450§<1550§@y

Tariq ibn Ziyad invades Visigothic Kingdom; The beginning of the Muslim conquest of Spain
§100§711§>450§<1550§@y

Accession of Harun al-Rashid to the Caliphate in Baghdad
§120§786§>450§<1550§@y

Sack of Lindisfarne. Viking attacks on Britain begin
§120§793§>450§<1550§@y

Charlemagne is crowned Holy Roman Emperor
§100§800§>450§<1550§@y

Treaty of Verdun; Division of Charlemagne's Empire between his grandsons; Holy Roman Empire and France becoming separate realms
§120§843§>450§<1550§@y

Emperor Simeon I becomes ruler of the First Bulgarian Empire in the Balkans; Golden age of the First Bulgarian Empire; The Cyrillic alphabet is developed
§150§893§>450§<1550§@y

King Aethelstan the Glorious unites the heptarchy of The Anglo-Saxon nations of Wessex, Sussex, Essex, Kent, East Anglia, Mercia and Northumbria founding the Kingdom of England
§100§927§>450§<1550§@y

Volodymyr I the Great embraces Christianity; Christianization of Kyivan Rus'
§80§988§>450§<1550§@y

The Great Seljuk Empire is founded by Tughril Beg
§180§1037§>450§<1550§@y

The East-West Schism which divides the church into Western Catholicism and Eastern Orthodoxy
§80§1054§>450§<1550§@y

The compilation of the Domesday Book, a great land and property survey commissioned by William the Conqueror
§80§1086§>450§<1550§@y

First Crusade takes Jerusalem; Beginning of the Kingdom of Jerusalem
§120§1099§>450§<1550§@y

The Concordat of Worms was drawn up between Emperor Henry V and Pope Calixtus II ends the Investiture Controversy
§120§1122§>450§<1550§@y

The Anarchy begins in England; War of succession between Stephen of Blois and Empress Matilda
§150§1135§>450§<1550§@y

Genghis Khan elected as Khagan of the Mongols; The Mongol Empire established
§100§1206§>450§<1550§@y

The Magna Carta is sealed by John Lackland of England.
§80§1215§>450§<1550§@y

Kyiv sacked by Mongols; The End of Kyivan Rus'
§80§1240§>450§<1550§@y

Baghdad, the capital of the Abbasid Caliphate, captured by Mongols; The end of the Islamic Golden Age
§80§1258§>450§<1550§@y

The Ottoman Empire is founded by Osman I.
§150§1299§>450§<1550§@y

Battle of Kosovo in Serbia.
§160§1389§>450§<1550§@y

The Kalmar Union is formed; Queen Margaret I of Denmark unites the Denmark, Sweden, and Norway
§180§1397§>450§<1550§@y

Battle of Grunwald; Polish–Lithuanian forces defeat the Teutonic Order
§180§1410§>450§<1550§@y

Kingdom of Portugal conquers Ceuta; Beginning of the Portuguese Empire and the Age of Discovery
§100§1415§>450§<1550§@y

Battle of Agincourt; English king Henry V defeat French army
§100§1415§>450§<1550§@y

The Council of Constance ends; The Western Schism ends; Martin V electedas the sole pope
§120§1417§>450§<1550§@y

The first Defenestration of Prague; Begginging of Hussite Wars
§120§1419§>450§<1550§@y

Joan of Arc lifts the siege of Orléans for the Dauphin of France, enabling him to eventually be crowned at Reims
§80§1429§>450§<1550§@y

The Hundred Years' War between France and England ends
§80§1453§>450§<1550§@y

Siege of Belgrade; Ottomans are defeated by John Hunyadi and their advance is halted for next seventy years
§100§1456§>450§<1550§@y

Battle of Bosworth Field; Richard III dies in battle, and Henry Tudor becomes king of England
§100§1485§>450§<1550§@y

Emirate of Granada conquered; The end of Reconquista and  Moorish-Muslim rule on Iberian Peninsula
§60§1492§>450§<1550§@y

Vasco da Gama begins his first voyage from Europe to India 
§60§1497§>450§<1550§@y



HARD 
/*
Rome is sacked by Alaric, King of the Visigoths
410
*/

Byzantines, under Belisarius, retake North Africa from the Vandals
§120§533§>450§<1550§@y

The Kingdom of the Lombards is founded in Italy
§160§568§>450§<1550§@y

Accession of Abu Bakr as first Caliph
§80§632§>450§<1550§@y

/*
End of the Three Kingdoms period in Korea
668
*/

Establishment of the Bulgarian Empire
§160§681§>450§<1550§@y

Iconoclast movement begun in the Byzantine Empire under Leo III
§140§726§>450§<1550§@y

Battle of Tours; Charles Martel halts Muslim advance
§80§732§>450§<1550§@y

Beginning of Abbasid Caliphate
§120§750§>450§<1550§@y

Battle of Roncevaux Pass; The death of Roland
§100§778§>450§<1550§@y

Muslims invade Sicily
§100§827§>450§<1550§@y

/*
Viking state in Russia founded under Rurik, first at Novgorod, then Kyiv.
862
*/

Viking Great Heathen Army arrives in England
§100§866§>450§<1550§@y

/*
Alfred the Great assumes the throne, the first king of a united England
871
*/

Cluny Abbey is founded; Cluny becomes the leader of Western Monasticism and centre of *Cluniac Reforms*
§120§910§>450§<1550§@y

The Viking Rollo and his tribe are given Normandy by Charles the Simple, the king of West Francia; The foundation of the Duchy of Normandy
§120§911§>450§<1550§@y

Henry the Fowler, Duke of Saxony elected German King; The first king of the Ottonian Dynasty
§140§919§>450§<1550§@y

Battle of Lechfeld; Otto the Great, son of Henry the Fowler, defeats the Magyars
§140§955§>450§<1550§@y

Otto the Great crowned the Holy Roman Emperor
§120§962§>450§<1550§@y

Eric the Red, exiled from Iceland, begins Scandinavian colonization of Greenland.
§150§985§>450§<1550§@y

Succession of Hugh Capet to the French Throne; Beginning of Capetian Dynasty
§150§987§>450§<1550§@y

The Canon of Medicine is written by Persian polymath Avicenna
§200§1025§>450§<1550§@y

University of Bologna is founded; The oldest university in continuous operation in the world
§200§1088§>450§<1550§@y

The Cistercian Order is founded
§200§1098§>450§<1550§@y

Kingdom of Croatia and Kingdom of Hungary formed a personal union of two kingdoms united under the Hungarian king
§180§1102§>450§<1550§@y

The University of Oxford is founded; The oldest university in the UK
§200§1117§>450§<1550§@y

The Knights Templar are founded to protect Jerusalem and European pilgrims on their journey to the city
§120§1118§>450§<1550§@y

The Second Lateran Council declared clerical marriages invalid, regulated clerical dress, and punished attacks on clerics by excommunication
§150§1139§>450§<1550§@y

The Hanseatic League is founded
§120§1158§>450§<1550§@y

Stefan Nemanja united Serbian territories, establishing the Medieval Serbian state
§150§1166§>450§<1550§@y

/*
Guelphs/Ghibelline 
barbarrosa 
*/

Beginning of Inquisition
§160§1184§>450§<1550§@y

Uprising of Asen and Peter; The reestablishment of the Bulgarian Empire
§180§1185§>450§<1550§@y

Saladin recaptures Jerusalem; Would lead to the Third Crusade
§80§1187§>450§<1550§@y

Founding of the Franciscan Order.
§120§1209§>450§<1550§@y

Battle of Bouvines; French King Philip Augustus defeats alliance of England and Holy Roman Empire
§120§1214§>450§<1550§@y

Serbian Orthodox Church becomes autocephalous under St. Sava
§160§1219§>450§<1550§@y

Genghis Khan dies; His kingdom divided among his children and grandchildren
§80§1227§>450§<1550§@y

Battle of Legnica; Force of Poles and Moravians defeated by Mongols
§80§1241§>450§<1550§@y

Battle of Mohi; Hungarian forces defeated by Mongols
§100§1241§>450§<1550§@y

/*
cathars
sicilians
*/

William Wallace is executed for treason
§80§1305§>450§<1550§@y

The Knights Templar are rounded up and executed by Philip the Fair of France
§80§1307§>450§<1550§@y

Battle of Bannockburn; Robert the Bruce restores Scotland's de facto independence
§100§1314§>450§<1550§@y

Battle of Crécy; English forces led by Edward III and Edward, the Black Prince defeat the French forces of Philip VI
§80§1346§>450§<1550§@y

The University of Prague founded; The oldest Czech and German-Speaking University in the world
§150§1347§>450§<1550§@y

Tamerlane establishes the Timurid dynasty
§180§1370§>450§<1550§@y

Battle of Kulikovo; Dmitry Donskoy and united Russian army defeats the Golden Horde
§120§1380§>450§<1550§@y

Wat Tyler's Peasants' Revolt in England
§120§1381§>450§<1550§@y

The Battle of Nicopolis; Crusade against Ottomans is defeated; Bulgaria is conquered
§120§1396§>450§<1550§@y

Battle of Ankara; Ottomans are defeated by Tamerlane; the sultan is captured causing the interregnum of the Ottoman Empire
§120§1402§>450§<1550§@y

Bohemian Church Reformer Jan Hus is burned at the stake for heresy
§100§1415§>450§<1550§@y

Trial and execution of Joan of Arc.
§80§1431§>450§<1550§@y

Battle of Varna; Ottomans defeat Hungarian-Polish crusaders; Władysław III of Poland dies.
§80§1444§>450§<1550§@y

Coronation of Frederick III, the first Holy Roman Emperor of the House of Habsburg
§80§1452§>450§<1550§@y

The Khanate of Crimea is conquered and made a vassal state by the Ottoman Empire.
§120§1475§>450§<1550§@y

Great Stand on the Ugra River; The end of the Tatar-Mongol yoke over the Russian principalities
§100§1480§>450§<1550§@y

Treaty of Tordesillas; Spain and Portugal agree how to divide the World outside of Europe between themselves
§80§1494§>450§<1550§@y