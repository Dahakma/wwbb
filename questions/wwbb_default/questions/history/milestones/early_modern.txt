Milestones of the Early Modern Era

TAG history milestones

EASY
The Fall of Constantinople marks the end of the Byzantine Empire
§40§1453§>1450§<1800§@y

Christopher Columbus landed in the Americas from Spain
§40§1492§>1450§<1800§@y

Leonardo da Vinci begins painting the Mona Lisa and completes it three years later
§100§1453§>1450§<1800§@y

The Reformation begins when Martin Luther posts his 95 Theses in Saxony
§50§1517§>1450§<1800§@y

Philippines encountered by Ferdinand Magellan during his first circumnavigation of the Earth. He was later killed in battle in central Philippines in the same year
§80§1521§>1450§<1800§@y

The Ottomans defeat the Kingdom of Hungary at the Battle of Mohács; The end of independent powerful Hungary; Louis II dies without legitimate heir leaving thrones of Bohemia and Hungary empty
§80§1526§>1450§<1800§@y

Pope Gregory XIII issues the Gregorian calendar; The date skips from the last day of the Julian calendar 4  October to 15 October
§80§1582§>1450§<1800§@y

The Defenestration of Prague; The Bohemian Revolt precipitates the Thirty Years' War
§50§1618§>1450§<1800§@y

The Peace of Westphalia ends the Thirty Years' War and the Eighty Years' War
§50§1648§>1450§<1800§@y

The Great Fire of London
§80§1666§>1450§<1800§@y

The Army of Ottoman Empire of Grand Vizier Kara Mustafa is defeated in the second Siege of Vienna by the army of Habsburg Monarchy, the Polish–Lithuanian Commonwealth and the Holy Roman Empire, under the command of Polish King John III Sobieski
§60§1683§>1450§<1800§@y

Saint Petersburg is founded by Peter the Great; it is the Russian capital until 1918
§60§1703§>1450§<1800§@y

The Treaty of Paris ends the Seven Years' War
§50§1763§>1450§<1800§@y

The Boston Tea Party
§50§1773§>1450§<1800§@y

The Treaty of Paris formally ends the American Revolutionary War
§50§1783§>1450§<1800§@y

The Storming of the Bastille
§50§1789§>1450§<1800§@y

MEDIUM
The Fall of Constantinople marks the end of the Byzantine Empire
§20§1453§>1450§<1800§@y

The Battle of Castillon is the last engagement of the Hundred Years' War
§40§1453§>1450§<1800§@y

The marriage of Ferdinand II of Aragon and Isabella I of Castile leads to the unification of Spain
§60§1469§>1450§<1800§@y

Onin War starts the Sengoku period of social upheaval, political intrigue and near-constant military conflict in Japan
§60§1467§>1450§<1800§@y

Henry VII defeats Richard III at the Battle of Bosworth, the last significant battle of the Wars of the Roses, and becomes the King of England
§60§1485§>1450§<1800§@y

Portuguese Navigator Bartolomeu Dias sails around the Cape of Good Hope
§60§1488§>1450§<1800§@y

Surrender of Granada marks the end of the Spanish Reconquista and Al-Andalus
§50§1492§>1450§<1800§@y

The Reformation begins when Martin Luther posts his 95 Theses in Saxony
§40§1517§>1450§<1800§@y

Philippines encountered by Ferdinand Magellan during his first circumnavigation of the Earth. He was later killed in battle in central Philippines in the same year
§40§1521§>1450§<1800§@y

The Ottomans defeat the Kingdom of Hungary at the Battle of Mohács; The end of independent powerful Hungary; Louis II dies without legitimate heir leaving thrones of Bohemia and Hungary empty
§50§1526§>1450§<1800§@y

Francisco Pizarro leads the Spanish conquest of the Inca Empire
§60§1532§>1450§<1800§@y

Eighty Years' War between Spain and the Netherlands begins
§40§1566§>1450§<1800§@y

The Polish–Lithuanian Commonwealth is created with the Union of Lublin
§50§1569§>1450§<1800§@y

St. Bartholomew's Day massacre takes the lives of Protestant leader Gaspard de Coligny and thousands of Huguenots
§50§1572§>1450§<1800§@y

Pope Gregory XIII issues the Gregorian calendar; The date skips from the last day of the Julian calendar 4  October to 15 October
§50§1582§>1450§<1800§@y

England repulses the Spanish Armada
§50§1588§>1450§<1800§@y

The Edict of Nantes ends the French Wars of Religion
§50§1598§>1450§<1800§@y

The Time of Troubles begins; Russia descends into anarchy after the death of the last Tsar from the Rurik dynasty Feodor Ivanovich and a famine
§50§1598§>1450§<1800§@y

Battle of Sekigahara in Japan; decisive battle ending the Warring States period and eventually leading to the beginning of the Edo period
§60§1600§>1450§<1800§@y

Elizabeth I of England dies and is succeeded by her cousin King James VI of Scotland, uniting the crowns of Scotland and England
§40§1603§>1450§<1800§@y

Gunpowder Plot to blow up the House of Lords fails in England
§60§1605§>1450§<1800§@y

Jamestown, Virginia, is settled as what would become the first permanent English colony in North America
§60§1607§>1450§<1800§@y

The Defenestration of Prague; The Bohemian Revolt precipitates the Thirty Years' War
§30§1618§>1450§<1800§@y

The Battle of Lützen; Swedes were victory over imperial forces led by Albrecht von Wallenstein but their king Gustav II Adolf was killed
§60§1632§>1450§<1800§@y

Louis XIV is crowned King of France; He reigns France for 72 years
§60§1643§>1450§<1800§@y

The Khmelnytsky Uprising, a Cossack rebellion in Ukraine which turns into a Ukrainian war of liberation from Poland, begins
§60§1648§>1450§<1800§@y

King Charles I is executed for High treason, the first and only English king to be subjected to legal proceedings in a High Court of Justice and put to death
§50§1649§>1450§<1800§@y

The Glorious Revolution starts with the Dutch Republic invading England, England becomes a constitutional monarchy
§50§1688§>1450§<1800§@y

Saint Petersburg is founded by Peter the Great; it is the Russian capital until 1918
§50§1703§>1450§<1800§@y

The Act of Union is passed, merging the Scottish and English Parliaments, thus establishing the Kingdom of Great Britain
§40§1707§>1450§<1800§@y

The Treaty of Utrecht ends the War of the Spanish Succession
§40§1713§>1450§<1800§@y

The Treaty of Nystad is signed between the Tsardom of Russia and the Swedish Empire, ending the Great Northern War
§40§1721§>1450§<1800§@y

After a coup against her husband Catherine II became the ruler of Russian Empire
§50§1762§>1450§<1800§@y

The Treaty of Paris ends the Seven Years' War and Third Carnatic War
§50§1763§>1450§<1800§@y

Adam Smith publishes The Wealth of Nations.
§70§1776§>1450§<1800§@y

Russian Empire annexes the Crimean Khanate.
§60§1783§>1450§<1800§@y

Declaration of the Rights of Man and of the Citizen
§30§1789§>1450§<1800§@y

Former King Louis XVI of France and his wife Marie Antoinette are guillotined. Louis is executed in January, Marie Antoinette in October
§20§1793§>1450§<1800§@y

HARD
Onin War starts the Sengoku period of social upheaval, political intrigue and near-constant military conflict in Japan
§60§1467§>1450§<1800§@y

The Great standing on the Ugra river; Tatar forces withdraw withot fighting; Muscovy gains independence from the Great Horde
§50§1480§>1450§<1800§@y

Matthias Corvinus of Hungary captureds Vienna, Frederick III, Holy Roman Emperor runs away
§60§1485§>1450§<1800§@y

Henry VII defeats Richard III at the Battle of Bosworth, the last significant battle of the Wars of the Roses, and becomes King of England
§30§1485§>1450§<1800§@y

Portuguese Navigator Bartolomeu Dias sails around the Cape of Good Hope.
§40§1488§>1450§<1800§@y

Surrender of Granada marks the end of the Spanish Reconquista and Al-Andalus
§20§1492§>1450§<1800§@y

Portuguese navigator Pedro Álvares Cabral claims Brazil for Portugal
§60§1500§>1450§<1800§@y

Michelangelo returns to his native Florence to begin work on the statue David.
§60§1501§>1450§<1800§@y

Leonardo da Vinci begins painting the Mona Lisa and completes it three years later
§40§1503§>1450§<1800§@y

King Afonso I of Kongo wins the battle of Mbanza Kongo, resulting in Catholicism becoming Kongo's state religion
§70§1506§>1450§<1800§@y

Machiavelli writes The Prince, a treatise about political philosophy
§70§1513§>1450§<1800§@y

The Ottomans defeats the Mamluks and gain control of Egypt, Arabia, and the Levant.
§50§1517§>1450§<1800§@y

Charles I of Spain becomes Emperor of Holy Roman Empire as Charles V, Holy Roman Emperor
§30§1519§>1450§<1800§@y

Belgrade is captured by the Ottoman Empire
§50§1521§>1450§<1800§@y

Sweden gains independence from the Kalmar Union.
§50§1523§>1450§<1800§@y

Spain and Germany defeat France at the Battle of Pavia, Francis I of France is captured
§40§1525§>1450§<1800§@y

The Ottomans defeat the Kingdom of Hungary at the Battle of Mohács; The end of independent powerful Hungary; Louis II dies without legitimate heir leaving thrones of Bohemia and Hungary empty
§20§1526§>1450§<1800§@y

Sack of Rome by the mutinous troops of Charles V, Holy Roman Emperor
§30§1527§>1450§<1800§@y

In England, Anne Boleyn is beheaded for adultery and treason
§50§1536§>1450§<1800§@y

Emperor Charles V decisively dismantles the Schmalkaldic League at the Battle of Mühlberg
§50§1547§>1450§<1800§@y

Grand Prince Ivan the Terrible is crowned tsar of (All)Russia, thenceforth becoming the first Russian tsar
§50§1547§>1450§<1800§@y

Elizabeth Tudor becomes Queen Elizabeth I at age 25
§30§1558§>1450§<1800§@y

Mercator world map published by Gerardus Mercator
§60§1569§>1450§<1800§@y

Ivan the Terrible, tsar of Russia, orders the massacre of inhabitants of Novgorod (allegedly planning to defect to Poland-Lithuania)
§50§1570§>1450§<1800§@y

The Spanish-led Holy League navy destroys the Ottoman Empire navy at the Battle of Lepanto
§60§1571§>1450§<1800§@y

Pope Gregory XIII issues the Gregorian calendar; The date skips from the last day of the Julian calendar 4  October to 15 October
§20§1582§>1450§<1800§@y

Spain repulses the English Armada
§50§1589§>1450§<1800§@y

The Edict of Nantes ends the French Wars of Religion
§40§1598§>1450§<1800§@y

Battle of Kinsale, England defeats Irish and Spanish forces at the town of Kinsale, driving the Gaelic aristocracy out of Ireland and destroying the Gaelic clan system
§60§1601§>1450§<1800§@y

The Dutch East India Company (VOC) is established by merging competing Dutch trading companies
§60§1602§>1450§<1800§@y

Tokugawa Ieyasu takes the title of shogun, establishing the Tokugawa shogunate. This begins the Edo period, which will last until 1868
§50§1603§>1450§<1800§@y

Captain Willem Janszoon and his crew aboard the Dutch East India Company ship Duyfken becomes the first recorded Europeans to sight and make landfall in Australia
§60§1606§>1450§<1800§@y

Emperor Ferdinand II defeats the Bohemian rebels in the Battle of White Mountain
§40§1620§>1450§<1800§@y

New Amsterdam founded by the Dutch West India Company in North America
§60§1625§>1450§<1800§@y

Battle of Lützen, death of king of Sweden Gustav II Adolf
§30§1632§>1450§<1800§@y

Taj Mahal building work started in Agra, India
§60§1632§>1450§<1800§@y

Torture is outlawed in England
§70§1640§>1450§<1800§@y

The death of Miyamoto Musashi, legendary Japanese Samurai warrior, of natural causes
§60§1645§>1450§<1800§@y

Defeated, King Charles I flees and surrenders to the Scottish. The First Civil War has ended in a victory for Parliament
§40§1646§>1450§<1800§@y

English Civil War ends with the Parliamentarian victory at the Battle of Worcester
§40§1651§>1450§<1800§@y

British troops capture New Amsterdam and rename it New York
§60§1664§>1450§<1800§@y

The successful Dutch Raid on the Medway during the Second Anglo-Dutch War
§50§1667§>1450§<1800§@y

The Army of Ottoman Empire of Grand Vizier Kara Mustafa is defeated in the second Siege of Vienna by the army of Habsburg Monarchy, the Polish–Lithuanian Commonwealth and the Holy Roman Empire, under the command of Polish King John III Sobieski
§20§1683§>1450§<1800§@y

The Treaty of Nijmegen ends various interconnected wars among France, the Dutch Republic, Spain, Brandenburg, Sweden, Denmark, the Prince-Bishopric of Münster, and the Holy Roman Empire
§40§1678§>1450§<1800§@y

Salem witch trials in Massachusetts
§50§1692§>1450§<1800§@y

Kingdom of Prussia declared under King Frederick I
§50§1701§>1450§<1800§@y

Forty-seven ronin attack Kira Yoshinaka and then commit seppuku in Japan
§50§1702§>1450§<1800§@y

The Act of Union is passed, merging the Scottish and English Parliaments, thus establishing the Kingdom of Great Britain
§30§1707§>1450§<1800§@y

Charles XII of Sweden flees to the Ottoman Empire after Peter I of Russia defeats his army at the Battle of Poltava
§30§1709§>1450§<1800§@y

The Treaty of Utrecht ends the War of the Spanish Succession
§40§1713§>1450§<1800§@y

Accession of George I, Elector of Hanover, to the throne of Great Britain
§40§1714§>1450§<1800§@y

Pirate captain Blackbeard (Edward Teach) is killed in battle against lieutenant Robert Maynard
§50§1718§>1450§<1800§@y

The South Sea Bubble collapse
§50§1720§>1450§<1800§@y

The Treaty of Nystad is signed between the Tsardom of Russia and the Swedish Empire, ending the Great Northern War
§30§1721§>1450§<1800§@y

Great Britain and Spain fight the War of Jenkins' Ear in the Caribbean
§50§1739§>1450§<1800§@y

The Battle of Plassey signals the beginning of formal British rule in India after years of commercial activity under the auspices of the East India Company
§40§1757§>1450§<1800§@y

French and Indian War: French commander Louis-Joseph de Montcalm and British commander James Wolfe die during the Battle of the Plains of Abraham
§40§1759§>1450§<1800§@y

The Boston *Massacre*; rioting mob attacks British soldiers and five people get themselves killed
§30§1770§>1450§<1800§@y

Pugachev's Rebellion, the largest peasant revolt in Russian history begins
§50§1773§>1450§<1800§@y

The Boston Tea Party
§20§1773§>1450§<1800§@y

Captain James Cook is killed by Hawaiian natives at Kealakekua Bay, following an attempted kidnapping and ransoming of ruling chief, Kalaniʻōpuʻu in return for a stolen boat
§40§1779§>1450§<1800§@y

The city of Los Angeles is founded by Spanish settlers
§60§1781§>1450§<1800§@y

Famine in Iceland, caused by the eruption of the Laki volcano
§50§1783§>1450§<1800§@y

The Storming of the Bastille
§15§1789§>1450§<1800§@y

The New York Stock & Exchange Board is founded
§60§1792§>1450§<1800§@y

Former King Louis XVI of France and his wife Marie Antoinette are guillotined. Louis is executed in January, Marie Antoinette in October
§20§1793§>1450§<1800§@y

War of the First Coalition: The Battle of Montenotte marks Napoleon Bonaparte's first victory as an army commander
§20§1796§>1450§<1800§@y