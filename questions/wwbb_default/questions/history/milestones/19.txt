Milestones of 19th Century 

TAG history milestones

EASY
The Kingdom of Great Britain and the Kingdom of Ireland merge to form the United Kingdom
§10§1801§>1799§<1900§@y

Austrian Empire founded by Francis I.
§14§1804§>1799§<1900§@y

Holy Roman Empire dissolved
§12§1806§>1799§<1900§@y

Beginning of war between the United States and United Kingdom
§6§1812§>1799§<1900§@y

The Congress of Vienna redraws the European map after the Napoleonic Wars
§10§1815§>1799§<1900§@y

July Revolution in France.
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg/606px-Eugène_Delacroix_-_La_liberté_guidant_le_peuple.jpg
§15§1830§>1799§<1900§@y

Battle of the Alamo ends with defeat for Texan separatists
§14§1836§>1799§<1900§@y

Queen Victoria is crowned
§12§1837§>1799§<1900§@y

The Communist Manifesto published
§12§1848§>1799§<1900§@y

Wave of revolutions across Europe
§10§1848§>1799§<1900§@y

Bombardment of Fort Sumter; American Civil War starts after secession of Confederacy
§12§1861§>1799§<1900§@y

United States President Abraham Lincoln issues the Emancipation Proclamation
§10§1863§>1799§<1900§@y

Robert E. Lee surrenders the Army of Northern Virginia to Ulysses S. Grant effectively ending the American Civil War
§12§1865§>1799§<1900§@y

Defeat of France in the Franco-Prussian War results in the unifications of Germany and Italy and the collapse of the Second French Empire
§14§1871§>1799§<1900§@y

MEDIUM
Ludwig van Beethoven performs his Moonlight Sonata for the first time.
§30§1802§>1799§<1900§@y

The United States more than doubles in size when it buys out France's territorial claims in North America via the Louisiana Purchase.
§20§1803§>1799§<1900§@y

Haiti gains independence from France and becomes the first black republic.
§12§1804§>1799§<1900§@y

Jane Austen publishes Pride and Prejudice
§18§1813§>1799§<1900§@y

Napoleon abdicates and is exiled to Elba.
§10§1814§>1799§<1900§@y

Missouri Compromise; Maine admitted as a free and Missouri as a slave state, maintaining the balance; slavery prohibited north of the 36°30′ parallel, excluding Missouri
§10§1820§>1799§<1900§@y

Regency period ends in the United Kingdom; George IV ascend the throne
§10§1820§>1799§<1900§@y

Monroe Doctrine - policy of opposing European colonialism in the Americas - declared by US President James Monroe
§12§1823§>1799§<1900§@y

Premiere of Beethoven's Ninth Symphony
§15§1824§>1799§<1900§@y

The Decembrist revolt; Liberal imperial officers uprise against Tsar Nicholas
§13§1825§>1799§<1900§@y

The Belgian Revolution in the United Kingdom of the Netherlands leads to the creation of Belgium.
§13§1830§>1799§<1900§@y

Greater Colombia dissolved and the nations of Colombia (including modern-day Panama), Ecuador, and Venezuela take its place
§16§1830§>1799§<1900§@y

Slavery Abolition Act bans slavery throughout the British Empire
§14§1833§>1799§<1900§@y

Charles Dickens publishes Oliver Twist
§20§1837§>1799§<1900§@y

Taiping Rebellion, the bloodiest conflict of the century leading to the deaths of 20 million people, begins
§14§1850§>1799§<1900§@y

The Great Exhibition in London is the world's first international Expo or World Fair
§12§1851§>1799§<1900§@y

Napoleon III assumes power in France in a coup
§12§1851§>1799§<1900§@y

United States Commodore Matthew C. Perry threatens the Japanese capital Edo with gunships, demanding that they agree to open trade
§12§1853§>1799§<1900§@y

Expedition of the Thousand; Giuseppe Garibaldi's volunteers lands in Sicily to conquer the Kingdom of the Two Sicilies ruled by the Spanish Bourbons; Major event for Italian unification
§12§1860§>1799§<1900§@y

Russia abolishes serfdom
§15§1861§>1799§<1900§@y

Bombardment of Fort Sumter; American Civil War starts after secession of Confederacy
§6§1861§>1799§<1900§@y

Robert E. Lee surrenders the Army of Northern Virginia to Ulysses S. Grant; Effectively ending the American Civil War
§6§1865§>1799§<1900§@y

Moravian friar Gregor Mendel formulates his laws of inheritance
§22§1865§>1799§<1900§@y

The United States purchases Alaska from Russia
§16§1867§>1799§<1900§@y

Alfred Nobel invents dynamite
§22§1867§>1799§<1900§@y

Dmitri Mendeleev creates the Periodic table
§22§1869§>1799§<1900§@y

Italian unification, *Risorgiemnto*, completed; Rome became the capital of the Kingdom of Italy
§10§1871§>1799§<1900§@y

Blue jeans and barbed wire are invented
§20§1873§>1799§<1900§@y

Battle of the Little Bighorn leads to the death of General Custer and victory for the alliance of Lakota, Cheyenne and Arapaho
§16§1876§>1799§<1900§@y

Mark Twain publishes The Adventures of Huckleberry Finn
§16§1884§>1799§<1900§@y

Louis Pasteur creates the first successful vaccine against rabies 
§12§1885§>1799§<1900§@y

Sir Hiram Maxim invents the first self-powered Machine gun
§16§1884§>1799§<1900§@y

France and the Russian Empire form a military alliance.
§10§1894§>1799§<1900§@y

US forces overthrow the government of Hawaii.
§14§1893§>1799§<1900§@y

HARD
Beginning of the First Barbary War between the United States and the Barbary States of North Africa
§15§1801§>1799§<1900§@y

The Kingdom of Great Britain and the Kingdom of Ireland merge to form the United Kingdom.
§4§1801§>1799§<1900§@y

The Wahhabis of the First Saudi State capture Mecca and Medina damaging the prestige of Ottoman Empire
§12§1803§>1799§<1900§@y

Austrian Empire founded by Francis I.
§4§1804§>1799§<1900§@y

Holy Roman Empire dissolved
§4§1806§>1799§<1900§@y

Cape Colony becomes part of the British Empire
§12§1806§>1799§<1900§@y

Potassium and Sodium are individually isolated by Sir Humphry Davy
§18§1807§>1799§<1900§@y

*The Grito de Dolores*; priest Miguel Hidalgo y Costilla begins the Mexican War of Independence
§8§1810§>1799§<1900§@y

War between the United States and Britain ends in draw
§6§1815§>1799§<1900§@y

The Congress of Vienna redraws the European map 
§5§1815§>1799§<1900§@y

Declared Independence of Argentina
§13§1816§>1799§<1900§@y

Mary Shelley publishes Frankenstein
§12§1818§>1799§<1900§@y

The modern city of Singapore is established by the British East India Company
§12§1819§>1799§<1900§@y

Liberia founded by the American Colonization Society for freed American slaves
§12§1820§>1799§<1900§@y

Mexico gains independence from Spain with the Treaty of Córdoba
§12§1821§>1799§<1900§@y

Prince Pedro of Brazil proclaims the Brazilian independence
§12§1822§>1799§<1900§@y

First isolation of aluminium.
§20§1825§>1799§<1900§@y

Declared Independence of Bolivia
§15§1825§>1799§<1900§@y

Auspicious Incident; Sultan Mahmud II disbands Janissary corps after their rebellion
§15§1826§>1799§<1900§@y

November Uprising in Poland against Russia begins
§15§1830§>1799§<1900§@y

The British Parliament passes the Great Reform Act introducing wide-ranging changes to the electoral system of England and Wales
§18§1832§>1799§<1900§@y

Queen Victoria is crowned
§6§1837§>1799§<1900§@y

Alexander Pushkin fatally wounded in a duel
§20§1839§>1799§<1900§@y

Anaesthesia used for the first time
§20§1842§>1799§<1900§@y

The Brontë sisters publish Jane Eyre, Wuthering Heights and Agnes Grey
§20§1847§>1799§<1900§@y

Wave of revolutions across Europe
§4§1848§>1799§<1900§@y

Battle of Balaclava and the Charge of the Light Brigade 
§10§1854§>1799§<1900§@y

Bessemer process enables steel to be mass-produced
§22§1855§>1799§<1900§@y

Charles Darwin publishes On the Origin of Species
§10§1859§>1799§<1900§@y

The Pony Express ended
§20§1862§>1799§<1900§@y

Victor Hugo publishes Les Misérables
§18§1862§>1799§<1900§@y

Formation of the International Red Cross
§16§1863§>1799§<1900§@y

Execution of Emperor Maximilian I; End of the Second Mexican Empire  
§14§1867§>1799§<1900§@y

Paraguay is defeated in the war against the Triple Alliance of Argentina, Brazil and Uruguay ends; Paraguay suffers catastrophic losses, almost 70% of its adult male population died
§16§1870§>1799§<1900§@y

Defeat of France in the Franco-Prussian War results in the unifications of Germany and the collapse of the Second French Empire
§6§1871§>1799§<1900§@y

Radical socialist and revolutionary Paris Commune briefly rules the French capital
§14§1871§>1799§<1900§@y

Henry Morton Stanley meets Dr. David Livingstone near Lake Tanganyika
§20§1871§>1799§<1900§@y

Following the Russo-Turkish War, the Treaty of Berlin recognizes formal independence of the Principality of Serbia, Montenegro and Romania. Bulgaria becomes autonomous
§12§1878§>1799§<1900§@y

Tsar Alexander II is assassinated
§12§1881§>1799§<1900§@y

Gunfight at the O.K. Corral
§18§1881§>1799§<1900§@y

King Leopold II of Belgium establishes the Congo Free State as a personal fiefdom
§14§1885§>1799§<1900§@y

Construction of the Statue of Liberty; Coca-Cola is developed
§12§1886§>1799§<1900§@y

Sir Arthur Conan Doyle publishes his first Sherlock Holmes story, A Study in Scarlet
§12§1887§>1799§<1900§@y

The Wounded Knee Massacre in South Dakota was the last battle in the American Indian Wars
§14§1890§>1799§<1900§@y

First use of the electric chair as a method of execution.
§20§1890§>1799§<1900§@y

New Zealand becomes the first country to enact women's suffrage
§20§1893§>1799§<1900§@y

Beginning of Klondike Gold Rush in Canada
§12§1896§>1799§<1900§@y

Empress Elisabeth of Austria is assassinated by anarchist Luigi Lucheni
§12§1898§>1799§<1900§@y