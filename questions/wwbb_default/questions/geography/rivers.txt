Rivers

TAGS geography

EASY
The Grand Canyon was carved by the river 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Grandcanyon_view5.jpg/640px-Grandcanyon_view5.jpg
Colorado
Rio Grande
Yellowstone
Missouri

The river with the greatest average discharge (209 000 m^3/s) is
Amazon
Yangtze
Ganges
Congo

In the picture are Paris and river
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Paris_View_from_the_Eiffel_Tower_third_floor_Seine_downstream_RD_01.jpg/640px-Paris_View_from_the_Eiffel_Tower_third_floor_Seine_downstream_RD_01.jpg
Seine
Rhone
Loire
Garonne

Mesopotamia means *Land between Rivers*. The southern, longer one is
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Tigr-euph-ar.png/478px-Tigr-euph-ar.png
Euphrates
Tigris
Jordan
Indus

Which river flows into the Gulf of Mexico?
Rio Grande
Delaware
Orinoco
Columbia

MEDIUM
The deposition of sediments by a river near its mouth before it drains into a body of water is called after the Greek letter
Δ
Ω
Ψ
Θ

Victoria Falls, the largest {by the total sheet of falling water} waterfalls are on the river 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Victoria_Falls_from_Zambia(August_2009).jpg/640px-Victoria_Falls_from_Zambia(August_2009).jpg
Zambezi
Limpopo
Congo
Okavango

Which Russian river flows into the inland Caspian Sea?
Volga
Don
Dnieper
Angara

The longest {3 692 km} European river is 
Volga
Danube
Ural
Dnieper 

Danube does NOT flow through the capital city of 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Parliament_Budapest_Hungary.jpg/640px-Parliament_Budapest_Hungary.jpg
Bulgaria
Austria
Slovakia
Serbia

The third longest river after Nile and Amazon is 
Yangtze (Chang Jiang)
Mekong
Yellow River (Huang He)
Congo

The Nile flows through 
11 countries
8 countries
5 countries
14 countries

HARD
The SECOND longest river in South America is
Paraná 
Orinoco
Madeira
Purus

The Elbe/Labe starts in 
Czechia
Germany 
Poland
Austria

The longest Russian river system {5539@km} is 
Yenisei–Angara–Selenge
Amur–Argun
Lena
Volga

The longest river in the United Kingdom {354@km} is the river
Severn
Thames
Trent
Tay

Which river flows into the Bering Sea?
Yukon
Snake
Mackenzie
Churchill

The deepest river with the maximal depth exceeding 220@m is
Congo
Danube
Hudson
Mekong

Danube, one of the longest European rivers, flows through capital cities of 
4 countries
3 countries
5 countries
6 countries 

The Nile does NOT flows through 
Chad
Burundi
South Sudan
Uganda

Free and Hanseatic City of Hamburg, the European second largest port, is located on river
Elbe
Danube
Rhine
Main 