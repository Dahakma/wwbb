U.S. States 

TAGS usa country

EASY
The state with the largest population {est. 39 536 653} is
California
Texas
Florida
New York 

The largest state by territory {1 723 337 km^2} is
Alaska
Texas
California
Montana

Which state does NOT share borders with Mexico
Nevada
California
Arizona
New Mexico

Which state does NOT share borders with Canada
Missouri
Michigan
Minnesota
Maine

The state of Washington is located in 
Northwest 
Southwest
Northeast
Southeast

Which state was created during the American Civil war by separation from a Confederate state?
West Virginia
North Carolina
Kentucky
Tennessee

MEDIUM
The state with the smallest population {est. 579 315} is
Wyoming
North Dakota
Alaska
Hawaii

Yellowstone National Park is located in 
Wyoming, Montana, and Idaho
Oregon, Idaho and Nevada
Wyoming, Utah and Idaho
Idaho, Nevada and Utah

The smallest state by territory {4 001 km^2} is
Rhode Island
Connecticut 
Vermont
New Jersey

Which state does NOT share borders with Canada
Massachusetts
New York
New Hampshire
Vermont

The Mojave, Hoover Dam, the economy tied to tourism, gold & silver mining
Nevada
Arizona
New Mexico
California

The state named in honour of queen of England (ruling 1558 – 1603) is 
Virginia
Carolina
Maryland
Louisiana

Between Nevada and Colorado is
Utah
New Mexico
Oregon
Nebraska

Between South Dakota and Kansas is 
Nebraska
Oklahoma
Arkansas
Iowa

HARD
In the picture is a map of
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Wyoming_counties_map.png/611px-Wyoming_counties_map.png
Wyoming
Utah
Nebraska
Iowa

MI is the official abbreviation of 
Michigan
Minnesota
Mississippi
Missouri

Grand Teton National Park, the first territory allowing women to vote {1869}, the largest coal producer in the US
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Devils_Tower_CROP.jpg/640px-Devils_Tower_CROP.jpg
Wyoming
Utah
Montana
North Dakota

The least populous capital city and the least populous most populous city in the US, the first state abolishing slavery {1777}, the biggest maple syrup producer in the US
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/LakeWilloughbyPisgah.jpg/360px-LakeWilloughbyPisgah.jpg
Vermont
Maine
New Hampshire
Connecticut

Theodore Roosevelt National Park, the geographical centre of North America, the top producer of flax, rape and honey, booming oil industry 
North Dakota
Iowa
Montana
Minnesota

The Black Hills Gold Rush, the Wounded Knee Massacre, Mount Rushmore, Badlands National Park
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Black_Hills_from_Harney_Peak.jpg/640px-Black_Hills_from_Harney_Peak.jpg
South Dakota
North Dakota
Nebraska
Colorado

The first state to ratify the Constitution, named after a governor of Virginia
Delaware
Connecticut
Pennsylvania
Vermont 

Puerto Rico, an unincorporated territory of the United States, has a similar population as 
Iowa
Montana
Arizona
Pennsylvania

Between Illinois and Ohio is
Indiana
Wisconsin
Tennessee
Iowa