Czech Republic

TAGS country europe ceu

EASY
This historical region is shared between the Czech Republic and Poland. {The whole region used to be a part of Czech Lands but it was conquered by Prussia because Habsburgs sucked at doing their job. }
Silesia
Moravia
Lusatia
Pomerania

The fourth most populous Czech city Plzeň {Pilsen} is worldwide famous for  
Pale lager
Late medieval cathedral 
Horse breeding
Car manufacturing

The Czech Republic does NOT share borders with
France
Slovakia
Poland
Germany

At the beginning of the WW1 were the Czech lands a part of 
Austro-Hungarian Empire
German Empire
Russian Empire
Independent

After a decision of Czech and Slovak politicians, Czechoslovakia was peacefully dissolved on 1 January {dividing federal assets in the approximate population ratio of 2 to 1}
1993
2008
1978
1963

MEDIUM
The largest Czech car manufacturer {founded 1895, now a part of Volkswagen Group} is
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/13-04-05-Skoda_Museum_Mladá_Boleslav_by_RalfR-233.png/475px-13-04-05-Skoda_Museum_Mladá_Boleslav_by_RalfR-233.png
Škoda
Dacia
Volvo
Opel

The greatest achievement in the modern history of the Czech Republic was the victory in the men's ice hockey tournament at the 1998 Winter Olympics. The tournament became know as the *Tournament of the Century* because for the first time in history NHL took a break which allowed the national teams to bring their greatest stars. However, the Czech team was able to gradually gloriously defeat favourised teams of USA, Canada and Russia and win the Gold. The 1998 Winter Olympics took place at 
Nagano, Japan
Salt Lake City, USA
Lillehammer, Norway
Torino, Italy

The Czech Republic is a NATO member since 
1999
2012
1987
1975

The second largest Czech city {population 377 4400} is
Brno
Ostrava
Plzeň
Olomouc

This version of the coat of arms depicts the Bohemian lion, however, the lion is not placed on a shield of some arrogant degenerate feudal lord but on a humble pavise of a common brave foot soldier. It was used to represent Czechoslovakia between years 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Coat_of_arms_of_Czechoslovakia_(1961-1989).svg/335px-Coat_of_arms_of_Czechoslovakia_(1961-1989).svg.png
1961-1989
since 1993
1938-1945
1918-1938

Which of following is NOT a loanword from the Czech language? 
Trek
Polka
Pistol
Robot
Howitzer

HARD
One of the world best football goalkeepers is Petr Čech, remarkable for his flawless saves and rugby-like helmet he started wearing after a serious head injury. Most of career he spent playing for
Chelsea 
Manchester United
Liverpool
Tottenham Hotspur

The Little Entete (existing between 1921 - 1938) was a military alliance of three countries backed by France. It should ensure common defense against Hungarian revanchism and the prevention of a Habsburg restoration - after the First World War all three coutries gained territory previously controlled by Hungary. However facing the rising power of the Naci Germany and different interests of member coutries the Little Entete slowly desintegrated. The members were
Czechoslovakia, Yugoslavia and Romania
Czechoslovakia, Yugoslavia and Bulgaria
Czechoslovakia, Poland and Romania
Czechoslovakia, Poland and Bulgaria

The capital Prague is located on the river
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Prague_skyline_view.jpg/640px-Prague_skyline_view.jpg
Vltava
Labe (Elbe)
Dunaj (Danube)
Visla (Vistula)

The Czech Republic does NOT share borders with
Hungary
Slovakia
Poland
Germany

The Second {actually Third, there was one minor nobody remembers} Prague defenestration sparked the Thirty Years' War when the rebelling protestant Bohemian estates threw out of windows of Prague Castle two Catholic Royal regents and their secretary. {They all miraculously survived.} The event took place in 
1618
1658
1698
1738