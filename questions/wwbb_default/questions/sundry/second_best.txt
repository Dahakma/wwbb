﻿The Second Best

TAG

EASY
The second highest mountain on Earth (8611@m) is 
K2
Kangchenjunga
Annapurna
Lhotse
Makalu
Dhaulagiri

The second-largest country (9984670@km2) is
Canada
USA
China
Australia
Brazil
Russia

The second most populous country (1 382 236 609) is
India
Nigeria
USA
Indonesia
Pakistan
Brazil

The second most populous continent (1.3 billion) is
Africa
Asia
North America
Europe
South America

The second least populous continent (42 million) is
Oceania
South America
North America
Antarctica
Europe
Africa

The second-smallest planet in the Solar system is 
Mars
Mercury
Venus 
Earth

MEDIUM
The second most abundant element in the universe is
Helium
Hydrogen
Iron
Carbon
Oxygen

The second-largest USA state is 
Texas
California
Montana
New Mexico
Arizona
Nevada

The second-largest continent (30365000@km2) is 
Africa
North America
Asia
Antarctica
South America

The second smallest continent (10180000@km2) is 
Europe
Antarctica
South America
Oceania
Africa

The second deepest lake is (1470@m) is
Tanganyika
Great Slave Lake
Malawi
Crater Lake
Vostok
O'Higgins/San Martín

The second smallest country (2.02@km) is
Monaco
Nauru
Tuvalu
San Marino
Liechtenstein
Andorra

The second man on the Moon was 
Buzz Aldrin
Michael Collins
Jim Lovell
Pete Conrad

The second explorer to circumnavigate the globe was
Francis Drake
James Cook
Abel Tasman
Jacques Cartier
Pedro Álvares Cabral

HARD
The second deepest oceanic trench (10800@m) is
Tonga Trench
Philippine Trench
Kuril–Kamchatka Trench
Kermadec Trench
Izu–Bonin Trench
South Sandwich Trench

The second-largest animal is 
Fin whale
Blue whale
Gray whale
Sperm whale
Humpback whale

The second-largest known dwarf planet is 
Eris
Ceres
Sedna
Haumea

/*
//TODO - Merdeka 118  - 2023
The second tallest building (632@m) is
Shanghai Tower (Shanghai)
Abraj Al-Bait Clock Tower (Mecca)
One World Trade Center (New York)
Ping An International Finance Centre (Shenzhen)
Lotte World Tower (Seoul)
*/

/*
//TODO - not sure if correct
The second most frequent letter in the English language is
T
E
O
I
N
*/

The second-largest bone in the human body is 
Tibia
Humerus
Radius
Fibula
Ulna
Femur

The second movie in the James Bond franchise is
From Russia with Love
Goldfinger
You Only Live Twice
On Her Majesty's Secret Service
Thunderball
Live and Let Die

The second hardest mineral on the Mohs scale is 
Corundum
Quartz
Apatite
Spinel
Emerald
Orthoclase

The second most common surname in the USA is 
Johnson
Miller
Garcia
Jones
Smith

The second game in The Elder Scrolls series is The Elder Scrolls II:
Daggerfall
Arena
Morrowind
Oblivion
Redguard