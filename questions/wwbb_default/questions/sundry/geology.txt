Geology 

TAGS science

EASY
Diamonds are made of 
Carbon 
Silicon
Calcium
Sulphur

The hardest mineral on the Mohs scale of mineral hardness is 
Diamond
Corundum
Tungsten carbide
Quartz

Which of following is a sedimentary rock (formed at the earth's surface by the accumulation and cementation of fragments of earlier rocks, minerals, and organisms)
Sandstone
Marble
Basalt
Slate

The supercontinent which existed 335 million - 175 million years ago was named
Pangaea
Hyperborea 
Lemuria
Laurentie

The most known volcanic glass is 
Obsidian 
Beryl
Flint
Serpentinite

MEDIUM
What is the only liquid layer of the Earth?
Outer core 
Crust
Inner core
Mantle

The softest mineral on the Mohs scale of mineral hardness is 
Talc
Gypsum
Graphite
Sulfur 

Branch of geology that studies rocks and the conditions under which they form is
Petrology
Mineralogy
Pedology
Geomorphology

The most common colour of lapis lazuli is
Blue
Green
Red
Yellow

The current geological epoch is called
Holocene
Pleistocene
Oligocene
Miocene

Which of following is an igneous rock (formed through the cooling and solidification of magma or lava)?
Basalt
Sandstone
Marble
Gneiss

Which of following is a sedimentary rock (formed at the earth's surface by the accumulation and cementation of fragments of earlier rocks, minerals, and organisms)
Limestone
Gabbro
Gneiss
Slate

Which of following is a metamorphic rock (formed by subjecting any rock type to heat and pressure)  
Gneiss
Limestone
Sandstone
Granite

HARD
The current geological era is called
Cenozoic
Paleozoic
Mesozoic
Neoproterozoic

Compare hardness of following minerals from the Mohs scale
Corundum > Quartz > Apatite > Calcite
Quartz > Corundum > Calcite > Apatite
Corundum > Apatite > Quartz > Calcite
Quartz > Calcite > Corundum > Apatite

Orogeny is a process leading to the formation of 
Mountains
Glacial lakes
Sedimentary rock
Soil

Which of following is an igneous rock
Granite
Shale
Marble
Quartzite

Which of following is a metamorphic rock
Marble
Diorite
Gabbro
Limestone

Which of following is a sedimentary rock
Shale
Diorite
Schist
Granite