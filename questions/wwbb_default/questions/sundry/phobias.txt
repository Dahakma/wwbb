Phobias

TAGS language
SHARED XXX is the fear of

EASY
Aerophobia
aircrafts or flying

Agoraphobia
open places

Anthropophobia
people or the company of people

Aquaphobia
water

Arachnophobia
spiders

Claustrophobia
having no escape and being closed in

Gynophobia
women

Hexakosioihexekontahexaphobia
the number 666

Necrophobia
death or the dead

Xenophobia
strangers, foreigners, or aliens

Triskaidekaphobia
the number 13

Phobophobia
fear itself or of having a phobia

MEDIUM
Phallophobia
erections

Acrophobia
heights

Algophobia
pain

Cynophobia
dogs

Coprophobia
feces or defecation

Ephebiphobia
youth

Erotophobia
sexual love or sexual abuse

Glossophobia
speaking in public or of trying to speak

Gerontophobia
growing old, or a hatred or the elderly

Haphephobia
being touched

Heliophobia
the sun or sunlight

Hemophobia
blood

Ichthyophobia
fish

Thanatophobia
dying

Ophidiophobia
snakes

HARD
Batrachophobia
frogs and other amphibians

Philophobia
love

Pogonophobia
beards

Oneirophobia
dreams

Scopophobia
being looked at or stared at

Spectrophobia
mirrors

Thalassophobia
the sea

Myrmecophobia
ants

Ablutophobia
bathing, washing or cleaning

Aichmophobia
sharp or pointed objects

Ailurophobia
cats

Coulrophobia
clowns

Coimetrophobia
cemeteries

Chiroptophobia
bats

Eurotophobia
or aversion to female genitals

Globophobia
balloons

Genophobia
sexual intercourse

Gephyrophobia
bridges

Astraphobia
thunder and lightning

Melissophobia
bees

Myrmecophobia
ants

Nyctophobia
darkness