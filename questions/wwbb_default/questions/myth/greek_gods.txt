Greek Gods

TAGS myth wip9

EASY
The goddess of beauty, love, desire, and pleasure was
Aphrodite
Athena
Demeter
Persephone

The virgin goddess of the hunt, wilderness and animals was 
Artemis
Hestia
Aphrodite
Eris

The goddess of reason, wisdom, intelligence, skill, peace, warfare, battle strategy and handicrafts was
Athena
Aphrodite
Demeter
Mnemosyne

The queen of the gods, and goddess of marriage, women, childbirth, heirs, kings and empires was
Hera
Athena
Hestia
Tethys

The god of music, arts, knowledge, healing, plague, prophecy, poetry, manly beauty, and archery was
Apollo
Zeus
Hades
Poseidon

The god of war, bloodshed, and violence.
Ares 
Hades
Apollo
Thanatos

The god of wine, fruitfulness, parties, festivals, madness, chaos, drunkenness, vegetation, ecstasy, and the theater was
Dionysus 
Eros
Ares 
Himeros

The god of boundaries, travel, communication, trade, language, thieves and writing was
Hermes
Dionysus
Apollo
Proioxis

The god of fire, metalworking, and crafts was 
Hephaestus
Hades
Ares
Apollo

Which Greek hunk fell in love with his own reflection in a pool of water? 
Narcissus
Adonis
Eros
Egon

Which bird was the symbol of the godess Athena?
Owl
Eagle
Seagull
Swallow
Sparrow


MEDIUM
The goddess of grain, agriculture, harvest, growth, and nourishmen was
Demeter
Rhea
Hestia
Hera

This titan feared a prophecy about being overthrown by his own child (like he overthrown his father). To prevent this he devoured all his children. However, his sister-wife tricked him with a stone and saved their youngest child who later overthrow him and saved his siblings. The titan's name was 
Cronos
Uranos
Chaos
Aether

Poseidon was Zeus'
Brother
Son
Uncle
Grandson

As *chthonic deity* could be described
Hades
Ares
Poseidon
Zeus

The golden apple with inscription *to the most beautiful* tossed in the midst of the feast of the gods caused strife between Hera, Athena and Aphrodite. Savvy Zeus refused to judge them an chose mortal Paris as their arbiter. Without knowing that the aftermatch of his decision will cause the Trojan war Paris picked 
Aphrodite
Hera
Athena
Neither

Artemis and Apollo were 
Twins
Secret lovers
Married
All three answers are correct

Cassandra was offered the gift to see the future by her divine suitor. She accepted the gift but then refused him. He cursed her causing that nobody believe her accurate prophecies and she was seen as a liar and a madwoman. Cassandra charmed god 
Apollo
Zeus
Hades
Dionysus

According to most versions, Hephaestus's (adulterous) consort was
Aphrodite
Athena
Artemis
Asteria

Two sons of Ares and Aphrodite - first of them personifying the feeling of dread and terror before a battle and the other fear and panic in mids of it - were 
Deimos and Phobos
Castor and Pollux
Ascalaphus and Ialmenus
Proetus and Acrisius

King *Acrisius* of Argos was told by the oracle of Delphi he will be killed by his grandson. His only daugher was imprisones in windowless chamber, however, that did not stop *Zeus* who transformed into golden rain to meet with her and impregnated her. The princess and the mother of *Perseus* was 
Danaë
Andromeda
Europa
Semele

Both *Zeus* and *Poseidon* fancied nereid (sea nymph) *Thetis*. However, the prophecy foretold her son will overshadow his father. Not wanting to risk, they made arrangements for her to marry a mortal, *Peleus*, king of Phthia, and she eventually gave birth to 
Achilles
Heracles
Odysseus
Perseus


HARD
The king of Crete *Minos* was about to sacrifice a snow-white bull to *Poseidon*. But in the last moment he decided to keep the bull for himself. The offended god made *Minos'* wife *Pasiphaë* fall in love with the bull. She hid in a hollow wooden cow to mate with the bull and conceive a monster *Minotaur*. *Minotaur* became a feroucious man-eater and was kept in a Labyrinth until it was slayed by 
Theseus
Perseus
Jason
Heracles

When Athena and Poseidon competed who will become the patron of this new city, Athena won and the city was named after her after she offered citizens
Olive tree
Alphabet 
Ship  
Law

Which goddess was born fully armed from *Zeus'* forehead?
Athena
Artemis
Nike
Nemesis

The titan *Cronus* was depicted with a sickle because
He castrated his father with it
He was a patron of agriculture and wine-making
He used it to gather magical herbs and laurel
He was a god of time and a prototype of Grim Reaper

The nereid *Thetis* was a mother of Greek hero
Achilles  
Ajax
Heracles
Perseus

*Arachne* was a woman cursed and transformed into a spider because she dared to 
Beat *Athena* in a competition
Refute *Apollo's* love advances
Cheat on *Hera* with *Zeus*
Kill a favourite animal of *Artemis*

*Nereides* are nymphs associated with 
Seas
Rivers and streams
Trees and forests
Evenings and sunsets

/*
//TODO - misleding dolphin too
Which animals are associated with Poseidon?
Horses
Dolphins
Snakes
Wolves 
*/

The Greek goddess/personification of the night was 
Nyx
Phoebe
Hecate
Asteria

*Calliope, Euterpe, Melpomene, Erato* and their five other sisters are 
Muses 
Nymphs
Furies
Charites

*Thalia* is the muse presiding over 
Comedy and pastoral poetry
Flutes and music
Love poetry and lyric poetry
Dance

*Clio* is the muse overseeing
History
Epic poetry 
Astronomy
Tragedy 

*Furies*, goddesses of vengeance, are also know under name
Erinyes
Moirai
Horai
Gorgons