Map of France

TAGS france map xmap country europe
SHARED On the map is highlighted

EASY
MAP fr;FR-E
Brittany (Bretagne)

MAP fr;FR-X4
Great East (Grand Est; Alsace-Champagne-Ardenne-Lorraine)

MAP fr;FR-J
Île-de-France

MAP fr;FR-X3
Normandy (Normandie)

MAP fr;FR-X5
Occitanie (Occitania; Languedoc-Roussillon-Midi-Pyrénées)

MAP fr;FR-U
Provence-Alps-Azure Coast (Provence-Alpes-Côte-d'Azur)

MAP fr;FR-X6
Upper France (Hauts-de-France; Nord-Pas-de-Calais-Picardie)


BLOCK

MAP fr MARK 48°51′24″N 2°21′08″E
Paris

MAP fr MARK 43°17′47″N 5°22′12″E
Marseille 

MAP fr MARK 45°46′N 4°50′E
Lyon

MAP fr MARK 43°36′16″N 1°26′38″E
Toulouse

MAP fr MARK 43°42′12″N 7°15′59″E
Nice

MAP fr MARK 47°13′05″N 1°33′10″W
Nantes

MAP fr MARK 48°35′00″N 07°44′45″E
Strasbourg

MAP fr MARK 44°50′N 0°35′W
Bordeaux


MEDIUM
MAP fr;FR-X7
Auvergne-Rhône-Alps (Auvergne-Rhône-Alpes)

MAP fr;FR-X1
Burgundy-Free County (Bourgogne-Franche-Comté)

MAP fr;FR-F
Centre-Loire Valley (Centre-Val de Loire)

MAP fr;FR-R	
Loire Countries (Pays de la Loire)

MAP fr;FR-X2
New Aquitaine (Nouvelle-Aquitaine; Aquitaine-Limousin-Poitou-Charentes)

BLOCK 

//size
MAP fr MARK 43°36′43″N 3°52′38″E
Montpellier

MAP fr MARK 50°37′40″N 3°03′30″E
Lille

MAP fr MARK 48°06′53″N 1°40′46″W
Rennes

MAP fr MARK 45°26′05″N 4°23′25″E
Saint-Étienne

MAP fr MARK 43°07′33″N 05°55′50″E
Toulon

MAP fr MARK 49°29′N 0°06′E
Le Havre

MAP fr MARK 45°10′18″N 5°43′21″E
Grenoble

//interesting
MAP fr MARK 48°41′37″N 6°11′05″E
Nancy

MAP fr MARK 47°23′37″N 0°41′21″E
Tours

MAP fr MARK 47°54′09″N 1°54′32″E
Orléans

MAP fr MARK 43°57′00″N 04°48′27″E
Avignon

MAP fr MARK 50°56′53″N 01°51′23″E
Calais

MAP fr MARK 49°15′46″N 4°02′05″E
Reims


HARD
MAP fr;FR-H
Corsica (Corse)

MAP fr;FR-GP
Guadeloupe

MAP fr;FR-GF
French Guiana (Guyane)

MAP fr;FR-MQ
Martinique

MAP fr;FR-YT
Mayotte

MAP fr;FR-RE
Réunion (La Réunion)

BLOCK

//size 
MAP fr MARK 47°19′00″N 5°01′00″E
Dijon

MAP fr MARK 47°28′25″N 0°33′15″W
Angers

MAP fr MARK 43°50′17″N 4°21′40″E
Nîmes

//TODO is not displayed (island)
//MAP fr;20°52′44″S 55°26′53″E
//Saint-Denis (Réunion)

//interesting
MAP fr MARK 49°53′31″N 2°17′56″E
Amiens

MAP fr MARK 49°07′13″N 6°10′40″E
Metz

MAP fr MARK 49°26′34″N 01°05′19″E
Rouen

MAP fr MARK 48°00′28″N 0°11′54″E
Le Mans

MAP fr MARK 48°23′N 4°29′W
Brest

MAP fr MARK 46°35′N 0°20′E
Poitiers

MAP fr MARK 46°09′33″N 1°09′06″W
La Rochelle

MAP fr MARK 51°02′18″N 2°22′39″E
Dunkirk

MAP fr MARK 43°31′35″N 5°26′44″E
Aix-en-Provence

MAP fr MARK 43°13′N 2°21′E
Carcassonne