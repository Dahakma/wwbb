Map of Italy

TAGS map xmap country europe
SHARED On the map is highlighted

EASY
MAP it;IT-82
Sicily (Sicilia)

MAP it;IT-21
Piedmont (Piemonte)

MAP it;IT-25
Lombardy (Lombardia)

MAP it;IT-88
Sardinia(Sardegna)

MAP it;IT-52
Tuscany (Toscana)

BLOCK

MAP it MARK 41°53′N 12°30′E
Rome (Roma)

MAP it MARK 45°28′01″N 09°11′24″E
Milan (Milano)

MAP it MARK 40°50′42″N 14°15′30″E
Naples (Napoli)

MAP it MARK 45°26′15″N 12°20′9″E
Venice (Venezia)

MAP it MARK 38°06′56.37″N 13°21′40.54″E
Palermo 

MEDIUM
MAP it;IT-34
Veneto

MAP it;IT-78
Calabria

MAP it;IT-72
Campania

MAP it;IT-42
Liguria

MAP it;IT-32
Trentino-South Tyrol (Trentino-Alto Adige)

MAP it;IT-62
Lazio

MAP it;IT-36
Friuli-Venezia Giulia

MAP it;IT-45
Emilia-Romagna

BLOCK

MAP it MARK 45°04′45″N 07°40′34″E
Turin (Torino)

MAP it MARK 44°24′40″N 8°55′58″E
Genoa (Genova)

MAP it MARK 44°29′38″N 11°20′34″E
Bologna

MAP it MARK 43°46′17″N 11°15′15″E
Florence (Firenze)

MAP it MARK 45°26′19″N 10°59′34″E
Verona

MAP it MARK 38°11′37″N 15°33′15″E
Messina

MAP it MARK 45°25′N 11°52′E
Padua (Padova)

HARD
MAP it;IT-57
Marche

MAP it;IT-67
Molise

MAP it;IT-65
Abruzzo

MAP it;IT-77
Basilicata

MAP it;IT-23
Aosta Valley (Valle d'Aosta)

MAP it;IT-75
Apulia (Puglia)

MAP it;IT-55
Umbria

BLOCK

MAP it MARK 41°07′31″N 16°52′0″E
Bari

MAP it MARK 37°30′0″N 15°5′25″E
Catania

MAP it MARK 45°39′01″N 13°46′13″E
Trieste

MAP it MARK 44°48′05.3″N 10°19′40.8″E
Parma

MAP it MARK 45°32′30″N 10°13′00″E
Brescia

MAP it MARK 40°28′N 17°14′E
Taranto

MAP it MARK 43°52′48″N 11°05′54″E
Prato

MAP it MARK 44°38′49″N 10°55′32″E
Modena

MAP it MARK 44°25′N 12°12′E
Ravenna

MAP it MARK 44°50′N 11°37′E
Ferrara

MAP it MARK 45°41′42″N 9°40′12″E
Bergamo