Central & South American Countries

TAGS map xmap america borders
SHARED On the map is highlighted

EASY
MAP ma;MX
Mexico

MAP ma;CU
Cuba

MAP ma;US
United States

MAP ma;PA
Panama

MAP ma;JM
Jamaica

MAP ma;TT MARK 10.6;-61.1
Trinidad and Tobago

MAP sa;BR
Brazil

MAP sa;CL
Chile

MAP sa;AR
Argentina

MAP sa;CO
Colombia

MEDIUM
MAP ma;PR
Puerto Rico

MAP ma;DO
Dominican Republic

MAP ma;NI
Nicaragua

MAP ma;HN
Honduras

MAP ma;HT
Haiti

MAP ma;GT
Guatemala

MAP sa;PE
Peru

MAP sa;VE
Venezuela

MAP sa;BO
Bolivia

MAP sa;EC
Ecuador

MAP sa;PY
Paraguay

HARD
MAP ma;SV
El Salvador

MAP ma;TT MARK 10.6;-61.1
Trinidad and Tobago

MAP ma;BZ
Belize

MAP ma;BS 25;-77.4
Bahamas

MAP ma;CR
Costa Rica

MAP ma;JM
Jamaica

MAP sa;SR
Suriname

MAP sa;CO
Colombia

MAP sa;GY
Guyana

MAP sa;UY
Uruguay

/*
MAP sa;FK
Falkland Islands (UK)
*/