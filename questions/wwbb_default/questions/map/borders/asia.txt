Asian Countries

TAGS asia map xmap borders
SHARED On the map is highlighted

EASY
MAP as;CN
China

MAP as;JP
Japan

MAP as;IN
India

MAP as;MN
Mongolia

MAP was;TR
Turkey

MAP was;PK
Pakistan

MAP was;IQ
Iraq

MAP as;ID
Indonesia

MAP was;SA
Saudi Arabia

MAP as;KZ
Kazakhstan

MEDIUM
MAP eas;HK MARK 22.3;114.2
Hong Kong

MAP eas;TW
Taiwan

MAP eas;LK
Sri Lanka

MAP eas;NP
Nepal

MAP eas;PH
Philippines

MAP was;AF
Afghanistan

MAP as;IR
Iran

MAP was;AM
Armenia

MAP was;SY
Syria

MAP eas;VN
Vietnam

MAP was;IL
Israel

MAP eas;KP
North Korea

MAP eas;KR
South Korea

MAP eas;MY
Malaysia

MAP eas;TH
Thailand

HARD
MAP eas;BD
Bangladesh

MAP eas;BN MARK 4.5;114.666667
Brunei

MAP was;BH MARK 26.216667;50.583333
Bahrain

MAP eas;BT
Bhutan

MAP was;JO
Jordan

/*
MAP was;32;35.25;PS
Palestine
*/

MAP was;LB MARK 33.833333;35.833333
Lebanon

MAP eas;LA
Laos

MAP eas;TL MARK -8.55;125.56
East Timor

MAP was;TM
Turkmenistan

MAP was;TJ
Tajikistan

MAP was;AE
United Arab Emirates

MAP was;GE
Georgia

MAP was;AZ
Azerbaijan

MAP was;OM
Oman

MAP was;KG
Kyrgyzstan

MAP was;UZ
Uzbekistan

MAP eas;MM
Myanmar

MAP eas;SG MARK 1.283333;103.833333
Singapore

MAP eas;KH
Cambodia

MAP was;CY
Cyprus

MAP was;QA
Qatar

MAP was;KW
Kuwait

MAP was;YE
Yemen