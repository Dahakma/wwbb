Computers

TAGS technology

EASY
The *byte* is a unit of digital information. It generally consists of
8 bits
10 bits
12 bits
4 bits

*.png* or *.bmp* file extensions are used by 
Images 
Documents
Audio
Video

*.rtf* or *.odt* file extensions are used by 
Documents
Images
Audio
Video

1 KB is equal to
1024 Bytes 
1000 Bytes
1244 Bytes 
1064 Bytes 

Tux, the mascot of Linux, is 
Penguin
Red Panda
Elephant
Owl

MEDIUM
RAM is an abbreviation for 
Random-access memory
Restricted-access memory
Rapid-access memory
Read and memorise

The *Unix time* represents the number of seconds that have passed since 00:00:00 UTC on 1 January
1970
1960
1980
1950

The common fictional characters used in cryptography examples are 
Alice and Bob
Aubrey and Bill
Adam and Betty
Alex and Briana

The most common capacity of the 3½-inch floppy disk - storage medium introduced in 1986, now known mostly as the *save* icon - was 
1.44 MB
4.8 MB
2.56 MB
1 MB

/*
//TODO - is true????
Which of the following versions of *Microsoft Windows* is made up? 
Windows 97
Windows 95
Windows Me
Windows XP
*/

HARD
The first email was sent by Ray Tomlinson in
§8§1971§>1965§@y

The Advanced Research Projects Agency Network (ARPANET), the precursor of the Internet became operational in
§8§1969§>1959§@y

The best current Internet browser is 
Vivaldi
Mozilla Firefox
Microsoft Edge
Google Chrome