European Capitals

TAGS europe cities
SHARED The capital city of XXX is

EASY
United Kingdom
London

France
Paris

Germany
Berlin

Greece
Athens

Russia
Moscow

Italy
Rome

Austria
Vienna

Portugal
Lisbon

Spain
Madrid

MEDIUM
Ukraine
Kyiv

Serbia
Belgrade

Belgium
Brussels

Ireland
Dublin

Denmark
Copenhagen

Finland
Helsinki

Norway
Oslo

Iceland
Reykjavik

Sweden
Stockholm

Hungary
Budapest

Czech Republic
Prague

Poland
Warsaw

HARD
Slovakia
Bratislava

Liechtenstein
Vaduz

Slovenia
Ljubljana

Belarus
Minsk

Latvia
Riga

Estonia
Tallinn

Georgia
Tbilisi

Lithuania
Vilnius

Armenia
Yerevan

Romania
Bucharest

Cyprus
Nicosia

Montenegro
Podgorica

Bosnia and Herzegovina
Sarajevo

North Macedonia
Skopje

Bulgaria
Sofia

Albania
Tirana

Malta
Valletta

Croatia
Zagreb

Moldova
Chisinau