import * as fs from 'fs';
import * as client from 'https';
//const client = require('https');
import cats from '../src/data/questions.json'  assert { type: 'json' };

const images_folder =  '../public/images' 




//VARIABLES 
let errors = 0;
let warnings = 0;
let downloaded = 0;
let report = [];
let pending = [];

//LOGGING 
import {log, color} from './fancy_log.js' ;
log.good = function(input){
	downloaded++;
	console.log(`${input.cat} - ${input.link}${color.green} DOWNLOADED${color.reset}`);
	if(downloaded + errors >= pending.length) log.done();
}

log.bad = function(input, error){
	errors++;
	console.log(`${input.cat} - ${input.link}${color.red} ERROR ${error}${color.reset}`);
	if(downloaded + errors >= pending.length) log.done();
}

log.done = function(){
	this.white("***************************");
	this.green(`${downloaded} IMAGES DOWNLOADED`);
	if(errors) this.red(`${errors} ${errors === 1 ? "ERROR" : "ERRORS"}`);
	//if(warnings) this.yellow(`${warnings} ${warnings === 1 ? "WARNINGS" : "WARNINGS"}`); 
	this.white("***************************");
}









//creates main /public/images folder
if (!fs.existsSync(images_folder)){
    fs.mkdirSync(images_folder);
}




cats.forEach( cat => {
//	console.log( "  " );
//	log.name(cat.name);
	
	const folder = `${images_folder}/${cat.id}`
	
	if( !fs.existsSync(folder) ){
		fs.mkdirSync(folder);
	}

	const questions = [...cat[0], ...cat[1], ...cat[2]];
	questions.forEach( que => {
		if(que.img){
			const link = encode(que.img);
			const name = parse_url(link);
			const path = `${folder}/${name}`;

			if(!fs.existsSync(path)){
				
				pending.push({
					link, 
					name, 
					path, 
					cat: cat.id, 
				})
			}

			
/*			console.log( "===" );
		
			console.log( link );
			
			
*/
			
			
			
//console.log(link);
			//download(link,  ).then(log.good).catch(log.bad);
		}
	})	
		
});



pending.forEach( a => {

	download(a.link, a.path).then(
		()=> log.good(a)
	).catch(
		(err)=> log.bad(a, err)
	);

});






//https://scrapingant.com/blog/download-image-javascript
function download(url, filepath) {
    return new Promise((resolve, reject) => {
        client.get(url, (res) => {
            if (res.statusCode === 200) {
                res.pipe(fs.createWriteStream(filepath))
                    .on('error', reject)
                    .once('close', () => resolve());
            } else {
                // Consume response data to free up memory
                res.resume();
				//reject(new Error(`Request Failed With a Status Code: ${res.statusCode}`));
				reject(res.statusCode);
            }
        });
    });
}



function parse_url(url){
	let out = url.split(".");
	const extension = out.pop();
	out = out.join("_").replace("://", "_").replaceAll("/", "_");
	out = `${out}.${extension}`;
	if(out.length > 50) out = out.substr(-50);
	return out;
}



//TODO - THIS WILL DEFINITELY CAUSE TRUBLES, HAS TO BE THE SAME AS IN QUESTIONS
//don't remember point of this but it makes some pictures work
function encode(adress){
	if(!adress) return adress;
	
	 adress = adress.replace("§WCT", "https://upload.wikimedia.org/wikipedia/commons/thumb");
	 
	adress = encodeURIComponent(adress)

	adress = adress.replace("%3A", ":");
	adress = adress.replace(/%2F/g, "/");
	adress = adress.replace(/%25/g, "%");
	adress = adress.replace(/%E2%80%99/g, "'");
	adress = adress.replace(/%3F/g, "?"); //TODO CHECK
	adress = adress.replace(/%3D/g, "="); //TODO CHECK

	return adress;
}





//import {database} from '../src/data/questions.json'  assert { type: `json` };
//const cat = require( '../src/data/questions.json' )
/*
import * as fs from 'fs';
const json = fs.readFile( '../src/data/questions.json', (err) => err && console.error(err));

console.log(json);
*/

//console.log(database);
//const cat = JSON.parse(json)



//import info from `./package.json` assert { type: `json` };

//const cat = JSON.parse(database);



//import tags from 'Data/tags.json';


//console.log("ey");
//console.log(cat);