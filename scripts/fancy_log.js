export const color = {
	reset: "\u001b[0m",
	yellow: "\u001b[33;1m",
	green: "\u001b[32;1m",
	blue: "\u001b[36;1m",
	red: "\u001b[31;1m",
	white: "\u001b[37;1m",
	grey: "\u001b[37m",
}

export const log = {
	blue(input){
		console.log(`${color.blue}${input}${color.reset}`);
	},
	yellow(input){
		console.log(`${color.yellow}${input}${color.reset}`);
	},
	grey(input){
		console.log(`${color.grey}${input}${color.reset}`);
	},
	red(input){
		console.log(`${color.red}${input}${color.reset}`);
	},
	green(input){
		console.log(`${color.green}${input}${color.reset}`);
	},
	white(input){
		console.log(`${color.white}${input}${color.reset}`);
	},
}