import * as fs from 'fs';
import {log} from './fancy_log.js' ;
import {var_to_string} from '../src/libraries/dh/index.js' ;

const header = `//this file was automaticaly generated by running "npm run index" or "npm run x" `;


log.green(`* * * * * * * * * * * * * * * * *`);


//NAMES 
{
	let out = [];
	const path =  "./src/data/names";
	
	out.push(header);

	fs.readdirSync(path).filter( folder => {
		if(folder === "index.js") return;
			fs.readdirSync(`${path}/${folder}`).filter( file => {
					file = file.split(".")[0];
					out.push( `import ${folder}_${file} from './${folder}/${file}.json'; `);
			});
	});

	out.push(` `);
	out.push(`export const names = {`);
	
	fs.readdirSync(path).filter( folder => {
		if(folder === "index.js") return;
		out.push( `	${folder}: { `);
			fs.readdirSync(`${path}/${folder}`).filter( file => {
					file = file.split(".")[0];
					out.push( `		${file}: ${folder}_${file}, `);
			});
		out.push( `	}, `);
	});

	out.push(`} `);

	fs.writeFileSync(`${path}/index.js`, out.join("\n"), (err)=> {
	  if (err) throw err;
	});

	log.green(`Random character names indexed`);
}

log.green(`MISC INDEXED`);
log.green(``);


//CATEGORIES
{
	let out = [];
	const path =  "./src/data/questions";
	const cats = fs.readdirSync(path).filter( a => a !== "index.js" );
	
	out.push(header);

	cats.forEach( cat => {
		out.push( `import ${cat}_questions from './${cat}/questions.json'; `);
		out.push( `import ${cat}_tags from './${cat}/tags.json'; `);
		out.push( `import ${cat}_meta from './${cat}/meta.json'; `);
	});

	out.push(` `);
	out.push(`export const categories = {`);

	cats.forEach( cat => {
		out.push( `	${cat}: { `);
		out.push( `		questions: ${cat}_questions, `);
		out.push( `		tags: ${cat}_tags, `);
		out.push( `		meta: ${cat}_meta, `);
		out.push( `	}, `);
	});

	out.push(`} `);
	
	fs.writeFileSync(`${path}/index.js`, out.join("\n"), (err)=> {
	  if (err) throw err;
	});

	log.green(`Categories of questions indexed`);

}

log.green(`CATEGORIES INDEXED`);
log.green(``);





//TEXTS

[
	"character_intros",
	"transformations",
	"banter",
	"epilogues",
].forEach( text => {
	let out = [];
	const path =  `./src/data/speeches/${text}`;
	const files = fs.readdirSync(path).filter( a => a !== "index.js" );
	
	out.push(header);

	files.forEach( file => {
		const name = file.split(".")[0];
		out.push( `import * as ${name} from './${file}'; `);		
	});
	
	out.push(`export const ${text} = {`);
	
	files.forEach( file => {
		const name = file.split(".")[0];
		out.push(`	...${name},`);		
	});
	
	out.push(`};`);

	fs.writeFileSync(`${path}/index.js`, out.join("\n"), (err)=> {
	  if (err) throw err;
	});


	log.green(`${var_to_string(text)} indexed`);
})
log.green(`SPEECHES INDEXED`);
log.green(``);

log.green(`DONE!`);
log.green(`* * * * * * * * * * * * * * * * *`);




