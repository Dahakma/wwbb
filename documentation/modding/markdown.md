# Markdown

Texts use Markdown-like syntax

|  code |  in HTML |  displayed  |
|:-:|:-:|:-:|
| `**something**`  | `<b>something</b>`  |  <b>something</b>  |
| `*something*`  | `<i>something</i>`  |  <i>something</i>  |
| `{something}`  | `<small>something</small>`  |  <small>something</small>  |
| `~something~`  | `<strike>something</strike>`  |  <strike>something</strike>  |
| `mc^2`  | `mc<sup>2</sup>`  |  mc<sup>2</sup>  |
| `h˅2`  | `h<sub>2</sub>`  |  h<sub>2</sub>  |

System texts (Errata, Changelog, Credits, etc.) also use block elements 

|  code |  in HTML |  displayed  |
|:-:|:-:|:-:|
| `# something`  | `<h1>something</h1>`  |  <h1>something</h1>  |
| `### something`  | `<h3>something</h3>`  |  <h3>something</h3>  |
| `- something`  | `<ul><li>something</ul>`  |  <ul><li>something</ul> |


### Unit Recounting 

Special syntax `number@symbol` can change units between SI units (centimetres) and US customary (inches) based on settings.

```
1@cm
```

Will be displayed as `1 centimetre` or `0.39 inches` 

Allowed couples of symbols are: 

- `cm` & `in`
- `m` & `ft`
- `m2` & `ft2`
- `m3` & `ft3`
- `km` & `mi`
- `km2` & `mi2`
- `c` & `f`

<!---
1@cm = 0.393701 in
1@m = 3.28084 ft
1@m2 = 10.7639 ft2
1@m3 = 35.3147 ft3
1@km = 0.621371 mi
1@km2 = 0.386102 mi2
1@c = 33.8 f

1@in = 2.54 cm
1@ft = 0.3048 m
1@ft2 = 0.092903 m2
1@ft3 = 0.0283168 m3
1@mi = 1.60934 km
1@mi2 = 2.58999 km2
1@f = -17.2222 c


100@cm = 39.3701
100@m = 328.084 ft
100@m2 = 1076.39 ft2
100@m3 = 3531.47 ft3
100@km = 62.1371 mi
100@km2 = 38.6102 mi2
100@c = 212 f

100@in = 254 cm
100@ft = 30.48 m
100@ft2 = 9.2903 m2
100@ft3 = 2.83168 m3
100@mi = 160.934 km
100@mi2 = 258.999 km2
100@f = 37.7778 c


-->





 [🔙 Back to Index](index.md)

<!---


### Cheatsheet

|  code |  alt + |  ctrl + shift + u + |  |
|:-:|:-:|:-:|:-:|
| `~`  | 126  | 7E | tilde |
| `†`  | 135  | 2020 | dagger |
| `^`  | 94  | 5E | carret |
| `˄`  | . | 2C4 | up arrowhead |
| `˅`  | . | 2C4 | down arrowhead |




| `\``  | 96  |  60  | grave accent
--->