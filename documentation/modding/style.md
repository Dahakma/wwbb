# Style Guide 

The linter is set up to follow my quirky style of writing code and the stuff that works for me. Here is the quick rundown: 

- use **tabs for indentation**; the purpose of tabs is to indentation, the purpose of spaces is to separate words
- use **semicolons**; can prevent some general problems caused by stuff like:
- one-line statements without brackets are fine `if(variable) do_someting();`
- use `const` when you can; do not use `var`
- in **switch** use commented out `//break;` to make omission of `break` explicit & use `default:`
- use **trailing commas** in multiline objects and arrays; forgetting the comma is like the most common bug I make 
	```js
	const something = [
		"first",
		"second",
	];
	```
- if you want to keep **console** in production use 
	```js
	/* eslint-disable no-console */

	/* eslint-enable no-console */
	```
- if you want keep **alert** in production use 
	```js
	/* eslint-disable no-alert */

	/* eslint-enable no-alert */
	```
- I usually use *snake_case* because it is the most aesthetically pleasing and make the most sense when you dynamically create names of variables e.g.
	```js
	["hue","saturation","lightness"].forEach( key => a[`highlight_${key}`] = a[key] )
	```

[🔙 Back to Index](index.md)


