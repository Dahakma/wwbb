# Used Tools

- the game is made with **Javascript**
- to speed up the writing of code and simplify handling DOM (HTML) it uses **Svelte** framework
- the project is bundled (put together) by **Vite**
- linting (checking for bad code) is done by **ESLint** with custom configuration

<hr>

- to display maps is used **jVectorMap** respective *jvectormap-next* respective *jvectormap-next-module* respective my fork of it that somehow works
- for charts is used **Chart.js**, respective *svelte-chartjs*

<hr>


[🔙 Back to Index](index.md)
