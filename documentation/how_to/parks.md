# I want to add questions guessing famous theme parks by their location on a map

## 1. Create locations codes

- to display a mark on a map, game uses code in format 

```
MAP code_of_map; latitude; longitude
```

- latitudes and longitudes could be found on Wikipedia or GeoHack. Most of possible formats are accepted:

```
MAP usa; 33.81°N 117.92°W
MAP usa; 33° 48′ 36″ N, 117° 55′ 12″ W
MAP usa; 33.81, -117.92
MAP usa; 33.81; -117.92
```
- currently available maps are:
- `af` or `afr` or `africa`  - Africa
- `na` - North America
- `sa` - South America
- `ma` - Middle America (zoomed map of North America)
- `us` or `usa` - USA
- `ca` or `canada` - Canada
- `eas` - east Asia from Chinese western border (Asia)
- `was` - west Asia to Indian eastern border (Asia)
- `as` or `asia` - Asia
- `au` or `australia` - Australia
- `oc` - Oceania
- `eu` or `europe` - Europe
- `ceu` - zoomed Europe without Island and Russia (Europe)
- `at` or `austria` - Austria 
- `fr` or `france` - France
- `de` or `germany` - Germany
- `it` or `italy` - Italy
- `pl` or `poland` - Poland
- `ua`  or `ukraine` - Ukraine (zoomed Europe)
- `uk` or `england` - United Kingdom
- `w` or `world` or anything else - World
- `medi` - Mediterranean region  (zoomed World)



## 2. Create the file with questions

- it is a normal text file `theme_parks.txt` (the name should not contain space)
- the first line in the file is the name of the category
- since all the questions will be map-based, we add tag `map` to `TAGS` (it automaticaly adds icon of globe to the name of the category or disable the category if map-based questions are disabled in settings)
- also tag `poi` - places of interest, *maps with interesting places*

```
Theme Parks 

TAGS map poi
AUTHOR your_nickname
SHARED On the map is
```

- since all questions will be the same - *On the map is* - we will use `SHARED` to give all questions the same text
- we can add incorrect answers 

```
MAP usa; 33.81; -117.92
Disneyland Park
Universal Studios Hollywood
Six Flags Magic Mountain
Knott's Berry Farm
```

- but if there is only one (correct) answer, the game will use answers to other questions 
- tags `EASY`, `MEDIUM` and `HARD` divide questions into three difficulties, the question share answers only within the same difficulty, so it is strongly recomended to have at least four or more questions within each block

```
MAP usa; 33.81; -117.92
Disneyland Park

MAP usa;34.136518, -118.356051
Universal Studios Hollywood

MAP usa; 34.427°N 118.597°W
Six Flags Magic Mountain

MAP usa; 33° 50′ 39.04″ N, 118° 0′ 0.96″ W
Knott's Berry Farm
```

- the result could be something like 

```
Theme Parks 

TAGS map
AUTHOR your_nickname
SHARED On the map is

EASY
MAP usa; 33.81; -117.92
Disneyland Park

MAP usa;34.136518, -118.356051
Universal Studios Hollywood

MAP usa; 34.427°N 118.597°W
Six Flags Magic Mountain

MAP usa; 33° 50′ 39.04″ N, 118° 0′ 0.96″ W
Knott's Berry Farm


MEDIUM 
MAP ceu; 48.268333; 7.720833
Europa-Park

MAP ceu; 48.8726083°N 2.7767472°E
Disneyland Paris

HARD 
MAP usa; 28°24′39″N 81°27′45″W
SeaWorld Orlando
Six Flags Great Adventure
Canada's Wonderland
Hersheypark
```


## 2. Add the file to the game

- put the file into `/questions/wtrivia_default/questions/`
- you can just put it there or into one of the folders (e.g. `/questions/wtrivia_default/questions/sundry/theme_parks.txt`)

## 2.5 Mark as new category (optional)

- the id of the question could be added to the list of the newest questions - `/questions/wtrivia_default/newest.js`
- the id is either the name of the file (`/questions/wtrivia_default/questions/theme_parks.txt` => `theme_parks`) or folder_file (`/questions/wtrivia_default/questions/sundry/theme_parks.txt` => `sundry_theme_parks`)

## 3. Compile questions

- open console and run `npm run q`

## 3. Run the game

- run in the console `npm run start`
- check your new category 
- done

For more details see [Questions](./modding/questions.md)
[Back](index.md)