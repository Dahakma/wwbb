# Quick How To Quide 

* [Back to README](./README.md)
* [I Want to Mod the Game!](start.md)

### Questions 

* [I Want to Add Questions about Teletubbies!](teletubbies.md)
* [I Want to Add My Own Database of Questions Only about Videogames!](database.md)

### Flavour

* [I Want to Add Georgian Lari as Currency!](currency.md)
* [I Want to Add Simple Black-White-Red Available Color Scheme!](color_scheme.md)
* [I Want to Add Dutch Names!](names.md)
* [I Want to Replace Bimbo Names with Elvish Names!](elvish_names.md)

### Advanced

* [I Want to Edit Errata/Changelog/Credits!](system_texts.md)
* [I Want to Change the Way the Weight of Categories or Tags is Calculated!](weight.md)
* [I Want to Change the Way the Difficulty of Questions is Calculated!](difficulty.md)