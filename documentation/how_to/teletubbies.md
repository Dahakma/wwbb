# I Want to Add Questions about Teletubbies!

## 1. Create the File with the Questions 

- it is a normal text file `teletubbies.txt` (the name should not contain spaces or fancy symbols)
- the basic format is 

```
Text of the question. 
Correct answer.
Incorrect answer.
2nd incorrect answer.
n-th incorrect answer. 
Empty line dividing questions 
```

- the correct answer have to be first, do not forget about that!
- the first line in the file is the name of the category
- tags `EASY`, `MEDIUM` and `HARD` divide questions into three difficulties 
- so you can create something like

```
Teletubbies

TAGS tv uk
AUTHOR your_nickname

EASY
The purple teletubbie is 
Tinky Winky
Po
Laa-Laa
Dipsy

The green one is 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/View_of_the_Teletubbies_on_top_of_a_Cbeebies_Village_Daimler_Fleetline_bus_in_the_Hamley's_Toy_Parade_(geograph_5200061).jpg/542px-View_of_the_Teletubbies_on_top_of_a_Cbeebies_Village_Daimler_Fleetline_bus_in_the_Hamley's_Toy_Parade_(geograph_5200061).jpg
Dipsy
Tinky Winky
Po
Laa-Laa

Tinky Winky has a triangular antenna
Yes
No

MEDIUM 
The sentient vacuum cleaner is named 
Noo-Noo
Moo-Moo
Boo-Boo
Poo-Poo

HARD 
The first episode of Teletubbies launched on 31 March 
1997
2000
2003
2006
2009
```

- tag `TAGS` could be ommited
- between text of the question and the correct answer could be a link to an image, it has to follow `IMG` tag
- there should be more questions, at least 15 (3 x 5)


## 2. Add the File to the Game

- put the file into `/questions/wwbb_default/questions/`
- you can just put it there
- or into one of the folders (`/questions/wwbb_default/questions/art/teletubbies.txt`)
- or create your own folders e.g. `kids_shows` (`/questions/wwbb_default/questions/aliens/kids_shows.txt`)

## 2.5 Mark as New Category (optional)

- the id of the question could be added to the list of the newest questions - `/questions/wwbb_default/meta.js`
```js
export const meta = {
	name: `Default questions`, 
	desc: `Default database of questions for WWBB`,
	newest: [
		`teletubbies`, 
	]
};
```
- the id is either the name of the file (`/questions/wwbb_default/questions/teletubbies.txt` => `teletubbies`) or folder_file (`/questions/wwbb_default/questions/art/teletubbies.txt` => `art_teletubbies`)
- it changes the name into `Teletubbies (*new*)` and boost the chance of category to be offered during the game

## 3. Compile Questions

- open console (in the root directory) and run `npm run q`
- if you made up tags or created own folder (the folder name is used as a tag too) the script will complain about non-standard tags. Ignore the warnings or add your new tags into `/questions/wwbb_default/tags.js`

## 3. Run the Game

- run in the console `npm run start`
- check your new category 
<!---
	- check the credits 
-->
- done

For more details see [Questions](./documentation/modding/questions.md)

[🔙 Back to Index](index.md)
