# I Want to Replace Bimbo Names with Elvish Names! (TODO WIP) (UNTESTED)

There are two ways to do this: 

1. open file `/src/data/misc/names/wwbb_default/bimbo.json`

2. replace bimbo names with elvish names, the resulting file: 

```js
[
	"Anairë",
	"Arien",
	"Artanis", 
	"Arwen",
	"Celebrían",
	"Eärwen",
	"Elenna",
	"Elerrína",
	"Galadriel",
	"Irimë",
	"Laurelin",
	"Lúthien",
	"Míriel",
	"Mithrellas",
	"Naira",
	"Vána",
	"Vanya"
]
```

3. disadvantage is that the original bimbo names are replaced and no longer available 


The other way is similar to [I Want to Add Dutch Names!](names.md): 

1. create new folder `elvish` in  `/src/data/misc/names/`

2. create new folder `bimbo.json`  (`/src/data/misc/names/elvish/bimbo.json`) with your elvish names
```js
[
	"Anairë",
	"Arien",
	"Artanis", 
	"Arwen",
	"Celebrían",
	"Eärwen",
	"Elenna",
	"Elerrína",
	"Galadriel",
	"Irimë",
	"Laurelin",
	"Lúthien",
	"Míriel",
	"Mithrellas",
	"Naira",
	"Vána",
	"Vanya"
]
```

3. in the root directory `/` open console and run command `npm run x` 

4. switch to *Elvish* in *Settings > Style > Flavour*

5. disadvantage is that you cannot combine *Elvish* with other sets of names (e.g. *Wwbb Default* or *German* will still use the original bimbo names from `/src/data/misc/names/wwbb_default/bimbo.json`)

[🔙 Back to Index](index.md)