# I Want to Edit Errata/Changelog/Credits!

1. open `/errata.md` or `/changelog.md` or `/credits.md`
2. edit the file, [Markdown-like](./documentation/modding/markdown.md) syntax could be used
3. done. Files could be accessed as *Markdown* on *Gitlab*, or are automaticaly loaded as *HTML* into the game 

[🔙 Back to Index](index.md)
