# I Want to Add My Own Database of Questions Only about Videogames! (TODO UNTESTED)

1. go to `/questions/` and create a folder with name like `videogames`  (`/questions/videogames/` )
2. create file with metadata `/questions/videogames/meta.js` 

```js
export const meta = {
	name: `Questions about Videogames`, 
	desc: `My Own Database with Questions Only about Videogames`,
	newest: [], 
};
```
3. create file with tags `/questions/videogames/tags.js` 
	- `newest` and `images` tags have hard-coded effects (`newest` is auto added to categories mentioned in `meta.js` and boosts them; questions with `images` could be disabled)
	- `name` - optional, capitalized key is used if not defined 
	- `desc` - optional, short description 
	- `hidden` - optional (default false), not displayed the the player 
	- `suffix` - optional, icon added behind the name
	
```js
export const tags = {
	newest: {
		name: `newest`,
		desc: `recently addded category`,
		hidden: true,
	},

	images: {
		name: `Recognise Image 📷`,
		desc: `involves images`,
		suffix: `📷`,
	},

	rpg: {
		desc: `role-playing games`,
	},

	shooters: {}, 
	
	strategies: {}, 
};
```

4. create folder `questions`  (`/questions/videogames/questions`)
5. create files with questions; they could be put directly into the folder `questions` or into subfolders 
	- names of subfolders are added as tags and used in ids of categories, e.g.  `/questions/videogames/questions/rpg/tes/morrowind.txt` has id `rpg_tes_morrowind` and auto-added tags `rpg` and `tes`

6. final structure might look like

```
	/
	--questions/
	----videogames
	------meta.js
	------tags.js
	------questions/
	--------factorio.txt
	----------rpg/
	------------mass_effect.txt
	--------------tes/
	-----------------morrowind.txt
	-----------------skyrim.txt
	----------shooters/
	------------doom.txt
	--------baba_is_you.txt
```
	
7. in the root folder (`/`) open console and run `npm run q` to compile all the questions (into `src/data/questions/videogames/`)
8. run `npm run index` to auto add links to the new database to  `src/data/questions/index`
9. start the game (`npm run start`), the new database should be available in *Settings > Questions*

[🔙 Back to Index](index.md)




