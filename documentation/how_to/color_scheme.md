# I Want to Add Simple Black-White-Red Available Color Scheme! (TODO WIP)

1. open file `/src/data/misc/color_schemes.js`
2. add your scheme (the key is turned into name `evil_witch => Evil Witch`)

```js
	black_red: {
		text: "black",
		walter: "black",
		bgr: "white",
		high: "red",
		hoover: "red"
	}, 
```
3. it is now available in *Settings > Style > Colours*

[🔙 Back to Index](index.md)