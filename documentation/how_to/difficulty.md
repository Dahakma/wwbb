# I Want to Change the Way the Difficulty of Questions is Calculated!

- basic adjustement could be done without coding in *PLAY > Difficulty* 

<!-- -->

1. open file `/src/system/questions/difficulty.js`
2. the difficulty is influenced by four variables 
	- `how_many` - how many questions will there be in this round
	- `index` - index of this question (e.g. the first question in this round);
	- `default_difficulty` - user adjusted difficulty (from *PLAY > Difficulty*)
		- `-1` - Easier
		- `0` - Normal
		- `1` - Harder
	- `dumb` - effect on mental transformations on the player (if enabled)
		- `-1` - Smart
		- `0` - Normal
		- `1` - Slightly lowered IQ
		- `2` - Significantly lowered IQ
3. the returned number decides the difficulty level of the question 
	- `0` - Easy
	- `1` - Medium
	- `2` - Hard

[🔙 Back to Index](index.md)