# I Want to Write Game Introduction 

1. go to `/src/data/texts/game_intro` and create a new file `/src/data/texts/game_intro/name_does_not_matter.js`

2. write the introduction 


create new folder `dutch` in  `/src/data/misc/names/`
2. add json file `/src/data/misc/names/dutch/male.json` with common male names
	- json file is basicaly text file with extension `.json`
	- in jsons there are no traling commas
	- pls do not manually copy-paste hundred names from a table, send me message I have scripts to handle that 
	
```js
[
	"Jan",
	"Peter",
	"Hans",
	"Rob",
	"Henk",
	"Paul",
	"Jeroen",
]
```


	
4. add json file `/src/data/misc/names/dutch/female.json` with common female names 

```js
[
	"Monique",
	"Karin",
	"Sandra",
	"Esther",
	"Linda",
	"Petra",
	"Yvonne"
]
```

5. (optional) you can also add 
	- uncommon male names (`/src/data/misc/names/dutch/male_all.json`)
	- uncommon female names (`/src/data/misc/names/dutch/female_all.json`)
	- bimbo names (`/src/data/misc/names/dutch/bimbo.json`)
	- goth names (`/src/data/misc/names/dutch/goth.json`)
	- if you do not, defalut (from `/src/data/misc/names/wwbb_default/`) will be used instead 

6. in the root directory `/` open console and run command `npm run index`
7. Dutch names are now available in *Settings > Style > Flavour*

[🔙 Back to Index](index.md)