# I Want to Add Georgian Lari as Currency 

1. open file `/src/data/misc/currencies.js`
2. add Lari

```js
gel: {
	symbol: "₾",
	name: "Lari",
},
```
3. it is now available in *Settings > Style > Flavour*

[🔙 Back to Index](index.md)
