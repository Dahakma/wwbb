# I Want to Mod the Game! 

- the game is made with Javascript and the basic knowledge of Javascript is beneficial but it is not required 
- TODO - I did not yet test whether this process works but I think it should

## 1. Get Node.js and npm 

- to work with the game you will need Node.js and NPM. **Node.js** is a JavaScript *runtime environment* which allows to execute Javascript code without a browser. You can get it from the [nodejs.org](https://nodejs.org/). Together with Node gets installed **npm** - *Node Package Manager*. Basically, any tool you can imagine was already made by somebody else. And you do not have to reinvent the wheel, you can just use the npm to add it to your project. 


## 2. Get the Game 

- download or fork it from [gitlab.com/Dahakma/wwbb](https://gitlab.com/Dahakma/wwbb).


![where_is_download_button](images/download.png)


## 3. Instal Dependencies

- open the terminal and write `npm install`
- TODO - how to open terminal


## 3. Run the Game

- write `npm run start`
- the game will open in your browser and reloads automatically every time you change the game files in `/src/` (source) folder (except sometimes when it does not)
- the command `npm run build` will build the finished game, bundles all files together and puts them in `/dist/` (distribution) folder


[🔙 Back to Index](index.md)
