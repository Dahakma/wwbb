# Popup

- creates custom prompts 

## Usage 

- add `Component.svelte` to the svelte file
- import `popup` from `popup/index` in javascript 

## Prompts 


### Alert 

```javascript
popup.alert(text, fce);
```
- alternative to `window.alert()`
- text {string} [= "Beware!"] - displayed message 
- fce {function} [= ()=>{}] - function triggered after confirming

### Confirm 

```javascript
popup.alert(text, fce);
```
- alternative to `window.confirm()`
- text {string} [= "???"] - displayed message 
- fce {function} [= (input)=>{}] - function triggered with input `true` or `false` after confirming or denying

### Prompt 

```javascript
popup.prompt(text, default_value, fce, options);
```
- alternative to `window.prompt()`
- text {string} = "???" - displayed message 
- value {string} = "" - default value
- fce {function} = (value)=>{} - function triggered with input `value` after confirming or default value if cancelled
- options {string[]} = [] - options that could be randomly offered 


### Options 

```javascript
popup.prompt(text, options, fce);
```
- TODO
- text {string} = "???" - displayed message 
- options {object[]} = [] - offered options; they have `options[].text` and `options[].fce` (triggered *after* fce)
- fce {function} = (value)=>{} - function triggered with input = selected option when confirmed


### Splitter

```javascript
popup.splitter(text, max, fce, value = undefined);
```

- text {string} = "" - displayed message 
- max {integer} = 1 - maximal value
- fce {function} = (value)=>{} - function triggered with input `value` after confirming or `0` if cancelled
- value {integer} = undefined - defaultly max/2


