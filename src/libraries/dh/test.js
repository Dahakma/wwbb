
//const {cap} = require( './src/Dh');

//const {cap} = import ( './index');
import { describe, it, assert, expect } from 'vitest'
import {
	cap, 
	vowel, 
	clamp, 
	parse_geodata,
	clone, 
	update,
} from 'Dh';

//console.log(typeof cap)
//console.log(cap)

describe("cap", ()=> {
	it(`name => Name`, () => {	assert.equal( cap("lorem"),  "Lorem") 	})
	it(`NAME => NAME`, () => { assert.equal( cap("NAME"),  "NAME") 	})
	it(`undefined => ""`, () => { assert.equal( cap(undefined),  "") 	})
	it(`null => ""`, () => { assert.equal( cap(null),  "") 	})
	it(`123 => ""`, () => { assert.equal( cap(123),  "123") 	})
})

describe("vowel", ()=> {
	it(`a`, () => {	assert.equal( vowel("a"),  true) 	})
	it(`b`, () => {	assert.equal( vowel("b"),  false) 	})
	it(`undefined`, () => {	assert.equal( vowel(),  false) 	})
})

describe("clamp", ()=> {
	it(`5 >0 <10`, () => {	assert.equal( clamp(5, 0, 10),  5) 	})
	it(`20 >0 <10`, () => {	assert.equal( clamp(20, 0, 10),  10) 	})
	it(`-20 >0 <10`, () => {	assert.equal( clamp(-20, 0, 10),  0) 	})
	it(`20 <10 >0`, () => {	assert.equal( clamp(20, 0, 10),  10) 	})
	it(`-20 <10 >0`, () => {	assert.equal( clamp(-20, 0, 10),  0) 	})
})



describe("parse geodata", ()=> {
	it(`dec`, () => {	expect( parse_geodata("33; -117") ).toEqual( [33, -117]) 	})
	it(`dec`, () => {	expect( parse_geodata("33, -117") ).toEqual( [33, -117]) 	})

	it(`dec`, () => {	expect( parse_geodata("-33, -117") ).toEqual( [-33, -117]) 	})

	it(`dec`, () => {	expect( parse_geodata("33.81;-117.92") ).toEqual( [33.81, -117.92]) 	})
	it(`dec`, () => {	expect( parse_geodata("33.81,-117.92") ).toEqual( [33.81, -117.92]) 	})
	it(`dec`, () => {	expect( parse_geodata("33.81 -117.92") ).toEqual( [33.81, -117.92]) 	})
	
	it(`dec`, () => {	expect( parse_geodata("  33.81     -117.92  ") ).toEqual( [33.81, -117.92]) 	})
	it(`dec`, () => {	expect( parse_geodata("  33.81  ;  -117.92  ") ).toEqual( [33.81, -117.92]) 	})


	it(`dec`, () => {	expect( parse_geodata("33° -117°") ).toEqual( [33, -117]) 	})
	it(`dec`, () => {	expect( parse_geodata("33.81° ,-117.92°") ).toEqual( [33.81, -117.92]) 	})
	

	it(`dsn`, () => {	expect( parse_geodata("33° N 117° W") ).toEqual( [33, -117]) 	})

	it(`dsn`, () => {	expect( parse_geodata("33° 48′ 36″ N 117° 55′ 12″ W") ).toEqual( [33.81, -117.92]) 	})
	it(`dsn`, () => {	expect( parse_geodata("33° 48′ 36″ N, 117° 55′ 12″ W") ).toEqual( [33.81, -117.92]) 	})
	it(`dsn`, () => {	expect( parse_geodata("33° 48′ 36″ N; 117° 55′ 12″ W") ).toEqual( [33.81, -117.92]) 	})

	it(`dsn`, () => {	expect( parse_geodata("33°48′36″N,117°55′12″W") ).toEqual( [33.81, -117.92]) 	})
	it(`dsn`, () => {	expect( parse_geodata("33°48′36″N;117°55′12″W") ).toEqual( [33.81, -117.92]) 	})
	
	it(`dsn`, () => {	expect( parse_geodata("33° 48′ 36″ N , 117° 55′ 12″ W") ).toEqual( [33.81, -117.92]) 	})
	it(`dsn`, () => {	expect( parse_geodata("33° 48′ 36″ N ; 117° 55′ 12″ W") ).toEqual( [33.81, -117.92]) 	})

	it(`dsn`, () => {	expect( parse_geodata("33° 48′ 36″ S 117° 55′ 12″ E") ).toEqual( [-33.81, +117.92]) 	})

	it(`dsn (diff characters)`, () => {	expect( parse_geodata("33° 48' 36” N 117° 55' 12” W") ).toEqual( [33.81, -117.92]) 	})
	
	it(`broken`, () => {	expect( parse_geodata("asdfasdf") ).toEqual( [undefined, undefined]) 	})

})


describe("clone", ()=> {
	const a = {b: 78, c: {d: [1,2,"a"]}};
	it(`same content`, () => {	expect( clone(a) ).toEqual(a) 	})
	it(`is not the same`, () => {	expect( clone(a) ).not.toBe(a) 	})
})


describe("update", ()=> {
	const a = {
		b: {
			c: 4, 
			cc: 444, 
			d: {
				k: [1,2,3],
			},
		}, 
		c: 44,
		e: 4,
	};
	const b = {
		b: {
			c: 5, 
			cccc: 555, 
			d : {
				o: 5, 
				k: [5, 5],
			},
		}, 
		d: 55,
		e: 5,
	}
	const ba = {
		b: {
			c: 4, 
			cc: 444,
			cccc: 555, 
			d : {
				o: 5, 
				k: [1, 2, 3],
			},
		}, 
		c: 44,
		d: 55,
		e: 4,
	}
	const ab = {
		b: {
			c: 5, 
			cc: 444,
			cccc: 555, 
			d : {
				o: 5, 
				k: [5, 5, 3],
			},
		}, 
		c: 44,
		d: 55,
		e: 5,
	}

	it(`ab`, () => {	expect( update(a, b) ).toEqual(ab) 	})
	it(`ba`, () => {	expect( update(b, a) ).toEqual(ba) 	})
})









/*
MARK 33.81;-117.92
MARK 33.81; -117.92
MARK 33.81, -117.92
MARK 33.81 -117.92
MARK 33° 48′ 36″; N 117° 55′ 12″ W
MARK 33° 48′ 36″ N 117° 55′ 12″ W
MARK 33° 48' 36” N 117° 55' 12” W
MARK 33° 48′ 36″, N 117° 55′ 12″ W
MARK 33°48′36″N117°55′12″W
MARK 33° N 117° W
MARK 33.81°N 117.92°W

*/

/*
describe("plural", ()=> {
	it(`kiss `, () => {	assert.equal( plural(2, "kiss"),  "kisses") 	})
	it(`kiss `, () => {	assert.equal( plural(2, "kiss"),  "kisses") 	})
})
*/

/*
describe("dh cap", ()=> {

  it("llrem => lrem", () => {
    assert.equal( cap("lorem"),  "lorem")
  })

	
})
*/


/*

Na W40K jsem přišel na anglických forech o deskovkách a naučil se lore z memů


	it(`dsn`, () => {	assert.equal( parse_geodata(""),  [33.81, -117.92]) 	})
	it(`dsn`, () => {	assert.equal( parse_geodata(""),  [33.81, -117.92]) 	})
	it(`dsn`, () => {	assert.equal( parse_geodata(""),  [33.81, -117.92]) 	})
	it(`dsn`, () => {	assert.equal( parse_geodata(""),  [33.81, -117.92]) 	})
	it(`dsn`, () => {	assert.equal( parse_geodata(""),  [33.81, -117.92]) 	})
*/