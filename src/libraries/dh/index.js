//TODO - add the rest of the library, lol




//NUMBERS

/**
 * change input into min or max value if its out of the range
 * @param {number} input - input number
 * @returns {number}  min - minimal value
 * @returns {number}  max - maximal value
 */
export const clamp = function(input, min, max) {
	if( input === undefined || isNaN(input) || min === undefined || isNaN(min) || max === undefined || isNaN(max) ) return undefined;
	if(min > max) [min, max] = [max, min];
	return input < min ? min : input > max ? max : input;
}





//TODO - allow numbers? 
/**
 * is vowel (aeiou)
 * @param {string} input 
 * @returns {boolean} 
 */
export const vowel = function(input){
	if(!input || typeof input !== "string") return false;
	return !!input.match(/[aeiouAEIOU]/);
}

/**
 * capitalize the first letter (or return empty string)
 * @param {string|number} string - input string
 * @returns {string} 
 */
export const cap = function(string) {
	if(!string) return "";
	if(typeof string === "number") return string.toString();
	if(typeof string !== "string") return "";
	return string.charAt(0).toUpperCase() + string.slice(1);
}



export const var_to_string = function(string) {
	return string.replace(/_|-/g," ").split(" ").map( a => cap(a) ).join(" ");
}





//TODO - THIS SHIT IS TOO COMPICATED 
/**
 * turns into plural if value not one
 * @param {number} value
 * @param {string} singular
 * @param {string} [plural] - optional, otherwise created automatically 
 * @returns {string} 
 */
export const plural = function(value, singular, plural){
	if(value === undefined || value === 1) return singular;
	if(plural !== undefined) return plural;
	
	
	const last = singular[singular.length - 1];
	const second_last = singular[singular.length - 2];
	
	if(last === "s" || last === "z") return singular + "es";
	if(last === "y" && !vowel(second_last) ) return singular.slice(0, -1) + "ies";
	if(
			(last + second_last) === "sh"
		||	(last + second_last) === "ch"
	) return singular + "es";
		
	//https://en.wikipedia.org/wiki/English_plurals#Regular_plurals
	//todo ies
	//f ves
	//guy	
	return singular + "s";
}


//TODO
const fce_plural = plural; //TODO
export const number_plural = function(value, singular, plural){
	return `${number(value)} ${fce_plural(value, singular, plural)}`;
}


/**
 * turns into possesive noun
 * @param {string} input
 * @return {string} 
 */
/*
export const posess = function(input){
	if(string.endsWith("s") || string.endsWith("z")) return `${string}'`;
	return `${string}'s`;
	return singular + "s";
}
*/



/**
 * clones object via JSON
 * @param {Object} input
 * @return {Object} 
 */
export const clone = function(input){
	return JSON.parse(JSON.stringify(input));
}


//TODO - this is tempfix 
export const update = function(original = {}, altered = {}){
	const chimera = clone(original);

	Object.keys(altered).forEach( key => {
		if(typeof altered[key] === "object" && !Array.isArray(altered[key]) ){
			chimera[key] = update(chimera[key], altered[key]);
		}else{
			chimera[key] = altered[key]
		}
	})
	
	return chimera;
}


//TODO TODO TODO
/*
export const retrieve = function(original, key){
	let current; 
	try {
		current = update(  original, JSON.parse( localStorage.getItem(key) )  );
	} catch (error) {
		current = clone(original);
	}

	try {
		localStorage.setItem(key, JSON.stringify(current));
		//current = update(  original, JSON.parse( localStorage.getItem(key) )  );
	} catch (error) {
	}

console.error(current);
	return current;
}
*/









/**
 * turns string with coordinates and returns [latitude, longitude] or [undefined, undefined]
 * @param {string} input
 * @returns {Array} coordinates
 * @returns {number} coordinates[0] - latitude (x)
 * @returns {number} coordinates[1] - longitude (y)
 */
export const parse_geodata = function(input){

	if( input.match("N") || input.match("S") ){ 

		const coordinates = input.trim().replace(/,| |;/g, ""); //doesnt care if divided by "," or " " or ";"
		/*
			/
				(\d+\.?\d*)°
				(\d+)?[′|']?
				(\d+\.?\d*)?[″|”]?
				(\w)
				
				\s*[,;]*\s*

				(\d+\.?\d*)°
				(\d+)?[′|']?
				(\d+\.?\d*)?[″|”]?
				(\w) 
			/
		*/
		const code = /(\d+\.?\d*)°(\d+)?[′|']?(\d+\.?\d*)?[″|”]?(\w)\s*[,;]*\s*(\d+\.?\d*)°(\d+)?[′|']?(\d+\.?\d*)?[″|”]?(\w)/.exec(coordinates); 

		if(code === null) return [undefined, undefined]; //broken

		const [uselesss, y_hour, y_minute, y_second, N, x_hour, x_minute, x_second, E] = code.map(a => {
			if(!a) return 0; //undefined
			if( isNaN(a) ) return a; //"N" or "S" or "E" or "W"
			return +a; //number in string format
		}); 
		
		let yyy = y_hour + (y_minute * 1/60) + (y_second * 1/3600);
		let xxx = x_hour + (x_minute * 1/60) + (x_second * 1/3600);
		
		if(N !== "N") yyy *= -1; //southern hemisphere
		if(E !== "E") xxx *= -1; //western hemisphere
		
		yyy = Math.round( yyy * 10000 ) / 10000; //rounding to 0.0001 gives accuracy ~10 m
		xxx = Math.round( xxx * 10000 ) / 10000;
		

		return [yyy, xxx];

	}else{

		/*
			(-?\d+\.?\d*)\s*°?
				\s*[,;]*\s*
			(-?\d+\.?\d*)\s*°?
		*/
	

		const code = /(-?\d+\.?\d*)\s*°?\s*[,;]*\s*(-?\d+\.?\d*)\s*°?/.exec(input); 
		if(code === null) return [undefined, undefined]; //broken
		const latitude = +code[1];
		const longitude = +code[2];

		return [
			isNaN(latitude) ? undefined : latitude,
			isNaN(longitude) ? undefined : longitude,
		];
	}
	
}
	
	




//TODO!!!!
export const and = function(...items) {
	if(items.length === 1) return item[1]; 
	const last = items.pop();
	return `${items.join(", ")} and ${last}`; 
}


//TODO
export const ordinal = function(number) {
	return number;
}

export const number = function(number) {
	return number;
}

/*
l


console.error( update(a, b) );


console.error( update(b, a) );

console.error( update({}, a) );
console.error( update(b, {}) );

console.log(a)
console.log(b)

*/