export function stringify_number(number, options){
	try { 
		return (number).toLocaleString(options.language) 
	} catch (err) {
		return (number).toLocaleString() 
	}
}



export function round(input, places = 0) {
   	input = input * Math.pow(10, places);
	input = Math.round(input); //math round is iffy; TO DO
	input = input / Math.pow(10, places);
	return input;
}