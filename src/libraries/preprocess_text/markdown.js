export const markdown = function(data, options){
	
	const escape = ""; //TODO
	
	if(options.block){

	//HEADLINES H1 - H6
		function id(text){ //TODO - TEMPFIX
			text = text.toLowerCase();
			text = text.trim().replaceAll(" ", "-");
			return text;
		}

		for(let i = 6; i > 0; i--){
			function exec(match, text){
				return `<h${i} id = "${id(text)}" >${text}</h${i}>`;
			}
			const rex = new RegExp(`^#{${i}}(.*)`, `gm`)
			data = data.replace(rex, exec); 
		}


	//PARAGRAPHS & LISTS 	//TODO - lists only one level deep!!!!
		let active_ul = false;
		let active_ul_2 = false;

		data = data.split("\n").map( line => {
			//if( line.startsWith("#") || line.startsWith("/") ) return line;

			//list 2
			if( active_ul && (line.startsWith("\t- ") || line.startsWith("\t* ")) ){
				if(active_ul_2){
					return `<li>${line.slice(1)}`;
				}else{
					active_ul_2 = true;
					return `<ul><li>${line.slice(1)}`;
				}
			}
		
			//list 1
			if( line.startsWith("- ") || line.startsWith("* ") ){
				
				if(active_ul_2){
					active_ul_2 = false;
					return `</ul><li>${line.slice(1)}`;
				}else if(active_ul){
					return `<li>${line.slice(1)}`;
				}else{
					active_ul = true;
					return `<ul><li>${line.slice(1)}`;
				}
			}

			if( line === "\r" ){
				let out = ``;
				//list 2
				if(active_ul_2){
					out += `</ul>`;
					active_ul_2 = false;
				}
				//list 
				if(active_ul){
					out += `</ul>`;
					active_ul = false;
				}
				//paragrap
				out +=  `</p> <p>`;
				return out; 
			}
			return line;
		}).join("\n");
		
		data = `</p>${data}<p>`;
	}




	//LINKS 
	data = data.replace( /\[(.*?)\]\((.*?)\)/g, (match, text, url)=> {
		const target = (url.startsWith(`http`) || url.startsWith(`www`)) ? `target="_blank"` : ``;
		return `<a href="${url}" ${target}>${text}</a>`;
	}); 



	function between(fce, start, end = start){
		function exec(match, p1){
			//if( match.startsWith(escape) ) return p1; //TODO - REXEX?
			return fce(p1);
		}
		
		const rex = new RegExp(`${escape}${start}(.*?)${end}`, `gm`)
		data = data.replace(rex, exec); 
	}
	
	function behind(fce, start){
		function exec(match, p1){
			//if( match.startsWith(escape) ) return p1; //TODO - REXEX?
			return fce(p1);
		}
		
		// anything following closed in "()"
		const rex_long = new RegExp(`${escape}${start}\\((.*?)\\)`, `gm`)
		data = data.replace(rex_long, exec); 
		
		// only following single word (or number)
		const rex_short = new RegExp(`${escape}${start}(\\w*)`, `gm`)
		data = data.replace(rex_short, exec); 
	}


/*
	MARKDOWN-LITE
	`**something**` => `<b>something</b>`
	`*something*` => `<i>something</i>`
	`~something~` => `<strike>something</strike>`
	`{something}` => `<small>something</small>`
	``something`` => `<code>something</code>`

	`^something else` => `<sup>something</sup> else` (^ - caret - alt+94)
	`^(something else)` => `<sup>something else</sup>`
	`∧something else` => `<sup>something</sup> else` (∧ wedge)
	`∧(something else)` => `<sup>something else</sup>`
	
*/

	// \ in strings are disappearing so they have to be doubled

	between(a => `<strong>${a}</strong>`, `\\*{2}`); // **something** to <strong>something</strong>
	between(a => `<em>${a}</em>`, `\\*{1}`); //  *something* to <em>something</em>

	between(a => `<strike>(${a})</strike>`, `~`); //  ~something~ to <strike>something</strike>
	between(a => `<small>(${a})</small>`, `{`, `}`); // {something} to <small>(something)</small>
	between(a => `<code>${a}</code>`, "`"); //  `something` to <code>something</code>

	//wwbb
	between(a => `<small>(${a})</small>`, `†`); //  †something† to <small>(something)</small>
	data = data.replaceAll("†",""); //TODO TEMPFIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//data = data.replace(/(†)\s*$/gm, ``); // removes tag † at the end of the line notifying events in 
	
	
	behind(a => `<sup>${a}</sup>`, `[\\^\\∧]`);
	behind(a => `<sup>${a}</sup>`, `˄`); //TODO
	behind(a => `<sub>${a}</sub>`, `˅`);





	//INDEX
	if(options.index){
		let active_3 = false;
		let cix = `<p><ul>`;

		function exec(match, i, id, text){
			if(i == 1){
				cix = `${match} ${cix}`;
				return "";
			}else if(i == 2){
				if(active_3){
					active_3 = false;
					cix += `</ul>`;
				}
				cix += `<li><a href="#${id}">${text}</a>`;
			}else if(i == 3){
				if(!active_3){
					active_3 = true;
					cix += `<ul>`;
				}
				cix += `<li><a href="#${id}">${text}</a>`;
			}
			return match;
		}
		const rex = new RegExp(`<h(.).*"(.*)".*>(.*)<.*>`, `gm`)
		data = data.replace(rex, exec); 
		
		if(active_3) cix += `</ul>`;
		cix += `</ul></p>`;

		data = `${cix}${data}`;
	}


	return data;
}




