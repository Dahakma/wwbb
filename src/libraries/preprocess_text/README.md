# Preprocess text 

## Language 

```javascript
options.language = navigator.language;
```

- can override browsers default language 

```javascript
options.english = en-US" ? "us" : "en";
```

- prefer American or British English 


## Fix  

```javascript
options.fix = true;
options.fix = {
	typos: true, 
	numbers: false, 
}
```

```javascript
options.fix.typos === true;
```

- remove typos often caused by procedural generation of text and combining various strings (like multiple spaces or dots etc.)
- `abc,,   qwe;;;` => `abc, qwe;` - replace multipled `,`, `;` and ` `
- `abc , ;` => `abc,;` - removes ` ` before `,` or `;`
- `abc,qwe;yxc` => `abc, qwe; yxc` - forces ` ` behind `,` and `;`; 		
	ignores `.`
	ignores when digits on both sides (`1,000` => `1,000)
- `  abc  ` => `abc` - outer trim

```javascript
options.fix.numbers === true;
```

- `1_000_000` => `1,000,000`
- `1000000` => `1,000,000`
- format based on explicit `options.language` or default detected language 



## Markdown 

```javascript
options.markdown = true;
options.markdown = {
	block: true, 
	index: false, 
}
```

- transforms markdown-like syntax into html tags
- `**something**` => `<b>something</b>`
- `*something*` => `<i>something</i>`
- `~something~` => `<strike>something</strike>`
- `{something}` => `<small>something</small>`
- `^something else` => `<sup>something</sup> else` (^ - caret - alt+94)
- `^(something else)` => `<sup>something else</sup>`
- `∧something else` => `<sup>something</sup> else` (∧ wedge)
- `∧(something else)` => `<sup>something else</sup>`
- `\`something\`` => `<code>something</code>`

```javascript
options.markdown.block === true
```

- `# something` => `<h1>something</h1> -  # at the start of the line, whole line, ## => h2, etc., ###### => h6
- `- something` => `<ul><li>something</ul>`
- added `<p>`s to paragraphs separated by empty lines 




## Units 

```javascript
options.units = false, 
options.units = {
	symbol,
	system, 
	full,
}
```

- recounts units; "1@mi" => "0.621 kilometres"
- `symbol` - used symbol detecting recountable units (usually used `@`) 
- `system` - `si` or `us` (US customary)
- `full` - `m` or `metre`



## Replace

```javascript
options.replace = undefined; 
options.replace{
	pattern,
	fce,
}
```

- uses the `fce` to replace substring matching `pattern`

```javascript
const text = "@name has a cat. ";

function replace(key){
	if(key === "name") return "John";
}

preprocess_text(text, {
	replace: {
		pattern: "@(\\w+)",
		fce: replace,
	},
}); //"John has a cat. "
```



## Distort 

```javascript
options.distort = false, 
options.distort = {
	shuffle,
	tldr, 
	capitals,
	roman,
}
```

- turns text into mess
- `shuffle` - shuffles letters 
	- `0` - no
	- `1` - two letters
	- `2` - all
- `tldr` - shortens text, replaces with - `blah...`
- `capitals` - randomly capitalise letters 
	- `0` - no
	- `1` - few
	- `10` - max
- `roman` - replaces numbers with Roman numbers 














