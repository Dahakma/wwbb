/**
	seed-based random number 
	see: http://indiegamr.com/generate-repeatable-random-numbers-in-js/ 
*/


export const rg = {
	number: 0,
	i(...arg){
		//TODO - overcomplicated
		arg = arg.map( a => {
			if(a === undefined){
				 return new Date().getTime();
			}else if(typeof a === "string"){
				return +[...a].map( c => c.charCodeAt() ).join('');
			}else{
				return a;
			}
		});
		if(!arg.length) arg.push( new Date().getTime() );
		this.number = arg.reduce( (a,c) => a + c );
		return this.g;
	},
	
	get g(){
		this.number = (this.number * 9301 + 49297) % 233280;
		return this.number / 233280;
	},
	
	get b(){
		return this.g < 0.5;
	},
	
	get percent(){
		return this.integer(0, 100);
	},
	
	
	integer(min = 1, max){
		if(max === undefined){
			max = min;
			min = 0;
		}
		return Math.round( min + this.g * (max - min) );
	},
	
	
	
	
	range(min = 1, max){
		if(max === undefined){
			max = min;
			min = 0;
		}
		return  min + this.g * (max - min);
	},
	
	array(array){
		if(!Array.isArray(array)){ 
			console.error(`Error - "rg.array(array)" - invalid input; array expected; "${array}" inputted`);
			return undefined;
		}
		return  array[ Math.floor( array.length * this.g ) ];
	},
	
	shuffle(array){
		//if(array === undefined) return; //TODO ???
		if(!Array.isArray(array)){ 
			console.error(`Error - "rg.shuffle(array)" - invalid input; array expected; "${array}" inputted`);
			return undefined;
		}
		//Using Durstenfeld shuffle algorithm (Fisher-Yates) TODO
		for (let i = array.length - 1; i > 0; i--) {
			const j = Math.floor(this.g * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}
		return array;
	},
	
	weighted(array){
		if(!Array.isArray(array)){ 
			console.error(`Error - "rg.weighted(array)" - invalid input; array expected; "${array}" inputted`);
			return undefined;
		}
		
		//clean input
		const input = array.map( a => {
			if(a === null) return 0;
			if( isNaN(a) ) return 0;
			if( a < 0 ) return 0;
			return a;
		});
	
		if( !input.some(a => a > 0) ) return undefined; //no value valid (number above zero)
		
		let range = input.reduce( (a,b) => a + b );
		const result = this.g * range;

		range = 0;
		for (let i = 0; i < input.length; i++){
			range += input[i];
			if(result <= range) return i;
		}
	},
	
	tweak(input, adjust){
		if( !(input * adjust) || isNaN(input * adjust) ) return input; //TODO
		return (input - adjust) + (2 * adjust * this.g);
	},
	
	
/////////////////////////////////	
	
	
	
	get date(){ //TODO - is eve used? 
		return  new Date().getTime();
		//return a;
	},
	
	get seed(){ //TODO - is eve used? 
		return Math.floor(this.g * 1000000); //TODO!!!!
	},
	
 
	between(min, max){ //TODO deprecated  use range
		return  min + this.g * (max - min);
	},
	
	one(input){
		if(!Array.isArray(input)){ 
			if(input === Object(input)){
				console.error(`Error - "rg.one(input)" - invalid input; array or primitive expected; "${input}" inputted`);
				return undefined;
			}else{
				return input;
			}
		}
		return  this.array(input);
	},
	
	
	
	

};



export const ra = {
	
	get g(){
		return Math.random();
	},
	
	get b(){
		return this.g < 0.5;
	},
	
	get percent(){
		return this.integer(0, 100);
	},
	
	integer(min = 1, max, random){
		if(random === undefined) random = this.g;
		if(max === undefined){
			max = min;
			min = 0;
		}
		return Math.round(  min + ( random * (max - min) )  );
	},
	
	range(min, max, random){
		if(random === undefined) random = this.g;
		if(max === undefined){
			max = min;
			min = 0;
		}
		return  min + (  random * (max - min)  );
	},
	
	array(array){
		if(!Array.isArray(array)){ 
			console.error(`Error - "ra.array(array)" - invalid input; array expected; "${array}" inputted`);
			return undefined;
		}
		return  array[ Math.floor( array.length * this.g ) ];
	},
	
	
	
	tweak(input, adjust){
		if( !(input * adjust) || isNaN(input * adjust) ) return input; //TODO
		return input - adjust + 2 * adjust * this.g;
	},
	
	weighted(array, random){
		if(!Array.isArray(array)){ 
			console.error(`Error - "ra.weighted(array, random)" - invalid input; array expected; "${array}" inputted`);
			return undefined;
		}
		if(random === undefined) random = this.g;

		//clean input
		const input = array.map( a => {
			if(a === null) return 0;
			a = +a;
			if( isNaN(a) ) return 0;
			if( a < 0 ) return 0;
			return a;
		})

		if( !input.some(a => a > 0) ) return undefined; //no value valid (number above zero)
		
		let range = input.reduce( (a,b) => a + b );
		const result = random * range;
		
		range = 0;
		for (let i = 0; i < input.length; i++){
			range += input[i];
			if(result <= range) return i;
		};
	},
	
	shuffle(array){
		if(!Array.isArray(array)){ 
			console.error(`Error - "ra.shuffle(array)" - invalid input; array expected; "${array}" inputted`);
			return undefined;
		}
		
		//Using Durstenfeld shuffle algorithm (Fisher-Yates) TODO
		for (let i = array.length - 1; i > 0; i--) {
			const j = Math.floor(this.g * (i + 1));
			[array[i], array[j]] = [array[j], array[i]];
		}
		return array;
	},
	
	tweak(input, adjust, random){
		if(random === undefined) this.g;
		if( !(input * adjust) || isNaN(input * adjust) ) return input; //TODO
		return (input - adjust) + (2 * adjust * random);
	},
	
	
	////////////////
	one(input){
		if(input === Object(input)){
			if(val === Object(val)){
				console.error(`Error - "rg.one(input)" - invalid input; array or primitive expected; "${input}" inputted`);
				return undefined;
			}else{
				return input;
			}
		}
		return  this.array(input);
	},
	
	
	between(min, max, seed){
		random ??= this.g;
		return  min + random * (max - min);
	},
	
	get seed(){
		return Math.floor(this.g * 1000000); //TODO!!!!
	},
	
	get date(){
		return  new Date().getTime();
		//return a;
	},
	
};
