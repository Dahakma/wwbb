import {rg} from "./util";

//TODO!!!!
function outputKeys(output, input){
	Object.keys(output).forEach(  a => output[a] = Math.round(output[a])  );
	
	if(input.s !== undefined){
		return output;
	}else if(input.satur !== undefined){
		return {
			hue: output.h,
			satur: output.s,
			light: output.l,
			alpha: output.a,
		}
	}else if(input.saturation !== undefined){
		return {
			hue: output.h,
			saturation: output.s,
			lightness: output.l,
			alpha: output.a,
		}
	}
}

function inputKeys(input){
	return {
		h: input.h ?? input.hue,
		s: input.s ?? input.satur ?? input.saturation,
		l: input.l ?? input.light ?? input.lightness,
		a: input.a ?? input.alpha,
	}
}

function tweak(input, adjust, seed){
	rg.i(seed);
	return {
		h: rg.tweak(input.h, adjust[0]),
		s: rg.tweak(input.s, adjust[1]),
		l: rg.tweak(input.l, adjust[2]),
		a: rg.tweak(input.a, adjust[3]),
	}
}

function boost(output, input, adjust){
	output.h += input.h < output.h ? adjust[0] : -adjust[0];
	output.s += input.s < output.s ? adjust[1] : -adjust[1];
	output.l += input.l < output.l ? adjust[2] : -adjust[2];
}

//COLOR.complimentary( COLOR.randomColor() );
					
export function complimentary(input, adjust = [20, 20, 20, 0], seed){
	let output = inputKeys(input);
	
	output.h -= 180;
	output.h = output.h < 0 ? output.h + 360 : output.h;
		
	output = tweak(output, adjust, seed);
	
	return outputKeys(output, input);
}
			
export function analogue(input, adjust = [30, 30, 10, 0], seed){
	const infix = inputKeys(input);
	const output = tweak(infix, adjust, seed);
		
	if(output.l < 40){
		output.light += rg.g * 30;
		boost(output, infix, [40, 2, 0, 0]);
	}else{
		output.l -= rg.g * 15;
		boost(output, infix, [40, 2, 10, 0]);
	}
				
	return outputKeys(output, input);
}

			
export function monochromatic(input, adjust = [20, 30, 0, 0], seed){
	const infix = inputKeys(input);
	const output = tweak(infix, adjust);
	
	if(output.l < 40){
		output.l += rg.g * 15;
		boost(output, infix, [7, 6, 15, 0]);
		
	}else{
		output.l -= rg.g * 10;
		boost(output, infix, [7, 6, 10, 0]);
	}
	
	return outputKeys(output, input);
}

