import {parseHSLA, parseRGB, stringifyHSLA} from "./parse";

/**
	RGBtoHSLA
	input - object {r: red, g: green, b: blue} or {r: red, g: green, b: blue, a: alpha}
		  - or string with RGB or RGBA color 
	returns {h: hue, s: saturation, l: lightness, a: alpha}
*/
//based on https://gist.github.com/mjackson/5311256
export function RGBtoHSLA(input){
	if(typeof input === "string"){
		input = parseRGB(input);
	}
	
	//TODO checking invalid input 
	
	let [r, g, b, a] = [input.r, input.g, input.b, input.a];
	
	r /= 255;
	g /= 255;
	b /= 255;
	
	const max = Math.max(r, g, b);
	const min = Math.min(r, g, b);
	let h, s, l = (max + min) / 2;
	
	if (max == min) {
		h = s = 0; // achromatic
	} else {
		const d = max - min;
		 s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

		switch (max) {
			case r: h = (g - b) / d + (g < b ? 6 : 0); break;
			case g: h = (b - r) / d + 2; break;
			case b: h = (r - g) / d + 4; break;
		}

		h /= 6;
	}
	
	h *= 360;
	s *= 100;
	l *= 100;
	a ??= 1;
	
	return {
		h,
		s,
		l,
		a,		
	};
}
	
	
/**
	HEXtoRGB(input)
	input string with hexadecimal color
	returns object {r: red, g: green, b: blue}
*/	
export function HEXtoRGB(input){
	if(typeof input !== "string") return undefined;
	
	const result = (()=>{
		if(input.length <= 4){ //because of short "#000" etc. ; TODO - could be solved in more elegant way
			const result = /^#?([a-f\d]{1})([a-f\d]{1})([a-f\d]{1})$/i.exec(input);
			result[1] += result[1];
			result[2] += result[2];
			result[3] += result[3];
			return result;
		}else{
			return /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(input);
		}
	})();
	return {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	};
}


/**
	HEXtoHSLA(input)
	input string with hexadecimal color
	returns object {h: hue, s: saturation, l: lightness, a: alpha}
*/
export function HEXtoHSLA(input){
	return RGBtoHSLA( HEXtoRGB(input) );
}


/**
	HSLAtoRGB
	input object {h: hue, s: saturation, l: lightness, a: alpha} or string with HSLA color
	returns `{r: red, g: green, b: blue, a: alpha}`
*/
//based on https://gist.github.com/mjackson/5311256
export function HSLAtoRGB(input){
	if(typeof input === "string"){
		input = parseHSLA(input);
	}
	
	//TODO - checking invalid input 
	let [h, s, l, a] = [input.h, input.s, input.l, input.a];
	let r, g, b;
	
	h = h/360; 
	s = s/100;
	l = l/100;
	
	if(s == 0){
		r = g = b = l; // achromatic
	}else{
		const hue2rgb = function(p, q, t){
			if(t < 0) t += 1;
			if(t > 1) t -= 1;
			if(t < 1/6) return p + (q - p) * 6 * t;
			if(t < 1/2) return q;
			if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
			return p;
		};

		const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		const p = 2 * l - q;
		
		r = hue2rgb(p, q, h + 1/3);
		g = hue2rgb(p, q, h);
		b = hue2rgb(p, q, h - 1/3);
	}

	return {
		r: Math.round(r * 255), 
		g: Math.round(g * 255), 
		b: Math.round(b * 255),
		a: a === undefined ? 1 : Math.round(input.a * 1000) / 1000,
	};	
}

export const HSLtoRGB = HSLAtoRGB;
	
/**
	RGBtoHEX
	input object {r: red, g: green, b: blue, a: alpha} or string with RGB color
	returns string with hexadecimal color
*/
 export function RGBtoHEX(input){
	if(typeof input === "string"){
		input = parseRGB(input);
	}
	
	function stringify(number){
		return number.toString(16).padStart(2, "0");
	}
	
	return `#${stringify(input.r)}${stringify(input.g)}${stringify(input.b)}`; 
}


/**
	HSLAtoHEX
	input object {h: hue, s: saturation, l: lightness, a: alpha} or string with HSLA color
	returns string with hexadecimal color
*/
 export function HSLAtoHEX(input){
	if(typeof input === "string"){
		input = parseHSLA(input);
	}
	return RGBtoHEX( HSLAtoRGB(input) );
}

export const HSLtoHEX = HSLAtoHEX;






/**
	TODO 
*/
export function toHEX(input){
	if(typeof input === "string" && input[0] == "h"){ //TODO????? WHAT THE FUCK?
		input = parseHSLA(input);
		input = HSLtoHEX(input)  
	}else{
		
	}
	return input;
}





export function toStringifiedHSLA(input){
	return stringifyHSLA(toHSLA(input));
}


export function toHSLA(input){
	//object 
	if(typeof input === "object"){
		//already parsed 
		if(input.h && input.l && input.s){ 
			if(input.a){
				return {h, s, l, a};
			}else{
				return {h, s, l, a: 1};
			}
		//parsed rgb
		}else if(input.r && input.g && input.b){
			return RGBtoHSLA(input);
		}
	//string
	}else if(typeof input === "string"){
		//hsla
		if(input.startsWith("hsl")){
			return parseHSLA(input);
		//rgb
		}else if(input.startsWith("rgb")){
			return RGBtoHSLA(parseRGB(input));
		//hex
		}else if(input.startsWith("#")){
			return HEXtoHSLA(input);
		}	
	}else{
		return undefined;
	}
}


