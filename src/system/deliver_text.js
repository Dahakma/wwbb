import {get} from 'svelte/store';
import {clone} from 'Dh';
import {rg, ra} from 'Random';
import {display} from 'System/display';

import {settings as org_settings} from 'System/settings';
import {user} from 'System/user';
import {game as org_game} from 'System/game/game';

import {character_intros} from 'Data/speeches/character_intros/index';
//import {core} from 'Data/speeches/core/index';
import * as game_intros from 'Data/speeches/core/game_intros.js'; 
import * as to_final_zero_points from 'Data/speeches/core/to_final_zero_points.js'; 
import * as to_final from 'Data/speeches/core/to_final.js'; 
import * as endings from 'Data/speeches/core/endings.js'; 



import {att, round, session, settings, mode} from 'System/write';
import {change} from 'System/write';
import {dim, npc, stats} from 'System/write';

console.log(game_intros);


function replies_links(replies, next){
	replies.forEach( reply => {
		if( reply.condit === undefined || reply.condit() ){
			display.add({
				type: "link",
				text: reply.label, 
				exec(){
					if(reply.accept){
						change.att.accept.by(reply.accept);
					}
					if(reply.text){
						if(reply.exec !== undefined) reply.exec();
						display.reset();
						reply.text();
						if(reply.replies) replies_links(replies, next);
					}else{
						if(reply.exec !== undefined) reply.exec();
						org_game.next(next);
					}
				},
			})
		}
	});
}
		
		
	
function execute(list, next){
	
	const array = Object.keys(list);
	
	const weight = array.map( key => {
		const chosen = list[key];
		if(typeof chosen === "function") return 1;
		if(chosen.condit) return chosen.condit();
		return 1;
	});
	
console.log(weight);
	const chosen = list[   array[  rg.weighted(weight) ]   ];
console.log(chosen);
	
	if(typeof chosen === "function"){
		chosen();
	}else{
		chosen.text();
	}
	
	if(chosen.replies) replies_links(chosen.replies, next);
}


function prop_clone(target, source){
	Object.keys( source ).forEach( key => {
		if(typeof source[key] === "object"){
			target[key] = clone( source[key] );
		}else{
			target[key] = source[key];
		}
	});
}

export const deliver_text = function(what, next){

//PREPARE VARIABLES 
	const game = get(org_game);
	prop_clone(mode, game.mode) 
	prop_clone(att, game.player.att) 
	prop_clone(dim, game.player.dim); //TODO WONT WORK
	prop_clone(npc, game.player.npc); 
	
	prop_clone(round, game.round);
	prop_clone(session, game.session);
	
	prop_clone(stats, user.stats);
	prop_clone(settings, org_settings) 
	
//INITIATES SEEDED RANDOM
	//rg.i(game.player.seed.game + game.session.index);
	rg.i( Math.random() * 1000000 );
	 
//DELIVER TEXT 
	//START
	if(what === "game_intro"){
		execute(game_intros, next);
	}else if(what === "character_intro"){
		execute(character_intros, next);
		
	//END
	}else if(what === "to_final_zero_points"){	
		execute(to_final_zero_points, next);
	}else if(what === "to_final"){	
		execute(to_final, next);
	}else if(what === "ending"){	
		execute(endings, next);	
		
	}
	
	
}


//deliver_text("game_introduction");