//import source_negative from '/assets/sounds/negative_beep.wav';
//import source_positive from '/assets/sounds/positive_beep.wav';

function sound(src) {
	this.sound = document.createElement("audio");
	this.sound.src = src;
	this.sound.setAttribute("preload", "auto");
	this.sound.setAttribute("controls", "none");
	this.sound.style.display = "none";
	document.body.appendChild(this.sound);
	this.play = function(){
		this.sound.play();
	}
	this.stop = function(){
		this.sound.pause();
	}
}

//export const negative = new sound(source_negative);
//export const positive = new sound(source_positive);

/*

negative https://freesound.org/people/themusicalnomad/sounds/253886/
*/