import {rg} from 'Random';
import {deliver_names} from 'System/deliver_data'

export const random = {
	get male(){
		return rg.array( deliver_names("male") );
	},
	get male_all(){
		return rg.array( deliver_names("male_all") );
	},
	get female(){
		return rg.array( deliver_names("female") );
	},
	get female_all(){
		return rg.array( deliver_names("female_all") );
	},
	get goth(){
		return rg.array( deliver_names("goth") );
	},
	get bimbo(){
		return rg.array( deliver_names("bimbo") );
	},
	get unisex(){
		return rg.array( deliver_names("unisex") );
	},
	get surname(){
		return rg.array( deliver_names("surname") );
	},
};

/*function random_name(input, random = Math.random() ){
	//if(typeof random === "undefined")random = Math.random();

	if(input=="sex"){
		if(sex==0){input="male"}else{input="female"}
	}; //NOT USED!!
	
	let temp;
	 

	switch (input){
		//basic names
		case "unisex": 		return dh.pick_one(unisexNames, random);
		case "female":
		case "femaleShort": 
				return dh.pick_one( 
					femaleNamesList[ nameListKeys[mode.alt_names] ],
					random
				);
		case "femaleLong":  return dh.pick_one(femaleNames, random);
		case "male":		
		case "maleShort":
				return dh.pick_one( 
					maleNamesList[ nameListKeys[mode.alt_names] ],
					random
				);
		case "maleLong":	return dh.pick_one(maleNames, random); 
		
		//names used for transformations
		case "goth":		return dh.pick_one(gothNames, random); 
		case "slut":		return dh.pick_one(slutyNames, random); 

		//horse names used in ponyfication-ralated epilogues
		case "pony":
		case "horse": 		return pick_one("Horsey McHorseface","Sugar","Clover","Cupcake","Daisy","Lucky","Buttercup","Jewel") );
			
		//libertarian country		
		case "lib_country": return  pick_one("Republic of","New","Principality of")+" "+pick_one("Minerva","Freedonia","Libertania","Liberland")
		
		//university
		case "major":	return pick_one("engineering","computer science") );

					
		//creepy country
		case "creepy_country": return pick_one("Japan","Russia","China","China","Saudi Arabia","Qatar","Brazil") );
		//unstable geographical area
		case "shithole": return pick_one("South American","South American","African","African","sub-Saharan","North American","European","East European","Balkan","Middle-Eastern","Middle-Eastern","Central Asian","Caribbean","Asian","East Asian") );
		
		//band name
		case "genre":	return pick_one("pop", "rock", "metal", "punk", "pop") );
		case "band":	return random_name( pick_one("pop", "rock", "metal", "punk", "pop") );
		case "pop": 	return pick_one("Beneath Future","Death Trap","The Excessive Machine","Public Warnings","One Girl, Four Ants","Vampire Vixens","Barbie Gonne Wild","Android of Beauty","Failure to Deliver") );
		case "rock": 	return pick_one("Beneath Future","Death Trap","Pitchfork","Bunk Deep","The Excessive Machine","Damp Jeannine","Pounding Cherry","Sexy Scissors","Public Warnings","One Girl, Four Ants","Vampire Vixens","Barbie Gonne Wild","Android of Beauty","Failure to Deliver","Carousing Countess","The Chastity Belt","Galerina Marginata","Bride of Satan") );
		case "metal": 	return pick_one("Pitchfork","Necronomicon","Vampire Vixens","Virgin Blood","Amatoxin","Galerina Marginata","Bride of Satan","Ëitël Hëxë") );
		case "punk": 	return pick_one("Beneath Future","Death Trap","Bunk Deep","The Excessive Machine","Damp Jeannine","Pounding Cherry","Sexy Scissors","Public Warnings","One Girl, Four Ants","Vampire Vixens","Barbie Gonne Wild","Android of Beauty","Failure to Deliver","Carousing Countess","The Chastity Belt") );
		
		case "mafia": 	 return pick_one("the Mafia","the Yakuza","Cartels","Triads","loan sharks") );
		case "crime":	return pick_one("shoplifting","shoplifting","public indecency","public indecency","public intoxication","assault","vandalism","reckless driving","sexual harassment","driving under the influence","indecent exposure") );	
		case "rich_guy": return pick_one("tech mogul","real estate magnate","TV star","banker and investor","football player") );
		
		case "strip_club": return `<i> ${pick_one("Bare Assets","Booby Trap","Pole Position","Bottoms Up","The Naughty Harlets","Bunny Pen","Teasers")} </i>`;
		case "surname": return pick_one("Smith","Jones","Taylor","Brown","Williams","Wilson","Johnson","Davies","Robinson","Wright","Thompson","Evans","Walker","White","Roberts","Green","Hall","Wood","Jackson","Clarke","Black") );
		case "arc_f": 	temp = random_name("female") );
						return pick_one("Isis, the Princess of Nile","High Queen Galadriel","Empress "+temp,"Imperatrix "+temp,"Princess "+temp,"Supreme mistress "+temp,"Emira "+temp);
		case "arc_m":	temp = random_name("male") ); sex = 0;
						return pick_one("Jarl Bjorn","Jarl Ragnar","Shahanshah Asad","Padishah "+temp,"Tsar "+temp,"Sultan "+temp,"Imperator "+temp,"Warlord "+temp);			

		
			
	
		
		case "bad_job":  temp = pick_one("a shop assistant","a waitress","a cleaner","a cashier","a hostess","a clerk","a maid") );
				STAT.final_fate=temp;
					break;
					
		case "sex_job":  temp = pick_one("a call girl","a topless waitress","a stripper","a prostitute","an erotic dancer","an escort","a stripper","a prostitute") );
				STAT.final_fate=temp;
					break;
		case "good_sex_job": temp = pick_one("a call girl","a successful porn star","a lingerie model","a high-class escort","an erotic dancer") );
				STAT.final_fate=temp;
					break;
		case "bad_sex_job": temp = pick_one("a second rate stripper","a streetwalker","a whore in a filthy brothel","a prostitute","a porn actress specialised on gang bangs and humiliation") );
				STAT.final_fate=temp;
					break;
		case "tri_sex_job":	temp = trio_end(
								pick_one("a call girl","a successful porn star","a lingerie model","a high-class escort","an erotic dancer"),
								pick_one("a call girl","a topless waitress","a stripper","a prostitute","an erotic dancer","an escort","a stripper","a prostitute"),
								pick_one("a second rate stripper","a streetwalker","a whore in a filthy brothel","a prostitute","a porn actress specialised on gang bangs and humiliation")
							);
				STAT.final_fate=temp;
					break;
		case "good_job":  temp = pick_one("an accountant","a chef","a dancer","a designer","an office worker") );
				STAT.final_fate=temp;
					break;	
		case "white_job":  temp = pick_one("an accountant","an office worker") );
				STAT.final_fate=temp;
					break;
					
 
				
			//TO DO 		
			case "wrestler": temp = pick_one("Mystique","The Beast","The Crimson Shadow","The Warrior","Lady Luck","Mystic") );
						return "<i>"+temp+"</i>";
						
		

	
		case "costume":  temp = pick_one("high heels, extremely tight pants and huge cleavages ","high heels, extremely short skirts and semi-transparent shirts ","tight catsuits, ballet heels and collars ","tight leather corsets, thigh-hight boots and long gloves ","various shiny semi-transparent dresses  ","sexy tight latex corsets and stockings ") );
					break;
		
	 
		case "blonde_joke": temp = pick_one("What is the difference between a blonde and a mosquito? When you slap the blonde she keeps on sucking. ",
					"What do the Bermuda Triangle and a blonde have in common?  They both swallow a lot of seamen. ",
					"What do screen doors and blondes have in common? The more you bang them, the looser they get. ",
					"Why do blondes love boob jobs?  It's really the only job they're qualified for. ",
					"Why did the blonde tattoo her zip-code on her thigh? She wanted a lot of male in her box. ",
					"How do you know if a Blonde has been using your computer? The joystick is still wet. "
					);
				break;				
		case "red_joke": temp = pick_one("How do you get a redhead’s mood to change? Wait 10 seconds","What is the difference between a redhead and a terrorist? You can negotiate with a terrorist. ") );
				break;	
		default: temp = "some error (the input in <i>random_name(input)</i> is invalid )";
				break;			
  };
  
  return temp
  
};


*/