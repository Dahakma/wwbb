import {settings} from "Settings";
import {game} from "Game";
import {currencies} from "Data/misc/currencies";
import {get} from 'svelte/store'
import {preprocess_text} from "Libraries/preprocess_text/index";
import {cap} from "Dh"; 
import {rg} from "Random"; 
import {opt} from "./opt"; 




const att_adjectives = {
	horny_2: ["constantly horny","sexually insatiable"],
	horny_1: ["shameless","horny"],
	sub_2: ["masochist","submissive","docile","subservient"],
	sub_1: ["submissive","obedient","meek","compliant"],
	dumb_2_bad: ["braindead","stupid","moronic"],
	dumb_2_neutral: ["dumb","dim-witted "],
	dumb_2_good: ["airhead","ditzy","silly"],
	dumb_1_bad: ["dumb","thick"],
	dumb_1_neutral: ["airhead","silly"],
	dumb_1_good: ["naive","simple"],
}





function replacer(text){
	const lowcap = text.toLowerCase();
	
	console.log(text);
	const att = get(game).player.att;
	if(text === "name") return att.name; 
	if(text === "asdf") return "lol"; 
		
		
	
	switch(text){
		case "prize": return settings.style.prize;
		case "money": return currencies[settings.style.currency].name;
		default: //empty;
	}
	
	
//HE X SHE
	switch(text){
		case "He": return ["He", "She", "They", "Xe"][att.sex];
		case "he": return ["he", "she", "they", "xe"][att.sex];
		case "Him": return ["Him", "Her", "Them", "Xem"][att.sex];
		case "him": return ["him", "her", "them", "xem"][att.sex];
		case "His": return ["His", "Her", "Their", "Xyr"][att.sex];
		case "his": return ["his", "her", "their", "xyr"][att.sex];
		default: //empty;
	}
	
//ATT ADJECTIVES
	if( ["horny", "dumb", "sub"].includes(lowcap) ){
		let out = rg.array( (()=>{
			switch(lowcap){
				case "horny":  return att.horny === 2 ? att_adjectives.horny_2 : att_adjectives.horny_1; 
				case "sub":  return att.sub === 2 ? att_adjectives.sub_2 : att_adjectives.sub_1; 
				case "dumb":  return att.dumb === 2 ? opt.end(att_adjectives.dumb_2_good, att_adjectives.dumb_2_neutral, att_adjectives.dumb_2_bad) : opt.end(att_adjectives.dumb_1_good, att_adjectives.dumb_1_neutral, att_adjectives.dumb_1_bad);
				default: return "";
			}
		})() );
			
		if(text !== lowcap) out = cap(out);
		return out;
	}
	

	
	return text;
}

/*
# Text Replacement 

- **Gender-based pronouns*: 
- `@He` - *He* x *She*
- `@he`
- `@His`, 
- `@his`, 
- `@Him`, 
- `@him` 
- **attributes adjectives** 
- `@Horny` - `att.horny` -  0 - nothing; 1 - e.g. *horny*; 2 - e.g. *sexually insatiable*
- `@horny`
- `@Sub`
- `@sub`
- `@Dumb`
- `@dumb`



*/

export const process = function(text, type){
	const options = {
		//language - default
		//english - default 
		fix: {
			typos: true, 
			numbers: true, 
		},
	
		markdown: {
			block: false,
			index: false,
		},	

		replace: {
			pattern: "@(\\w+)",
			fce: replacer,
		},
		
		units: {
			system: settings.style.units,
			full: true,	
		},
		
		distort: false,
	};
	
	if(type === "answer"){
		//TODO
	}else if(type === "answer_year"){
		options.fix.numbers = false;
	}else if(type === "question"){
		//TODO
	}else if(type === "sys_tex"){
		options.markdown.block = true;
	}
	
	
	return preprocess_text(text, options);

}



