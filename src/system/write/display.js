import {display} from 'System/display';
import {process} from 'System/write/process_text';

//TODO 
export const w = function(text){
	display.add({
		type: "w",
		text: process(text),
	});
}

export const pc = function(text){
	display.add({
		type: "pc",
		text: process(text),
	});
}

export const txt = function(text){
	display.add({
		type: "txt",
		text: process(text),
	});
}