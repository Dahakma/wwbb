import {display} from 'System/display';
import {game} from 'System/game/game';
import {game_default} from 'Src/variables';


export {rg, ra} from 'Random';

export {and, number, ordinal, number_plural, plural} from 'Dh';

export {w, pc, txt} from 'System/write/display';
export {change} from 'System/write/change';
export {random} from 'System/write/random';

export {opt} from 'System/write/opt';
export {check} from 'System/write/check';
export {people} from 'System/write/npc';

	
export const dim = {};	
export const npc = {};	
export const att = {};
export const round = {};
export const session = {};
export const mode = {};
export const stats = {};
export const settings = {};


/*
import {w, pc, txt} from 'System/write';
import {change} from 'System/write';
import {att, round, session} from 'System/write';
*/