import {game} from "Game";
import {user, save} from "System/user";
import {game_default, user_default} from "Variables";

export const change = {
	att: {},
	stats: {},
};

Object.keys(user_default.stats).forEach( key => {
	change.stats[key] = {
			to(value){
				user.stats[key] = value;
				save(user);
			},
			by(value){
				user.stats[key] += value;
				save(user);
			},
	}
	//TODO - SAVE!!!
});
	
	
	
Object.keys(game_default.player.att).forEach( key => {
	change.att[key] = {
			to(value){
				game.change("player", "att", key, value, undefined);
			},
			by(value){
				game.change("player", "att", key, undefined, value);
			},
			
			/*
			set plus(){
				game.change("player", "att", key, undefined, 1);
			},
			set minus(){
				game.change("player", "att", key, undefined, -1);
			},
			*/
	}
});


/*
import {w, pc, txt} from 'System/write';
import {change} from 'System/write';
import {att, round, session} from 'System/write';
*/