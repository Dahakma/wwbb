import {settings} from "Settings";
import {game} from "Game";
import {get} from 'svelte/store'
import {and} from "Dh"; 
import {rg} from "Random"; 
import {check} from "./check"; 
import {att} from "./index"; 

export const opt = {
//ENDINGS 
	end(...arg){
		
		switch(arg.length){
			default:
			case 2: return this.duo(...arg);
			case 3: return this.trio(...arg);
			case 4: return this.quadro(...arg);
		}
		
	},
	
	//Fairytale - 100% good
	//Normal - 50% good; 50% bad
	//Grimdark - 100% bad
	duo(good, bad){
		if(check.end >= 1){
			return good;
		}else if(check.end <= -1){
			return bad
		}else{
			return rg.g < 0.5 ? good : bad;
		}
	},
	
	//Fairytale - 66% good;  33% neutral
	//Normal - 33% good; 33% neutral; 33% bad;
	//Grimdark - 33% neutral; 66% bad
	trio(good, neutral, bad){
		const random = rg.g;
		 if ( (check.end > 0 && random > 0.33) || (check.end === 0 && random < 0.33) ){
				 return good;
		}else if ( (check.end < 0 && random < 0.66) || (check.end === 0 && random > 0.66) ){
				return bad;
		}else{
				return neutral;
		}
	},
	
	//Fairytale - 50% best; 50% good
	//Normal - 50% good; 50% bad
	//Grimdark - 50% bad; 50% worst
	quadro(best, good, bad, worst){
		const random = rg.g ;
		if (check.end > 0){ 
				return random < 0.5 ? best : good;
		}else if (check.end == 0){ 
				return random < 0.5 ? good : bad;
		}else{ 
				return random < 0.5 ? bad : worst;
		}
	},
	
	quatro(best, good, bad, worst){ 
		return this.quadro(best, good, bad, worst);
	},
	
//ATTRIBUTES
	resolve(key, two, one = ""){
		if(att[key] >= 2){
			return two;
		}else if(att[key] <= 0){
			return "";
		}else{
			return one;
		}
	}, 
	
	dumb(two, one){
		return this.resolve("dumb", two, one);
	},
	sub(two, one){
		return this.resolve("sub", two, one);
	},
	nympho(two, one){
		return this.resolve("nympho", two, one);
	},

	combine(...input){
		const output = rg.shuffle(input).splice(0, 3);
		return and(output);
	},

	random(...arg){ 
		if(typeof arg[0] === "object") arg = arg[0];
		return rg.array(arg);
	},

	girly_name(){
		/*
			let result = "";
	
			if(PC.done.goth){
				result = random_name("goth");
			}else if( PC.bimbo - mode.end >= 1){
				result = random_name("slut");
			}else{
				result = random_name("femaleShort");
			}
			
			PC.name = result;
			return result;
		*/
	},

}




/*
# Selecting from Arguments 

- **endings*: 
- `end(good, bad)`
	- fairytale - 100% good
	- normal - 50% good; 50% bad
	- grimdark - 100% bad
- `end(good, neutral, bad)`
	- fairytale - 66% good;  33% neutral
	- normal - 33% good; 33% neutral; 33% bad;
	- grimdark - 33% neutral; 66% bad
- end(best, good, bad, worst)
	- fairytale - 50% best; 50% good
	- normal - 50% good; 50% bad
	- grimdark - 50% bad; 50% worst
- **attributes** 
- `dumb(two, one)`
- `horny(two, one)`
- `sub(two, one)`
- `bimbo(total_bimbo, bimbo, woman, man)`
- `resolve(key, two, one)` - `resolve("horny", two, one)` === `horny(two, one)`
//- `combine(a, b, c, d)` returns 3 shuffled combined, e.g. *c, d and b*
- `random()`
//- `girly_name()`
*/
