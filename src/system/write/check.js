import {settings} from "Settings";
import {game} from "Game";
import {get} from 'svelte/store'
import {rg} from "Random"; 
import {att} from "./index"; 

export const check = {

	resolve(value){
		if(value >= 2){
			return true;
		}else if(value <= 0){
			return false;
		}else{
			return rg.g < 0.5;
		}
	},
	
	resolve_two(first, second){
		if(!first || !second) return false;
		const sum = first + second;
		if(sum >= 4){
			return true;
		}else if(sum <= 2){
			return rg.g <= 0.35;
		}else{
			return rg.g <= 0.7;
		}
	},
	
	get end(){
		return settings.chat.end;	
	}, 

	get good() {
		return this.resolve(this.end + 1)
	},
	
	get bad() {
		return !this.resolve(this.end + 1)
	},

	get worst(){
		return this.end < 0 && rg.b;
	},
	
	get best() {
		return this.end > 0 && rg.b;
	},
	
	get better() {
		const random = rg.g;
		if(this.end ===  0 && random < 0.36) return true;
		if(this.end > 0 && random < 0.75) return true;
		return false;
	},
	
	get worse() {
		const random = rg.g; 
		if(this.end ===  0 && random < 0.36) return true;
		if(this.end < 0 && random < 0.75) return true;
		return false;
	},
	

	get bimbo() {
		return this.resolve(att.bimbo);
	},
	
	get rich() {
		return this.resolve(att.money);
	},
	
	get horny() {
		return this.resolve(att.horny);
	},
	
	get sub() {
		return this.resolve(att.sub);
	},
	
	get dumb() {
		return this.resolve(att.dumb);
	},
	
	get horny_sub() {
		return this.resolve_two(att.horny, att.sub);
	},
	
	get dumb_sub() {
		return this.resolve_two(att.dumb, att.sub);
	},
	
}


/*

	
	get bodymods(){
		switch(PC.done.tat + PC.done.jew){
			case 4: return true;
			case 3: return rg.g < 0.66;
			case 2: return rg.g < 0.33;
			default: return false;
		}
	},
	
	get makeup(){
		return PC.done.lips && PC.done.mascara;
	},
	
	get huge_tits(){
		return PC.getDim("breastSize") > 40; //TODO!!!
	},

	
	get clothes() {
		return this.resolve(PC.done.sluty)
	},
	
	get trashy() {
		return trashy_factor() > 4;
	},
	
	get dicklet() {
		PC.getDim("penisSize") < 35;
	},
	
	get man(){
		return PC.original.sex === 0;
	},
	
	get woman(){
		return PC.original.sex === 1;
	},
	
	get feminised(){
		return PC.original.sex === 0 && PC.bimbo > -1;
	},
	
	get trap(){
		return PC.original.sex === 0 && PC.done.penis;
	},
	
	get shemale(){
		return PC.original.sex === 1 && PC.done.penis;
	},
	
	get cheats(){
		return half_cheater();
	},
	
	
	get bf(){
		return PC.fam.stat > 1;
	},
	
	*/
	
	
/*
# Checking stuff

- **ending**
- `end`
	- fairytale - 1
	- normal - 0
	- grimdark - -1
- `good`
	- fairytale - 100%
	- normal - 50% 
	- grimdark - 0%
- `bad`
	 - fairytale - 0%
	 - normal - 50% 
	 - grimdark - 100%
- `better`
	 - fairytale - 75%
	 - normal - 35% 
	 - grimdark - 0%
- `worse`
	 - fairytale - 0%
	 - normal - 35% 
	 - grimdark - 75%
- `best`
	 - fairytale - 50%
	 - normal - 0% 
	 - grimdark - 0%
- `worst`
	 - fairytale - 0%
	 - normal - 0% 
	 - grimdark - 50%	 
- **attributes**
- `horny`
	- `att.horny == 0` - 0%
	- `att.horny == 1` - 50%
	- `att.horny == 2` - 100%
- `sub`
- `dumb`
- `bimbo`
- `dumb_sub`
- `horny_sub`
*/
