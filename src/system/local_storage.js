import {clone, update} from 'Dh';
//import {settings_default, user_default} from 'Src/variables';


const prefix = "wwbb9";



/*
function get_original(key){
	switch(key){
		case "settings": return clone(settings_default);
		default: return {};
	}
}
*/



export const save = function(key, what){
	const actual_key = `${prefix}_${key}`;
	try {
		localStorage.setItem(actual_key, JSON.stringify(what) );
	} catch (error) {
	}
}

export const load = function(key, original = {}){
	const actual_key = `${prefix}_${key}`;
	original = clone(original); //to not change the original
	
	try {
		return JSON.parse( localStorage.getItem(actual_key) );
	} catch (error) {
		return original;
	}
}

export const initiate = function(key, original = {} ){
	const actual_key = `${prefix}_${key}`;
	original = clone(original); //to not change the original

	let current; 
	try {
		current = update(original, JSON.parse( localStorage.getItem(actual_key) )  );
	} catch (error) {
		current = original;
	}
	return current;
}


