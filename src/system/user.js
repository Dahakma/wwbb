import {initiate} from 'System/local_storage';
import {categories} from 'Data/questions/index';
//import {deliver_cats, deliver_tags} from 'System/deliver_data';
import {user_default} from 'Src/variables';


import {save as ls_save} from 'System/local_storage';

export const save = function(what){
	ls_save("user", what);
}


export const original = function(user){
	Object.keys(categories).forEach( database => {
		const data = categories[database];
		Object.keys(data.tags).filter( key =>  !data.tags[key].hidden ).forEach( key => user.tags[key] = 10 );
		data.questions.forEach( cat => user.categories[cat.id] = [0, 0] );
	});
	return user;
}

export const user = initiate("user", original(user_default) );

console.error(user);

