import {settings} from 'System/settings';



import {categories} from 'Data/questions/index';

export const deliver_cats = ()=> categories[settings.questions.database].questions;
export const deliver_tags = ()=> categories[settings.questions.database].tags;
export const deliver_meta = ()=> categories[settings.questions.database].meta;



import {names} from 'Data/names/index';

export const deliver_names = (database) => {
	return names[settings.style.names]?.[database] ? names[settings.style.names][database] : names.wwbb_default[database];
}


 