import {initiate} from 'System/local_storage';
import {save as ls_save} from 'System/local_storage';
import {settings_default} from 'Src/variables';

//import {load as ls_load} from 'System/local_storage';


export const save = function(what){
	ls_save("settings", what);
}

//creates svelte store 

/*

	save,
	load,

function create_setting(input){
	const { subscribe,  update } = writable(input);

	return {
		subscribe,
		update,

		//resets to default
		reset: () => update(setting => {
			reset_default("settings");
			return setting;
		}),

		//saves setting to localStorage
		save: () => update(settings => {
			save("settings", settings);
			return setting;
		}),
	}
}
*/

//export const settings = create_setting( initiate(settings_default, "settings")  ); 

export const settings = initiate("settings", settings_default);

/*
console.warn( "settings" )
console.warn( typeof settings )
console.warn( settings )
*/