import {display} from 'System/display';

export const keyboard = {
	items: [], 
	add(index, fce){
		this.items[index] = fce;
	},
	reset(){
		this.items.length = 0;
	},
}

document.addEventListener("keydown",  ({key}) => {
	if(typeof keyboard.items?.[key] === "function"){
		keyboard.items[key]();
		keyboard.reset();
	}
	if(key === "Escape"){
		display.back();
		keyboard.reset();
	}
});