import { writable, get } from 'svelte/store';

//creates svelte store 
function create(input){
	const { subscribe,  update } = writable(input);

	return {
		subscribe,
		update,
		
		screen: (screen, subscreen, subsubscreen) => update(display => {
			if(screen !== undefined)  display.screen = screen;
			if(subscreen !== undefined) display.subscreen = subscreen;
			if(subsubscreen !== undefined) display.subsubscreen = subsubscreen;
			return display;
		}),
	
		menu: () => update(display => {
			display.screen = "menu";
			return display;
		}),
		
		back: () => update(display => {
			if(display.subsubscreen){
				display.subsubscreen = "";
				return display;
			}else if(display.subscreen){
				display.subscreen = "";
				return display;
			}else{
				display.screen = "menu";
				return display;
			}
		}),

		media: (what, source) => update(display => {
			display.left.type = what;
			display.left.source = source;
			return display;
		}),
		
		reset: () => update(display => {
			display.right = [];
			return display;
		}),
		
		add: (what) => update(display => {
			if(what.type === "link"){
				what.link_index = ++(display.right.filter( a => a.type === "link" && !a.disabled ).length);
			}else if(what.type === "dead_link"){
				what.type = "link";
				what.link_index = -1-display.right.length; //TODO STUPID
				what.disabled = true;
			}
			display.right.push(what);
			return display;
		}),
		
		check: () => update(display => {
			console.warn(display);
			return display;
		}),
		
		promise: (promise) => update(display => {
			display.promise = promise;
			return display;
		}),
	}
}



export const display = create({
	screen: "menu",
	subscreen: "",
	subsubscreen: "",
	right: [], 
	left: {
		type: "none",
		source: undefined,
	},
	promise: ( new Promise( resolve => resolve() ) ),
}); 


