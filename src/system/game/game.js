import {writable} from 'svelte/store';
import {initiate, save} from 'System/local_storage';
import {game_default} from 'Src/variables';
import {clone} from 'Dh';
import {rg} from 'Random';
import {display} from 'System/display';
import {change} from 'System/write';
import {settings} from 'Settings';
import {deliver_text} from 'System/deliver_text';
import {categories, tagged} from 'System/questions/categories';
import {questions, ninja_swap} from 'System/questions/questions';


import {
	sex, 
	name, 
	archetype, 
	smugness, 
	appearance, 
} from 'System/game/game_start.js';

import {
	selection, 
	question, 
	answer,
	unending, 
	sarc_start,
} from 'System/game/game_questions.js';



import {
	to_final,
	final_selection,
	final_question,
	final_answer,
	ending, 
} from 'System/game/game_final.js';




function create_game(input){
	
	//console.warn(input);
	
	const { subscribe,  update  } = writable(input);

	return {
		subscribe,
		update,

//CHANGE OF VARIABLES 

		set: (what) => update(game => {

			game = what;

			return game;
		}),
		
		mode: (what) => update(game => {

			game.mode = what;

			return game;
		}),
		
		
		phase: (what) => update(game => {
			game.phase = what;
			return game;
		}),
		

		
		change: (major, minor, prop, to, by) => update(game => {
			if(game[major]?.[minor] ?.[prop] === undefined) return;
		
			if(to){
				game[major][minor][prop] = to;
			};
			
			if(by){
				game[major][minor][prop] += by;
			};
		
			return game;
		}),
		
		
//START 
		start: ()=> update(game => {
			const mode = clone(game.mode);
			game = clone(game_default);
			game.mode = mode;
			
			game.phase = "sex";
			
			game.player.seed = {
				game: (new Date()).getTime(),
				avatar: 789789,
				transformation:  949151,
				epilogue: 4919161,
			}
			
			game.session.round = -1; //immediatelly gets changed into 0
			
			save("game", game);
			
console.log("game start");
console.log(game);
			display.screen("game");
			return game;
		}),
		
		
		next: (phase)=> update(game => {
			if(phase) game.phase = phase; 
console.log(`NEXT ${game.phase}`);
console.log(game)

			display.reset();

		//GAMESTART
			if(game.phase === "sex"){
				sex("name");
			}else if(game.phase === "name"){
				name("archetype");
			}else if(game.phase === "archetype"){
				archetype("smugness");
			}else if(game.phase === "smugness"){
				smugness("appearance");
			}else if(game.phase === "appearance"){	
				appearance("new_round");
				//deliver_text("new_round");
			
		//only for CONTINUE
			}else if(game.phase === "unending"){	
				unending();
			}else if(game.phase === "selection"){	
				selection("question");

			}else if(game.phase === "question"){	
				question("answer");	
			}else if(game.phase === "answer"){	
				answer("new_question");	
				
			}else if(game.phase === "to_final"){	
				to_final("final_selection");
			}else if(game.phase === "final_selection"){	
				final_selection("final_question");	
			}else if(game.phase === "final_question"){	
				final_question("final_answer");	
			}else if(game.phase === "final_answer"){	
				final_answer("final_selection");
				
			}else if(game.phase === "ending"){	
				ending();	
				
			}else{



				if(game.phase === "new_question"){	
				
					game.session.question++;
					if(game.session.question < game.round.questions.length){
						game.phase = "question";
						question("answer");
					}else{
						game.phase = "sarc_start";
						sarc_start("new_round");
					}
				}
				
				if(game.phase === "new_round"){	
					game.session.tally[game.session.round] = {
						//selected: game.round.selected,
						right: game.round.right,
						wrong: game.round.wrong,
						questions: game.round.questions,
						answers: game.round.answers,
					};
					game.session.round++;
					game.session.question = 0;
	
					//unending
					if(game.mode.gameplay === "unending" && game.session.round !== 0 && game.session.round % settings.gameplay.unending_rounds === 0){
						game.phase = "unending";
						unending();
					}else if(game.mode.gameplay === "rounds" && game.session.round === game.mode.rounds){
						game.phase = "to_final";
						to_final();
					}else{
						game.phase = "selection";
						selection("question");
					}
				}
			
	
			}
			
			console.error(game);
			save("game", game);
			
			return game;
		}),
		
		

		
		
		
		resolve_question: (correct)=> update(game => {
			
			if(game.mode.schadenfreude) correct = !correct;
			
			if(settings.extra.gaslight && correct){
				if(rg.g < 1.25){
					const que = game.round.questions[ game.session.question ];
					if(que.level === 0){
						que.correct_choice--;
						if(que.correct_choice < 0) que.correct_choice = que.choices.length - 1;
						console.log(`Gaslight mode: correct answer "${que.correct}" changed to "${que.choices[que.correct_choice]}"`);
						que.correct = que.choices[que.correct_choice];
						correct = false;
					}else if(que.level === 2){
						console.log(`Gaslight mode: correct answer changed into a wrong one`);
						correct = false;
					};
				}
			}
			
			if(correct){
				game.round.answers.push(true);
				game.round.right++;
				game.session.right++;
				game.session.points += settings.style.prize;
			}else{
				game.round.answers.push(false);
				game.round.wrong++;
				game.session.wrong++;
			}
			return game;
		}),
		
		
		resolve_final_question: (correct)=> update(game => {
		
			if(game.mode.schadenfreude) correct = !correct;
			
			if(settings.extra.gaslight && correct){
				if(rg.g < 1.25){
					const que = game.round.questions[ game.session.question ];
					if(que.level === 0){
						que.correct_choice--;
						if(que.correct_choice < 0) que.correct_choice = que.choices.length - 1;
						console.log(`Gaslight mode: correct answer "${que.correct}" changed to "${que.choices[que.correct_choice]}"`);
						que.correct = que.choices[que.correct_choice];
						correct = false;
					}else if(que.level === 2){
						console.log(`Gaslight mode: correct answer changed into a wrong one`);
						correct = false;
					};
				}
			}
			
			
			//const bet = 
			
			game.session.points -= game.session.bet;
			
			if(correct){
				game.round.answers.push(true);
				game.round.right++;
				game.session.final_right++;
				game.session.money += game.session.bet;
			}else{
				game.round.answers.push(false);
				game.round.wrong++;
				game.session.final_wrong++;
			}
			
			game.session.tally.push({
				right: game.round.right,
				wrong: game.round.wrong,
				questions: game.round.questions,
				answers: game.round.answers,
			});
			
			return game;
		}),
		
		
		
		
		
		
		
		load_categories: ()=> update(game => {
			
			console.error("categories");
	
			const {catags, gameplay} = game.mode;
			
			const how_many = (()=>{
				if(catags === "mix") return game.mode.questions;
				if(gameplay === "unending") return settings.gameplay.unending_offered_cats;
				return game.mode.rounds + 1 + Math.ceil(game.mode.rounds / settings.gameplay.rounds_offered_cats);
			})();
			
			const seed = game.player.seed.game + game.session.round;
			
			if(catags === "mix"){ //new categories have to be mixed every round 
				game.session.categories = categories(how_many, seed);
			}else if(gameplay === "rounds" && game.session.categories.length){ //categories already initated   (in standard game they are initialized only at the beginning)
				//empty 
			}else{
				if(catags === "tags" ){
					game.session.categories = tagged(how_many, seed);
				}else if(catags === "cats" ){
					game.session.categories = categories(how_many, seed);
				}
			}
			
			return game;
		}),
		
		
		
		load_final_categories: ()=> update(game => {
			
			if(!game.session.final_round){
				game.session.final_round = true;
				game.session.selected.length = 0;
				game.session.won_points = game.session.points;
				
				const seed = game.player.seed.game + 10; //just to not have the same seed as the main game
				const how_many = settings.gameplay.final_questions + 1 + Math.ceil(settings.gameplay.final_questions / settings.gameplay.final_offered_cats);
			
				game.session.categories = categories(6, seed);
			}else{
				//already initiated
			}
			
			return game;
		}),
		
		
		
		load_questions: (category_index, category_name, selection_index, master_tag)=> update(game => {


			if(selection_index !== undefined) game.session.selected.push(selection_index);

			game.round = {
				category_name,
				category_index,
				right: 0,
				wrong: 0,
				questions: [],
				promised: [],
				answers: [],
			}
			
			const seed = game.player.seed.game + game.session.round;
			const how_many = game.session.final_round ? 1 : game.mode.questions;
			
			const pending = questions(
					category_index, //index of category or categories
					how_many,
					seed,
					master_tag,
			);

			const already_tried = pending.map( a => a );
			
			function load(a, index){
				return new Promise( (good, bad) => {
					let attempts = 5;
					function check_image(a){
						if(!a.img) return good(a);
						const image = new Image();
						image.src = a.img;
						image.onload = () => {
							good(a);
						}
						image.onerror = () => {
							/* eslint-disable no-console */
							console.warn(`WARNING: picture cannot be loaded! [cat: ${a.category}; lev: ${a.level}; que: ${a.question}] [index:   ${index}]`);
							console.warn(a.img);
							/* eslint-enable no-console */

//TODO - all failed attemts untested!!!
							if(attempts-- == 0) return bad(a); 
							
							//when image cannot be loaded, tries to switch the question for another one
							already_tried.push(a);
							const ninja = ninja_swap(a, already_tried, seed + already_tried.length);
							game.round.questions[index] = ninja;
							check_image(ninja);
						}
					}
					
					check_image(a);				
				})
			} 
			
			pending.forEach( (a,i) => {
				game.round.questions.push( a );
				game.round.promised.push( load(a,i) );
			});
			
			
			return game;
		}),
		
		
		final_zero_points: ()=> update(game => {
			game.session.points += 2 * settings.style.prize;
			return game;
		}),
		
		final_bet: (value)=> update(game => {
			game.session.bet = value;
			return game;
		}),
		
		end: ()=> update(game => {
			console.log("end");
			console.log(game);
			
			//EMLA LOCK 
			const lock = settings.extra.emla_lock;
			game.session.emla_lock = undefined;
			if(lock.enable){
				const minus = (game.session.right + game.session.final_right) * lock.minus; 
				const plus =  (game.session.wrong + game.session.final_wrong) * lock.plus; 
				const sum = plus - minus; 
				game.session.emla_lock = sum;
				
				const og_name =  ""; //game.player.att.name !== game.player.att.og_name ?  ` (${game.player.att.og_name}) ` : "";
	/*			
				let text = encodeURI(`
					xWho Wanna Be a Bimbo - ${game.player.att.name}${og_name} -
					+${plus}s (${game.session.wrong + game.session.final_wrong} wrong);
					-${minus}s (${game.session.right + game.session.final_right} right);
				`);
*/
				const text = encodeURI(`Who Wanna Be a Bimbo - ${game.player.att.name} - R${game.session.right + game.session.final_right} - W${game.session.wrong + game.session.final_wrong} `);
				
				const url = `https://api.emlalock.com/${sum > 0 ? "add" : "sub"}?userid=${lock.user_id}&apikey=${lock.api_key}&value=${Math.abs(sum)}&text=${text}`;
		
				const send = async function(){
					const response = await fetch(url);
					const data = await response.json();
					console.log(data);
					
					if (data.error) {
						throw new Error( `EmlaLock: ERROR (${data.error})`);
					} else {
						return `EmlaLock: ${sum > 0 ? "+" : ""}${sum} seconds `;
					}
				}
				send();
				
			}
			
			return game;
		}),
		
		
		
		
	}
	
	
	
	
}


export const game = create_game( initiate("game", game_default) ); 

