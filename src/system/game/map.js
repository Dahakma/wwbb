import {settings} from 'System/settings';

const background =  settings.style.bgr;
const foreground =  settings.style.text;
const highlight =  settings.style.high;


function parse(src){

	let [atlas, region] = src.map.split(";");
	const marks = Array.isArray(src.marks) ? src.marks.map( a => a) : [];
	
	const exclusive_zoom = (()=>{
		if(region && region.slice(-1) === "+"){
			region = region.slice(0,-1);
			return true;
		}else{
			return false;
		}
	})();
	
	const focus = (()=>{
			if(exclusive_zoom) return {region};
			
			switch(atlas){
				case "medi": return {regions: ["OM","GB","MA"]};
				case "ma": return {regions: ["MX","PA","TT"]};
				case "ua": return {regions: ["UA"]};
				case "eas": return {regions: ["CN","ID"]}; //east asia - china - indonesia
				case "was": return {regions: ["TR","IN"]}; //west asia - turkey - india
				case "ceu": return {regions: ["IE", "GR", "EE"]};
				default: break;
			}
			
			return undefined; 
	})();
	
	
	//ALIASES 
	atlas = (()=>{ switch(atlas){
			case "africa":
			case "afr": return  'af';
			case "as":
			case "eas":
			case "was":
			case "asia": return 'as';
			case "au":
			case "australia": return 'au';
			case "austria":
			case "at": return 'at';
			

			case "ca":
			case "canada": return 'ca';
			case "de":
			case "germany": return 'de';
			case "france":
			case "fr": return 'fr';
			case "italy":
			case "it": return 'it'; 
			case "poland":
			case "pl": return 'pl';
			case "england": 
			case "eng": 
			case "uk": return 'uk';

			case "eu":
			case "ua":
			case "ceu":
			case "europe": return 'eu';
			
			case "us":
			case "usa": return 'us';
			
			case "ma": 
			case "na": return  'na';
			
			case "sa": return 'sa';
			
			case "oc": return 'oc';
			
			case "medi":
			case "w":
			case "world":
			default: return 'w'; 
	}})();

	
	
	
	return {
		atlas, 
		region, 
		marks,
		//tolerance,
		focus,
		exclusive_zoom,
	}
	


}




const original_map = {
	regionsSelectable: false, //this map is passive - you cannot click on anything
	markersSelectable: false,
	
	backgroundColor: background,
	
	markerStyle: {
	  initial: {
		fill: highlight,
		stroke: highlight,
		r: 7,
	  },
	},
	
	zoomMin: 0,
	panOnDrag: true, //exclusive_zoom - only zoomed on one country & undragable
	
				
	regionStyle: {
	  initial: {
		fill: foreground, //exclusive_zoom hides other countries by making their color same as the background
		"fill-opacity": 1,
		stroke: 'none',
		"stroke-width": 0,
		"stroke-opacity": 1,
	  },
	  hover: {
		"fill-opacity": 0.8,
		cursor: 'pointer',
	  },
	  selected: {
		fill: /*exclusive_zoom ? color.foreground :*/ highlight, //exclusive_zoom displays only one region so it does not have to be highlighted
	  },
	  selectedHover: {
	  },
	},
	
	
	
	
}

Object.freeze(original_map);


export const create = function(code){
	
	const {
		atlas, 
		region, 
		marks,
		//tolerance,
		focus,
		exclusive_zoom,
	} = parse(code);
	
	
	const map = JSON.parse(JSON.stringify(original_map));
	
	map.onRegionTipShow = function(event, label){
		label.html( '???');
	 }; 	//won't survive cloning
	 
	map.map = atlas;
	
	if(exclusive_zoom){
		map.zoomMin = 2;
		map.panOnDrag = false;
		map.regionStyle.initial.fill = background;
		map.regionStyle.selected.fill = foreground;
	}
	
	if(region !== undefined){
		map.selectedRegions = [region];
	}

	if(marks.length){
		map.markers = marks.map( a =>{return {latLng: a} });
	}
/*
	if(xxx !== undefined && yyy !== undefined){
		map.markers = [{latLng: [yyy, xxx]}];
	}
*/

	if(focus){
		map.focusOn = focus;
	}

	return map;
	
}