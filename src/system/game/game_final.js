//import {writable} from 'svelte/store';
//import {initiate, save} from 'System/local_storage';
//import {game_default} from 'Src/variables';
//import {clone} from 'Dh';
import {get} from 'svelte/store';
import {display} from 'System/display';
import {settings} from 'Settings';
import {change} from 'System/write';
import {game} from 'System/game/game';
import {deliver_text} from 'System/deliver_text';
import {rg} from 'Random';
import {random} from 'System/write/random';
import {popup} from "Libraries/popup/index";

import {w, pc, txt} from "System/write/index";
import {att, stats} from "System/write/index";
import {ordinal, number_plural, number, plural} from 'System/write';

import {sarc_start} from 'System/game/game_questions.js';



//START OF THE FINAL ROUND 
export const to_final = function(next){
	
	if( get(game).session.points === 0){
		
		deliver_text("to_final_zero_points");
		
	
		display.add({
			type: `link`,
			text: `Accept`, 
			exec: ()=>{
				game.phase = "sarc_start";
				game.final_zero_points();
				display.reset();
				sarc_start("to_final");
			},
		});
		
		display.add({
			type: `link`,
			text: `End`, 
			exec: ()=>{
				/*
				game.phase = "end";
				display.reset();
				to_final();
				*/
				game.end();
				game.next("ending");
			},
		});
		
	}else{
		deliver_text("to_final");

		display.add({
			type: `link`,
			text: `Final Round`, 
			exec: ()=>{
				game.next(next);
			},
		});
	}
}



//FINAL CATEGORIES SELECTION
export const final_selection = function(next){
	game.load_final_categories(); //before betting!
	
	const index = (get(game).session.final_right + get(game).session.final_wrong);
	if(index === 0){
		game.final_bet( Math.round( get(game).session.points / 3 ) );
	}else if(index === 1){
		game.final_bet( Math.round( get(game).session.points / 2 ) );
	}else{
		game.final_bet(  get(game).session.points );
	}
	

	let key = 0;

	get(game).session.categories.map( (cat, i) => {
 		
		 if( get(game).session.selected.includes(i) ){
			return {
				type: `dead_link`,
				text: cat.name, 
			};
		}else{
			return { 
				key: ++key,
				type: `link`,
				text: cat.name, 
				exec: ()=>{
					game.load_questions(cat.index, cat.name, i, cat.master_tag);
					game.next(next);
				},
			}
		}
	}).forEach( item => display.add(item)  );
	
	
	/*


	var text = "";
	if(PC.smug>0){text += "Wow. <i>Zero points?</i> You are really one of the dumbest players I have ever seen and you really should feel ashamed. However we cannot pass a chance to torment you further.  "}
	else if(PC.smug==0){text += "No points before the Final Round means especially awful performance. Definitely one of the worst performances I have ever seen. However our viewers want to see the Final Round. "}
	else {text+="It seems that tonight you were especially unlucky. Entering the Final Round without any points is a rare feat. But we do not want to let you go empty-handed.  "};
	text += "I can offer you "+(2*con.reward)+" points - in exchange for one transformation. Then you can continue to the Final Round. Either this or you can end the game right now";
	add.w(text);
	*/
	
}	



//FINAL QUESTION
export const final_question = function(next){
	const que = get(game).round.questions[0];

	display.promise( get(game).round.promised[0]  );
		
	if(que.img){
		display.media("img", que.img);
	}else if(que.map){
		display.media("map", que.map);
	}
 
 	display.add({
		type: `w`,
		text: que.text, 
	});
	
	que.choices.forEach( (a,i) => display.add({
		type: `link`,
		text: a, 
		exec: ()=>{
			game.resolve_final_question(i === que.correct_choice);
			game.next(next);
		},
	}));
	
}



//FINAL ANSER 
export const final_answer = function(next){
	const que = get(game).round.questions[0];
	const correct = get(game).round.answers[0];
	const bet = get(game).session.bet;
	
	const index = (get(game).session.final_right + get(game).session.final_wrong);
	const end_final = index >= settings.gameplay.final_questions  || get(game).session.points === 0; //this triggers end //TODO

	
	display.media("avatar"); //TODO
	
	
	if(correct){
		w(`Answer *${que.correct}* is **right!**`);
		w(`You changed ${bet} points into ${bet} @money! `);
	}else{
		w(`You answer is **wrong!**`);
		w(`The correct answer was *${que.correct}*.`);
		w(`You lost ${bet} points! `);
	}
	
	if(end_final){
		display.add({
			type: `link`,
			text: `Finish`,  
			exec: ()=>{
				game.end();
				game.next("ending");
			},
		});
		
	}else{
		display.add({
			type: `link`,
			text: `Continue`,  
			exec: ()=>{
				game.next(next);
			},
		});
	}

}






export const ending = function(next){
	
	 
	deliver_text("ending");
	
	//EMLA LOCK
	const emla_lock = get(game).session.emla_lock;
	if(emla_lock !== undefined){
		if(emla_lock === null){
			w(`EmlaLock: ERROR`);
		}else if(emla_lock >= 0){
			w(`EmlaLock: +${emla_lock} seconds`);
		}else{
			w(`EmlaLock: ${emla_lock} seconds`);
		}
	}
	
	display.add({
		type: `link`,
		text: `New Game`,  
		exec: ()=>{
			display.screen("start", "", "");
		},
	});
	
	display.add({
		type: `link`,
		text: `Menu`,  
		exec: ()=>{
			display.screen("menu", "", "");
		},
	});
	
	
}

/*
const rounds =
const right = 
const questions =
const sucrate =
const points =
const money = 
*/