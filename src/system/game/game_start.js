import {get} from 'svelte/store';
import {display} from 'System/display';
import {settings} from 'Settings';
import {change} from 'System/write';
import {game} from 'System/game/game';
import {deliver_text} from 'System/deliver_text';
import {rg, ra} from 'Random';
import {random} from 'System/write/random';
import {popup} from "Libraries/popup/index";
import {w, pc} from "System/write/index";
import {att, stats} from "System/write/index";
import {archetypes} from 'Data/archetypes';


//DECIDE CHARACTERS SEX 
export const sex = function(next){
	display.media("none"); //TODO
	
	deliver_text("game_intro");

	display.add({
		type: `link`,
		text: `...sir`, 
		exec: ()=>{
			change.att.sex.to(0);
			game.next(next);
		},
	})
	display.add({
		type: `link`,
		text: `..madam`, 
		exec: ()=>{
			change.att.sex.to(1);
			game.next(next);
		},
	})
}


//DECIDE CHARACTERS NAME 
export const name = function(next){
	
	w(ra.array([`What is your name? `, `Could you introduce yourself? `]));
		
	rg.i();
	
	let list = [];
	
	const sex =  get( game ).player.att.sex; //att not initiated because deliver_text not triggered
	
	if( sex === 0 ){
		list = ["male", "male", "male", "male_all", "male_all"];
	}else{
		list = ["female", "female", "female", "female_all", "female_all"];
	}
		
	list.forEach( a => {
		const name = random[a]; 
		display.add({
			type: `link`,
			text: name, 
			exec: ()=>{
				change.att.name.to(name);
				game.next(next);
			},
		})
	});
	
	
	//previously user-inputed name
	{
		let name = ""; 
	
		if(sex === 0 && stats.names.male.length){
			name = rg.array(stats.names.male); //TODO - OR RA? 
		}else if(stats.names.female.length){
			name = rg.array(stats.names.female); 
		}
		
		if(name){
			display.add({
				type: `link`,
				text: name, 
				exec: ()=>{
					change.att.name.to(name);
					game.next(next);
				},
			})
		}
	}
	

	//allows inputing of custom name
	display.add({
		type: `link`,
		text: `Edit`, 
		exec: ()=>{
			popup.prompt(
				"Character name:",
				"Anon", 
				(verdict) => {
					if(verdict == undefined){
						display.reset();
						name();
					}else{
						change.att.name.to(verdict);
						
							//saves the user-inputed name
							const names = stats.names; 
							if(sex === 0){
								names.male.push(verdict); //TODO - UNIQUE!!
							}else{
								names.female.push(verdict);
							}
							change.stats.names.to(names);
		
						game.next();						
					}
				}, 
				()=> random[list[0]],
			);
		},
	})
	
	
	display.add({
		type: `link`,
		text: `Randomise`, 
		exec: ()=>{
			display.reset();
			name();
		},
	})
}




//DECIDE CHARCTERS ARCHETYPE
export const archetype = function(next){
	
	function offer_archetypes(sex){
		const available = archetypes.filter( archet => archet.sex === sex ); 
		const result = [];
		
		for(let i = 3; i && available.length; i--){
			const weight = available.map( archet => archet.weight );
			const index = ra.weighted(weight);
			result.push( available[index] );
			available.splice(index, 1); 
		}
		return result;
	}

	w(`Can you tell us what are you doing? `);

	offer_archetypes( get( game ).player.att.sex ).forEach( archet => {
		display.add({
			type: `link`,
			text: archet.name, 
			exec: ()=>{
				change.att.archetype.to( archet.id ); 
				game.next(next);
			},
		});
	});
}



//DECIDE CHARCTERS SMUGNESS
export const smugness = function(next){
	deliver_text("character_intro", next);
}


//DECIDE CHARCTERS APPEARANCE - TODO!!!
export const appearance = function(next){
	display.media("avatar");
	
	display.add({
		type: `w`,
		text: `Appearance. `,
	});
	
	display.add({
		type: `link`,
		text: `Start`, 
		exec: ()=>{
			game.next(next);
		},
	})
}