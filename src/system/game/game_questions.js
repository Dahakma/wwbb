//import {writable} from 'svelte/store';
//import {initiate, save} from 'System/local_storage';
//import {game_default} from 'Src/variables';
//import {clone} from 'Dh';
import {get} from 'svelte/store';
import {display} from 'System/display';
import {settings} from 'Settings';
import {change} from 'System/write';
import {game} from 'System/game/game';
import {deliver_text} from 'System/deliver_text';
import {rg} from 'Random';
import {random} from 'System/write/random';
import {popup} from "Libraries/popup/index";

import {w, pc} from "System/write/index";
import {att, stats} from "System/write/index";

import {to_final} from 'System/game/game_final.js';

//SELECT FROM CATEGORIES OF QUESTIONS	
export const selection = function(next){
	
	game.load_categories();
	
	//MIX of different questions (no selection, starts the round)
	if( get(game).mode.catags === "mix" ){
		const name = `Round ${get(game).session.round + 1}`; 
		display.add({
			type: `link`,
			text: name, 
			exec: ()=>{
				game.load_questions( get(game).session.categories.map( a => a.index),  name );
				game.next(next);
			},
		});		
		return; 
	}
	
	
	//when mode.gameplay == "rounds" categories are initiated only once and those once chosen cannot be chose again 
	//when mode.gameplay == "unending" categories are initiated every round and all could be chosen (because we do not know how many rounds there will be
	const disabling = get(game).mode.gameplay === "rounds" && get(game).mode.catags !== "mix"; 
	let key = 0;

 	get(game).session.categories.map( (cat, i) => {
 		
		 if(disabling && get(game).session.selected.includes(i) ){
			return {
				type: `dead_link`,
				text: cat.name, 
			};
		}else{
			return { 
				key: ++key,
				type: `link`,
				text: cat.name, 
				exec: ()=>{
					game.load_questions(cat.index, cat.name, i, cat.master_tag);
					game.next(next);
				},
			}
		}
	}).forEach( item => display.add(item)  );
		
}



//ASKS QUESTION
export const question = function(next){
	const que = get(game).round.questions[ get(game).session.question ];
	display.promise( get(game).round.promised[ get(game).session.question ]  ); //promise - wait until image is loaded 
	
	//console.warn(que)
	
	if(que.img){
		display.media("img", que.img);
	}else if(que.map){
		display.media("map", que.map);
	}
 
 	display.add({
		type: `w`,
		text: que.text, 
	});
	
	que.choices.forEach( (a,i) => display.add({
		type: `link`,
		text: a, 
		exec: ()=>{
			game.resolve_question(i === que.correct_choice);
			game.next(next);
		},
	}));
}



//ANSWER QUESTION
export const answer = function(next){
	const que = get(game).round.questions[ get(game).session.question ];
	const correct = get(game).round.answers[ get(game).session.question ];
	
	display.media("avatar"); //TODO
	
	if(correct){
		w(`Answer *${que.correct}* is **right!**`);
		w(`You won @prize points! `);
	}else{
		w(`You answer is **wrong!**`);
		w(`The correct answer was *${que.correct}*.`);
	}
	
	display.add({
		type: `link`,
		text: `Continue`,  
		exec: ()=>{
			game.next(next);
		},
	});
	
}




//TODO SARCOPHAGUS
export const sarc_start = function(next){
	const wrong = get(game).round.wrong; 
	
	w(`Sarcophagus TODO *${wrong}*`);
	
	display.add({
		type: `link`,
		text: `Continue`,  
		exec: ()=>{
			game.next(next);
		},
	});
}
	
	
	
	
	
//UNENDING MODE - continue X end 
export const unending = function(){
	w(`Do you want to continue? Or end the game? `);

	display.add({
		type: `link`,
		text: `Continue`, 
		exec: ()=>{
			game.phase = "selection";
			display.reset();
			selection("question");
		},
	});
	
	display.add({
		type: `link`,
		text: `Final Round`, 
		exec: ()=>{
			game.phase = "to_final";
			display.reset();
			to_final("final_selection");
		},
	});
}

