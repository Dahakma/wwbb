import {settings} from 'System/settings';
import {user} from 'System/user';

import {rg} from 'Random';

import {deliver_cats, deliver_tags} from 'System/deliver_data';
const tags = deliver_tags();
const cats = deliver_cats();


/**
 * returns category weight (how big is the chance that this category will be offered to the player 
 * @param {number} index - index of the category
 * @returns {Object} weight 
 * @returns {number} weight.weight - value 
 * @returns {string} weight.report - explanation for debugging purposes
 * @returns {string} weight.master_tag - "images", "map", "xmap" or "undefined"
 * @returns {string[]} weight.actual_tags - tags without conflicting and invalid ones
 */
export const category_weight = function(index){
	const cathy = cats[index];
	const report = [];
	

	//SYSTEM
		//remove finals - should not be here, used only in WWBB
		if(cathy.fin) return 0;
	
	
	//MASTER TAG & ACTUAL TAGS - basically the category cannot have both "map" and "xmap" tags and here is randomy decided which one it will be
		let actual_tags = cathy.tags.filter( tag => Object.keys(tags).includes(tag) ); //only real, defined tags, not everything
		const master_tag = (()=> {
			const map = cathy.tags.includes("map");
			const xmap = cathy.tags.includes("xmap");
			const images = cathy.tags.includes("images");

			if(images){
				return "images";
			}else if(map && xmap){ //BEWARE - assumes weight of at least one of the tags is possitive 
				{
					const master = ["map","xmap"][rg.weighted([user.tags.map, user.tags.xmap])];
					if(master === "map"){
						actual_tags = actual_tags.filter( tag => tag !== "xmap" );
					}else{
						actual_tags = actual_tags.filter( tag => tag !== "map" );
					}
					return master;
				}
			}else if(map){
				return "map";
			}else if(xmap){
				return "xmap";
			}
		})();


		
	//DISABLED CATEGORY LEVEL
		let disable = false; 
		
		//category has one of user-baned tags
		const banned = actual_tags.filter( key => user.tags[key] <= 0 );
		if(banned.length){
			report.push(`x 0 - the category has user-banned tags (${banned.join("; ")})`);
			disable = true;
		}
		
		//questions with maps are disabled
		if(  !settings.questions.enable_maps && ( actual_tags.includes("map") || actual_tags.includes("xmap") )  ){
			report.push(`x 0 - has tag "map" but maps are disabled`);
			disable = true;
		}
		
		//requires online content but it is disabled or there is no internet connection 
		if(  actual_tags.includes("images") ){
			if( !settings.questions.enable_images ){
				report.push(`x 0 - has tag "images" but content (images) downloaded from the Internet is disabled`);
				disable = true;
			}else if(!window.navigator.onLine){
				report.push(`x 0 - has tag "images" but no Internet conection is detected and the downloaded content (images) is not available`);
				disable = true;
			}
		}

		//questions with serious themes are disabled
		if(  !settings.questions.enable_serious && actual_tags.includes("serious")  ){
			report.push(`x 0 - has tag "serious" but serious themes are disabled`);
			disable = true;
		}
		
		//questions with tag "hidden"
		if( actual_tags.includes("hidden")  ){
			report.push(`x 0 - has tag "hidden"`);
			disable = true;
		}
		
		if(disable){
			return {
				weight: 0,
				report: report.join("<br>"),
				actual_tags,
			};
		}


	//BASE VALUE
		let weight =  4; //base chance
		report.push(`&nbsp${weight} - base weight`);

		//NUMBER OF QUETIONS
		const sum = cathy[0].length + cathy[1].length + cathy[2].length;
		if(sum > 40){ 
			weight += 3;
			report.push(`+3 - more than 40 questions`);
		}else if(sum > 30){
			weight += 2;
			report.push(`+2 - more than 30 questions`);
		}else if(sum > 20){
			weight++;
			report.push(`+1 - more than 20 questions`);
		}
	

	//NEWEST
		if(  actual_tags.includes("newest") ){
			weight += 5;
			report.push(`+5 - has tag "newest"`); //TODO!!!
		}



	//TAG BOOST
		let tag_boost = 1;
		actual_tags.forEach( key => {
			//if( user.tags[key] ) boost += (user.tags[key] - 1)
			if( user.tags[key] ) tag_boost *= user.tags[key]/10;
		});

		weight = Math.round(weight * tag_boost);
		report.push(`x ${tag_boost} - multiplier based on tags`); //TODO!!!
		

	report.push(`= ${weight}`);

	return {
		weight,
		report: report.join("<br>"),
		actual_tags,
		master_tag,
	};
}
