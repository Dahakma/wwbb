import {settings} from 'System/settings';
import {user} from 'System/user';

import {deliver_cats, deliver_tags} from 'System/deliver_data';
const tags = deliver_tags();
const cats = deliver_cats();

import {category_weight} from './category_weight';


/**
 * returns category weight (how big is the chance that this category will be offered to the player)
 * @param {string} tag - id of the tag
 * @returns {Object} weight 
 * @returns {number} weight.weight - value 
 * @returns {string} weight.report - explanation for debugging purposes
 * @returns {Object[]} weight.actual_cats - valid available categories with the tag
 * @returns {number} weight.actual_cats[].index - index of category
 * @returns {number} weight.actual_cats[].weight - weight of category
 * @returns {number} weight.actual_cats[].sum - number of questions in category
 */
export const tag_weight = function(tag){
	const report = [];
	const actual_cats = [];
		
		let disabled = false; 
		
		//hidden tag
		if( tags[tag] === undefined  ){
			report.push(`0 - not a valid tag`);
			disabled = true;
		}

		//hidden tag
		if( tags[tag].hidden  ){
			report.push(`0 - hidden tag - not offered during gameplay`);
			disabled = true;
		}

		//tag banned by user
		if( user.tags[tag] === 0 ){
			report.push(`0 - tag banned by user`);
			disabled = true;
		}

		//questions with maps are disabledd
		if(  !settings.questions.enable_maps && (tag === "map" || tag === "xmap") ){
			report.push(`0 - maps are disabled`);
			disabled = true;
		}

		//requires online content but it is disabled or there is no internet connection 
		if( tag === "images" ){
			if( settings.questions.enable_images ){
				report.push(`0 - content (images) downloaded from the Internet is disabled`);
				disabled = true;
			}else if(!window.navigator.onLine){
				report.push(`0 - no Internet conection is detected and the downloaded content (images) is not available`);
				disabled = true;
			}
		}
		
		//serious topics disabled disabled
		if(  !settings.questions.enable_serious && tag === "serious"){
			report.push(`0 - serious themes are disabled`);
			disabled = true;
		}

		//categories with the tag
		cats.forEach( (cathy, index) => {
			if( cathy.tags.includes(tag) ){
				const weight = category_weight(index).weight;
				if( weight > 0 ){
					actual_cats.push({
						index, 
						weight,
						sum:  cathy[0].length + cathy[1].length + cathy[2].length,
					});
				}
			}
		});
		const sum = actual_cats.reduce( (tot, a) => tot + a.sum, 0 );


		//no available valid categories 
		if(!actual_cats.length){
			report.push(`0 - no valid categories associated with the tag`);
			disabled = true;
		}


		if(disabled){
			return {
				weight: 0,
				report: report.join("<br>"),
				actual_cats,
			};			
		}

	
	//base value
		let weight =  2; //base chance
		report.push(`&nbsp${weight} - base weight`);
	
		if(sum > 200){ 
			weight += 3;
			report.push(`+3 - more than 200 valid questions`);
		}else if(sum > 100){
			weight += 2;
			report.push(`+2 - more than 100 valid questions`);
		}else if(sum > 50){
			weight++;
			report.push(`+1 - more than 50 valid questions`);
		}
	
		if(actual_cats.length > 6){ 
			weight += 3;
			report.push(`+3 - more than 6 valid categories`);
		}else if(actual_cats.length > 4){
			weight += 2;
			report.push(`+2 - more than 4 valid categories`);
		}else if(actual_cats.length > 2){
			weight++;
			report.push(`+1 - more than 2 valid categories`);
		}



	//tag boost
		const tag_boost = user.tags[tag]/10;
		weight = Math.round(weight * tag_boost);
		report.push(`x ${tag_boost} - user-adjusted boost`); //TODO!!!
		

	report.push(`= ${weight}`);


	const master_tag =  ["map", "xmap", "images"].includes(tag) ? tag : undefined;
		

	return {
		weight,
		report: report.join("<br>"),
		actual_cats,
		master_tag,
	};
}


