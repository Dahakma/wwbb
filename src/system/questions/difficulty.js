import {get} from 'svelte/store';
import {settings} from 'Settings';
import {game} from 'Game';
import {rg} from 'Random';
import {clamp} from 'Dh';

/*
	input 
		how_many - how many questions will there be in this round
		index - index of this question (e.g. the first question in this round)
		default_difficulty - could be adjusted before the start of the game, values  -1 easier; 0 normal; 1 harder
		dumb - effect of transformations on IQ, values -1 smart; 0 normal; 1 dumb; 2 very dumb 
	output
		0 - easy 
		1 - medium
		2 - hard 
*/
export const difficulty = function(index, how_many){
	const default_difficulty = get(game).mode.difficulty;
	const dumb = get(game).player.att.dumb;
	
	const difficulty = default_difficulty + dumb;
	
	//override - all hard or all easy
	if(difficulty <= -3) return 0;  // 100% - 0% - 0% 
	if(difficulty >= 3) return 2;  // 0% - 0% - 100%
	
	const random = rg.g;
	let out = 1;
	
	switch(difficulty){
		case -2: // 65% - 30% - 5%
			if(random < 0.65) out = 0;
			if(random > 0.95) out = 2;
			break;
			
		case -1: // 45%  - 40% - 15%
			if(random < 0.45) out = 0;   
			if(random > 0.85) out = 2;
			break;
			
		default:
		case 0: // 25% - 50% - 25%
			if(random < 0.25) out = 0;
			if(random > 0.75) out = 2;
			break;
			
		case 1: // 15% - 40% - 45%
			if(random < 0.15) out = 0;
			if(random > 0.55) out = 2;
			break;
			
		case 2: // 5% - 30% - 65%
			if(random < 0.5) out = 0;
			if(random > 0.35) out = 2;
			break;
	}
	
	// first and last questions are easier respectively harder 
	if(how_many >= 10){
		if(index === 0) out = 0;
		if(index === 1) out--;
		if(index === how_many - 2) out++;
		if(index === how_many - 1) out = 2;
	}else  if(how_many >= 5){
		if(index === 0) out--;
		if(index === how_many - 1) out++;
	}
	
	return clamp(out, 0, 2);
}

