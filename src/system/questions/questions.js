import {settings} from 'System/settings';

import {rg, ra} from 'Random';
import {process} from 'System/write/process_text';
import {cap, clamp} from 'Dh';

import {deliver_cats} from 'System/deliver_data';
const cats = deliver_cats();

import {difficulty} from './difficulty';




const get_unique_question = function(category, level, already_used){
	function get_available(level, everything = false){
		if( !cats[category]?.[level]?.length ) return []; //this level of difficulty doesn't exist in the category; or there are no questions 

		let indexes = [...Array(cats[category][level].length).keys()]; 
		
		if(everything) return indexes.map( i => [level, i] );

	//DISABLED QUESTION LEVEL
		//questions with maps are disabled
		if(  !settings.questions.enable_maps  ){
			indexes = indexes.filter( i => !cats[category][level][i].map );
		}

		//requires online content but it is disabled or there is no internet connection 
		if( !settings.questions.enable_images ||  !window.navigator.onLine){
			indexes = indexes.filter( i => !cats[category][level][i].img && !cats[category][level][i].hd );
		}
		
		//filters already used 
		indexes = indexes.filter( i => !already_used.some(a => a.category === category && a.level === level && a.question === i) );

		return indexes.map( i => [level, i] );
	}
	
	//desired difficulty 
	let available = get_available(level);

	//if none, tries different difficulty 
	if(!available.length){ 
		available = [...get_available(0), ...get_available(1), ...get_available(2)];
	}

	//if none, EMERGENCY, uses invalid questions too (user will notice but game won't break)
	if(!available.length){ 
		available = [...get_available(0, true), ...get_available(1, true), ...get_available(2, true)];
	}

	const [actual_level, question]  = rg.array(available);
	
	return {
		question,
		level: actual_level,
		category,
	};
}

















//MULTIPLE ANSWER - defaut simplest category of questions with multiple choices
function answers_multiple(category,level,question){
	let choices = cats[category][level][question].ans.map( a => a );
	
	let correct = choices.shift(); 
	choices = ra.shuffle(choices);
	
	choices.length = clamp(choices.length, choices.length, settings.gameplay.max_answers - 1);
	
	//inserts correct answer 
	const correct_choice = ra.integer(0, choices.length);
	choices.splice(correct_choice, 0,  correct);
	
	//preprocess text
	choices = choices.map( a => process(a, "answer") );
	correct = process(correct, "answer");
	
	return {
		correct_choice,
		correct,
		choices,
	};
}


//ONE ANSWER - there is only one answer, others are borrowed from other questions
function answers_one(category,level,question){

	let choices = (()=> { 
		const available = []; //levels could be subdivided into even smaller block containing only few related questions
		let lowest = 0; 
		let highest = cats[category][level].length - 1;

		if(cats[category].block){ //basically find block in which is the question present
			if(cats[category].block[level]){
				for(const block of cats[category].block[level]){
					const [low, high] = block;
					if(question >= low && question <= high){
						lowest = low; 
						highest = high;
						break;
					}
				}
			}
		}

		for(let ii = lowest; ii <= highest; ii++){
			if(ii == question) continue; //is the right answer, not add amoung wrong
			available.push( cats[category][level][ii].ans[0] );
		}
		
		return available;
	})();
	
	choices = ra.shuffle(choices);
	if(choices.length > settings.gameplay.max_answers - 1) choices.length = settings.gameplay.max_answers - 1; //worst case scenario - there are no available choices and the question will have only one single answer
	
	//inserts correct answer 
	let correct = cats[category][level][question].ans[0];
	const correct_choice = ra.integer(0, choices.length);
	choices.splice(correct_choice, 0,  correct);
	
	//preprocess text
	correct = process(correct, "answer");
	choices = choices.map( a => process(a, "answer") );
	
	return {
		correct_choice,
		correct,
		choices,
	};
	
}




//SORT - sorts items in the answer in random order
function answers_sorted(category,level,question){
	let choices = [];
		
	//disassemble
	let parts = cats[category][level][question].ans[0].split("§");
	const symbol = parts[1][0];
	parts = parts[2].split(";");
	
	//creates correct answer
	let correct = parts.join(` ${symbol} `); 
	choices.push(correct);
	
	//in loop to check for duplicates; only 100 cycles to not create endless loop if there are some issues
	for(let failsafe = 0; failsafe < 100; failsafe++){
		choices.push( ra.shuffle(parts).join(` ${symbol} `) );
			
		//removes possible duplicates and then checks if there are enough answers
		choices = [...new Set(choices)];
		if(choices.length >= settings.gameplay.max_answers) break;
	}

	//shuffle
	choices = ra.shuffle(choices);
		
	//preprocess text
	correct = process(correct, "answer");
	choices = choices.map( a => process(a, "answer") );
	
	//finds the index of correct answer 
	const correct_choice = choices.indexOf(correct);

	return {
		correct_choice,
		correct,
		choices,
	};
	
}










//NUMBER - create questions with random nubers 
function answers_numbers(category,level,question){
	
	let emergency = false; //the inputed range is not big enough to accomodated all desired generated numbers
	let possible_lower_answers = settings.gameplay.max_answers;
	let possible_higher_answers = settings.gameplay.max_answers;
		
			
	//ADJUSTMENT: (adjust/2) + ( Math.random()*(adjust/2) ) 
		function random_number(number, adjust, up){
			let mod = Math.round(adjust / 2) + Math.round(Math.random() * (adjust / 2));
			if(emergency) mod = minimal_adjust;
			//if(emergency)mod = Math.ceil(adjust+Math.random());//emergency mode - just ignore everything and hope for best
			if(mod < 1) mod = 1;
			return up ? number + mod : number - mod;
		}

/*		
	function dateIntoMiliseconds(string){ //assumes format day/moth/year
	//console.log(string);
		let [day, month, year] = string.split("/");
		//fucking months are indexed from zero
		day = ++day; //there are no zero days
		const date = new Date(+year, +month, +day);
		return date.getTime();
	}
	
	function fuckingMiliseconds(string){ //assumes format day/moth/year
		let [day, month, year] = string.split("/");
		const mpd = 86400000;
		day = +day + (30 * +month) + (30 * 12 * +year);
		return day * mpd;
	}
	
	function milisecondsIntoDate(miliseconds){
		//TODO CUSTOMISE MORE
		//const loc = mode.imperial ? "en-GB" : "en-US";
		//let [day, month, year] = new Date(miliseconds).toLocaleDateString("en-GB").split("/")
		//in imperial day is month and month is day!
//		console.log("KO");
		let b = new Date(miliseconds).toLocaleDateString(mode.imperial ? "en-US" : "en-GB");
	//	console.log(b);
		return b; // new Date(miliseconds).toLocaleDateString(mode.imperial ? "en-US" : "en-GB")
	}
*/			
		
	//PARSE INPUT
		//turns §6§1812§>1799§<1900 or §600§1538§°C into question
		//the first part is always set - §6§1812 or  §600§1538
		const temp = cats[category][level][question].ans[0].split("§");

		let adjust = temp[1];
		let correct_number = temp[2];

		//rest is optional - §>1799§<1900 or §°C
		let minimum = void 0;
		let maximum = void 0;
		let common_part = "";
		
		for(let i = 3; i <= 5; i++){
			const part = temp[i];
			if(part){
				if(part[0] == ">"){
					minimum = part.slice(1);
				}else if(part[0] == "<"){
					maximum = part.slice(1);
				}else{
					common_part = part;
				}
			}
		}
		
			
		//TODO
		//if the input includes date day/month/year instead of numbers 
		if(common_part === "date"){
			/*
			correct_number = dateIntoMiliseconds(correct_number);
			adjust = fuckingMiliseconds(adjust);
			if(minimum) minimum = dateIntoMiliseconds(minimum);
			if(maximum) maximum = dateIntoMiliseconds(maximum);
			*/
		}else{
			correct_number = +correct_number.replaceAll("_","");
			adjust = +adjust.replaceAll("_","");
			if(minimum) minimum = +minimum.replaceAll("_","");
			if(maximum) maximum = +maximum.replaceAll("_","");
		}

	//check how many lower/higher questions would fit in the set limits
		const minimal_adjust = (adjust/2 + 1);
		
		if(minimum){
			possible_lower_answers = 0;
			let ii = correct_number - minimal_adjust;
			while(ii > minimum && possible_lower_answers < settings.gameplay.max_answers){
				possible_lower_answers++;
				ii -= minimal_adjust;
			}
		}
		
		if(maximum){
			possible_higher_answers = 0;
			let ii = correct_number + minimal_adjust;
			while(ii < maximum && possible_higher_answers < settings.gameplay.max_answers){
				possible_higher_answers++;
				ii += minimal_adjust;
			}
			
		}
	
		/* DONT DELETE ALTERNATIVE ALGORITM
		place the right answer somewhere randomly with enough space left for other lower/higher answers
		correct =  (con.answers - possible_higher_answers) + Math.floor(Math.random()*(1+possible_lower_answers-(con.answers-possible_higher_answers)));
		*/
	
		//emergency mode - there is not enough space for demanded number of questions
		if( possible_lower_answers + possible_higher_answers < settings.gameplay.max_answers){
			emergency = true;
			possible_lower_answers = settings.gameplay.max_answers;
			possible_higher_answers = settings.gameplay.max_answers;
		//	console.warn("There could NOT be "+settings.gameplay.max_answers+" numbers in range "+minimum+" to "+maximum+" with a minimal margin "+adjust+". The range will be ignored. (category: "+category+"; level: "+level+" question: "+question+") ");//SIC
		}
		
		
	//adds all questions 
		let choices = [];
		
		//lower
		let number = correct_number;
		for(let ii = 0; ii < possible_lower_answers; ii++){
			number = random_number(number, adjust, false);
			if(number <= minimum) break;
			choices.push(number);
		}
		choices.reverse();
	
		//correct
		choices.push(correct_number);
	
		//higher
		number = correct_number;
		for(let ii = 0; ii < possible_higher_answers; ii++){
			number = random_number(number, adjust, true);
			if(number >= maximum) break;
			choices.push(number);
		}

//TODO - CASES WHEN THERE ARE NOT ENOUGH QUESTIONS		
	
	

	//cuts the superfluous ones
		while(choices.length > settings.gameplay.max_answers){
			let top = ra.b;
			if(choices[0] === correct_number) top = false;
			if(choices[choices.length - 1] === correct_number) top = true;
			if(top){
				choices.shift();
			}else{
				choices.pop();
			}
		}


	//finds thre right one
	const correct_choice = choices.findIndex( a => a === correct_number );
	
	//turns number into string 
	
	//adds common part
	if( common_part.startsWith("@y") ){
		choices = choices.map( a => `${a}` );	
	}else if( common_part[0] === "@" ){
		choices = choices.map( a => `${a}${common_part}` );
	}else{
		choices = choices.map( a => `${a} ${common_part}` );
	}

	//preprocess text
	if( common_part.startsWith("@y") ){
		choices = choices.map( a => process(a, "answer_year") ); //YEAR ISN'T NUMBER
	}else{
		choices = choices.map( a => process(a, "answer") );
	}

	//PADSTART - i.e. align right; 
	const max = Math.max( ...(choices.map( a => a.length )) );
	choices = choices.map( a =>  a.padStart(max, ` `) ); //TODO
	
	return {
		correct: choices[correct_choice],
		correct_choice,
		choices,
	};
	
}
	

 
	
//don't remember point of this but it makes some pictures work
function encode(adress){
	if(!adress) return adress;
	
	 adress = adress.replace("§WCT", "https://upload.wikimedia.org/wikipedia/commons/thumb");
	 
	adress = encodeURIComponent(adress)

	adress = adress.replace("%3A", ":");
	adress = adress.replace(/%2F/g, "/");
	adress = adress.replace(/%25/g, "%");
	adress = adress.replace(/%E2%80%99/g, "'");
	adress = adress.replace(/%3F/g, "?"); //TODO CHECK
	adress = adress.replace(/%3D/g, "="); //TODO CHECK

	return adress;
}




//INITIATE ANSWERS
/**
 * returns question with answers
 * @param {number} category - index of the category 
 * @param {number} level - difficulty (0 easy; 1 medium; 2 hard)
 * @param {number} question - index of questions
 * @returns {Object} question - question
 * @returns {number} question.category - index of the category
 * @returns {number} question.level - index of the level
 * @returns {number} question.question - index of the questions
 * @returns {string} question.category_name - name of the category
 * @returns {string} question.text - displayed text of the questions
 * @returns {string|undefined} question.img - link to image
 * @returns {string|undefined} question.hd - link to higher definition image
 * @returns {string|undefined} question.map - map code
 * @returns {string} question.correct - text of the correct answer
 * @returns {number} question.correct_choice - index of the correct choice
 * @returns {string[]} question.choices - displayed text of the offered choices 
 * @returns {number} question.correct_choice - index of the correct choice
*/
export const answers = function(category, level, question){
	const cathy = cats[category];
	const quest = cats[category][level][question];
	const answ = cats[category][level][question].ans;

	 //text of the questions 
	const text = process((()=> {
		if(quest.que){
			if(cathy.shared &&  cathy.shared.indexOf("XXX") !== -1){
				return cathy.shared.replace("XXX", quest.que);
			}else{
				return quest.que;
			}
		}else{
			return cathy.shared; 
//TODO - IMPLICIT!!
		}
	})(), "question");
	
	//multimedia

	const map = (()=>{
		if(quest.map === undefined){
			return undefined;
		}
		return {
			map: quest.map,
			marks: quest.marks,
			//TODO - shared maps or tolerance? 
		}
	})();

	const {img, hd} = (()=> {
		let img = encode(quest.img);
		let hd =  encode(quest.hd);

		//DISABLED JUST TO BE SURE LEVEL - NULL - forcedfully disabled
		if(!settings.questions.enable_images ||  !window.navigator.onLine){
			img = null;
			hd = null;
		}

		return {img, hd};
	})();

	//answers 
	const {correct, correct_choice, choices} = (()=>{
		//MULTIPLE - default simplest category of questions with multiple answers
		if(answ.length > 1){
			return answers_multiple(category, level, question);
		
		//SORTED A > B > C > D
		}else if( answ[0].startsWith("$>") || answ[0].startsWith("$>") ){
			return answers_sorted(category, level, question);
		
		//RANDOM NUMBER
		}else if(answ[0][0] == "§"){
			return answers_numbers(category, level, question);
			
		//LINE - there is only one answer, others are borrowed from other questions
		}else{
			return answers_one(category, level, question);
		}
	})();

	const category_name = cap(cathy.name);

	return {
		category,
		level,
		question,
		category_name,
		text,
		img,
		hd,
		map,		
		correct,
		choices,
		correct_choice,
	}
}






/**
 * returns list of random questions
 * @param {number|number[]} category - index of the category or array of indexes of multiple categories 
 * @param {number} how_many - number of returned questions
 * @param {number} [seed] - seed for random seeded generation
 * @param {string} [master_tag] - atm "map" X "xmap"
 * @returns {Object[]} list - list of questions
 * @returns {number} list[].category - index of the category
 * @returns {number} list[].level - index of the level
 * @returns {number} list[].question - index of the questions
 * @returns {string} list[].text - displayed text of the questions
 * @returns {string} list[].correct - text of the correct answer
 * @returns {number} list[].correct_choice - index of the correct choice
 * @returns {string[]} list[].choices - displayed text of the offered choices 
 */
export const questions = function(category, how_many, seed, master_tag){
	rg.i(seed);
	const list = [];
	const categories = (()=>{
		if(typeof category === "number"){ //all questions are from the same category 
				return Array(how_many).fill(category);
		}else{ 
			let temp = [...category]; //cloned to not change input
			if(temp.length > how_many){ //more than required
				temp.length = how_many;
			}else if(temp.length < how_many){ //less than required
				while(temp.length <= how_many) temp = [...temp, ...temp]; //rought and approximate but should work in emergency cases
				temp.length = how_many;
			}
			return temp;
		}
	})();
	
	categories.forEach( (category, question)  => {
			const level = difficulty(question, how_many);
			list.push( get_unique_question(category, level, list) );
	});
	

	return list.map( ({category, level, question}) => answers(category, level, question, master_tag) );
}



/**
 * replace question with another 
 * @param {Object} swapped - swapped question
 * @param {Object[]} already_used - list of all already used or tried questions (to make sure it is unique)
 * @param {number} [seed] - seed for random seeded generation
 * @returns {Object} - new question
*/
export const ninja_swap = function(swapped, already_used, seed){
	rg.i(seed);
	const {category, level, question} = get_unique_question(swapped.category, swapped.level, already_used);
	return answers(category, level, question) ;
}







 
	
	
	