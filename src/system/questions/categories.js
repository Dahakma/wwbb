import {settings} from 'Settings';
import {user} from 'System/user';

import {rg} from 'Random';
import {cap} from 'Dh';


import {category_weight} from './category_weight';
import {tag_weight} from './tag_weight';

import {deliver_cats, deliver_tags} from 'System/deliver_data';
const tags = deliver_tags();
const cats = deliver_cats();


 
	
/**
 * returns list of random categories available to be chosen by the player
 * @param {number} how_many - number of categories
 * @param {number} [seed] - seed used for seeded generation
 * @returns {Object[]} list - list of items with categories
 * @returns {string} list[].name - displayed name
 * @returns {number[]} list[].index - array of indexes of categories
 * @returns {string} list[].master_tag - atm map X xmap
 */
export const tagged = function(how_many, seed){ 
	rg.i(seed);
	const list = [];
	
	const tagged_array = Object.keys(tags);
	const tagged_details = tagged_array.map( key => tag_weight(key) );
	let tagged_weight = tagged_details.map( a => a.weight );


	//EMERGENCY - there are not enough valid categories! 
		const valid_tags = tagged_weight.filter( a => a ).length;
		const reuse_tags = valid_tags && (how_many > valid_tags);

		 /* eslint-disable no-console */
			if(valid_tags === 0){
				console.error(`ERROR - no valid tags! (Will treat all tags as valid)`);
				tagged_weight = tagged_weight.map( ()=> 1 );
			}		
			if(reuse_tags){
				console.warn(`WARNING - not enough valid categories! (Valid categories will be reused)`);
			}
		/* eslint-enable no-console */


	//randomly selects based on the weight 
	for (let i = 0; i < how_many; i++){
		const index = rg.weighted(tagged_weight);
		if(!reuse_tags) tagged_weight[index] = 0; 


		//select categories
		const categories = [];
		const cat_weight = tagged_details[index].actual_cats.map( c => c.weight );
		for (let k = 0;  k < settings.gameplay.questions.max; k++){
			const cat_index = rg.weighted(cat_weight);
			categories.push(  tagged_details[index].actual_cats[cat_index].index ); //tagged_details[index].actual_cats[cat_index] - is the index of the category
		}

		list.push({
			name:  cap( tags[tagged_array[index]].name ?? tagged_array[index] ),
			index: categories, //indexes of categories
			master_tag: tagged_details[index].master_tag,
		});
	}
	
	return list;
}




/**
 * returns list of random categories available to be chosen by the player
 * @param {number} how_many - number of categories
 * @param {number} [seed] - seed used for seeded generation
 * @returns {Object[]} list - list of items with categories
 * @returns {string} list[].name - displayed name
 * @returns {number} list[].index - index of the category
 * @returns {string} list[].master_tag - (at the moment map X xmap)
 */
export const categories = function(how_many, seed){ 
	
	rg.i(seed);
	const list = [];
	const categories_details = cats.map( (a, i) => category_weight(i) );
	let categories_weight = categories_details.map( a => a.weight );

	
	//EMERGENCY - there are not enough valid categories! 
		const valid_categories = categories_weight.filter( a => a ).length;
		const reuse_categories = valid_categories && (how_many > valid_categories);

		 /* eslint-disable no-console */
			if(valid_categories === 0){
				console.error(`ERROR - no valid categories! (Will treat all categories as valid)`);
				categories_weight = categories_weight.map( ()=> 1 );
			}		
			if(reuse_categories){
				console.warn(`WARNING - not enough valid categories! (Valid categories will be reused)`);
			}
		/* eslint-enable no-console */

	
	//randomly selects based on the weight 
	for (let i = 0; i < how_many; i++){
		const index = rg.weighted(categories_weight);
		if(!reuse_categories) categories_weight[index] = 0; 
		list.push( get_properties( index, categories_details[index] ) );
	}
	
	return list;
}


function get_properties(index, details){ 

	let name = cap(cats[index].name);
	details.actual_tags.forEach( tag => {
		if( tags[tag].suffix ) name = `${name} ${tags[tag].suffix}`;
	})

	return {
		index,
		name, 
		master_tag: details.master_tag,
	}
}