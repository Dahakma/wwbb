import {att} from 'Data/character_attributes';
export const version = "0.9.0"; //window.__VERSION__; //TODO - doesnt work during build
export const debug = true;
window.debug = true;

export const meta = {
	discord: "https://discord.gg/mwtVmAs",
	itch: "https://dahakma.itch.io/who-wanna-be-a-bimbo",
	gitlab: "https://gitlab.com/Dahakma/wwbb", 
	patreon: "https://www.patreon.com/Dahakma",
	website: "https://dahakma.maweb.eu",
}



//SETTING 
import {color_schemes} from "Data/misc/color_schemes";
const darkmode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches; //user preference for dark schemes
const yank =  navigator.language === "en-US"; //based on language assumed the user is from the USA 

export const user_default = {
	tags: {}, //user-adjusted weight of tags 
	categories: {}, //stats of categories 
	stats: {
		names: { //user-inputed names
			male: [],
			female: [], 
		},
		sessions: 0, //played games 
	},
}

export const game_default = {
	mode: {
		transform: "stan", 
		/*
			stan - standard 
			leev - lesser evil
			alta - all transformations, bimbofication quarantee
		*/
		catags: "cats", 
		/*
			cats - categories
			tags - tags
			mix - mix of questions from different categories
		*/
		schadenfreude: false, 
		gameplay: "unending",
		/*
			rounds
			unending
		*/
		questions: 5, //questions in one round
		rounds: 5, //rounds in the whole game
		difficulty: 0, //adjustment of difficulty of questions -1, 0, 1
	},
	round: {
		category_name: "", //name of the category / tag
		category_index: 0, //index of used category / categories
		right: 0, //right anwers in this round
		wrong: 0, //wroing answers in this round
		questions: [], //loaded questions 
		promised: [], //used to load questions
		answers: [], //correct/incorect answers to questions
		transformations: [], //transformations in this round
	},
	session: {
		question: 0, //index of currently active question
		round: 0, //index of currently active round
		right: 0, //right answers total in whole game
		wrong: 0, //wrong answers total in whole game 
		selected: [], //selected categories
		tally: [], //stats of different rounds 
		categories: [], //loaded cateories
		final_round: false, //final round in progress
		final_right: 0, //right in the final round
		final_wrong: 0, //wroing in the final round
		points: 0, //active points
		won_poinst: 0, //total won ponts 
		money: 0, //won money
		transformations: [], //all transformations
		emla_lock: 0,
	},
	phase: undefined,
	player: {
		seed: {
			avatatar: undefined,
			transformation: undefined,
			epiloque: undefined,
			game: undefined,
		}, 
		att,
		dim: {},
		npc: {},
	},
}



export const settings_default = {
	gameplay: {
		questions: {
			min: 1,
			max: 10,
		},
		rounds: {
			min: 1,
			max: 10,
		},
		unending_offered_cats: 3, //offered 3 categories of questions 
		rounds_offered_cats: 3, //offered "number_of_rounds + 1 + Math.ceil(number_of_rounds / rounds_offered_cats)" 
		final_offered_cats: 3, //offered "number_of_rounds + 1 + Math.ceil(number_of_rounds / final_offered_cats)" 
		final_questions: 3, //questions in the final round
		final_zero_points: 2, //if zero points, offered X * prize
		final_zero_points_transformations: 1, //in exchange for X transformations
		unending_rounds: 2, //asked if wants to end every X rounds
		leev_cards: 3, //offered cards with transformations
		exchange_cards: 1, //only X cards could be exchanged per round 
		exchange_price: 2, //for 1 exchanged transformation the player gets X new
		max_answers: 4, //maximum amount of displayed answers to question 
	},
	
	style: {
		text: darkmode ? color_schemes.dark.text : color_schemes.light.text,  //color of the main text
		bgr: darkmode ? color_schemes.dark.bgr : color_schemes.light.bgr, //color of the background
		high: darkmode ? color_schemes.dark.high : color_schemes.light.high, //color of highlighted links and buttons 
		units: yank ? "us" : "si", //preferable units cm x inches etc. 
		currency:  yank ? "usd" : "eur", //monetary prize currency
		names: "wwbb_default",
		prize: 1000,
	},
	questions: {
		database: "wwbb_default", //used database of questions
		enable_maps: true,
		enable_images: true, 
		enable_serious: false,
		prefer_hd: false,
	},
	extra: {
		emla_lock: {
			enable: false,
			user_id: "",
			api_key: "",
			plus: 10,
			minus: 0,
		},
		gaslight: false,
	},
	chat: {
		end: 0,
	},
/*
	background:  "hsla(50, 75%, 75%, 1)", //background (neutral color) //#efdf8f
	background_2: "hsla(44, 92%, 48%, 1)", //background of the whole page //#ebaf0a
	highlight: "hsla(10, 100%, 40%, 1)", //important, buttons, headlines //#c20

	text: "hsla(10, 90%, 20%, 1)", //default color of text //#611405
	enable_online: true, //online context (pictures downloaded from wikimedia commons)
	enable_maps: true, //jvector maps 
	
	difficulty: 0, 
*/
}
Object.freeze(settings_default);
