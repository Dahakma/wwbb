export const archetypes = [
	{
		id: `secretary`, //valid variable name (no spaces, capitalization, special symbols)
		name: `Secretary`, 
		weight: 1, //probability of being offered 
		sex: 1, //gender: 0 man; 1 woman
		intro: `secretary`, //introduction text (src/data/texts/character_intros/)
		epilogue: `secretary`, //default epilogue (src/data/texts/epilogues/)
	},
	{
		id: `lawyer`, //valid variable name (no spaces, capitalization, special symbols)
		name: `Lawyer`, 
		weight: 1, //probability of being offered 
		sex: 0, //gender: 0 man; 1 woman
		intro: `lawyer`, //introduction text (src/data/texts/character_intros/)
		epilogue: `lawyer`, //default epilogue (src/data/texts/epilogues/)
	},
];

/*
const archet = [
	{
		name:	"Default (some error)",
		id: 	0,
		condit: 0,
		bias: 	0.4,
		random: 0.5,//	0.8,
		sex : 	1,
		epilogue: undefined,
	},{
		name:	"Secretary",
		id:		101,
		condit:	0.6, 
		bias:	0.4,
		random: 0.4,//	0.8,
		sex: 	1,
		epilogue: EPILOGUE.secretary,
	},{
		name:	"Librarian",
		id:		102,
		condit:	0.6,
		bias:	0.3,
		random: 0.5,//	0.8,
		sex:	1,
		epilogue: EPILOGUE.librarian,
		fallback: 101,
	},{
		name:	"Teacher",
		condit:	0.6,
		id:		104,
		bias: 	0.4,
		random: 0.4,// 0.8,
		sex: 	1,
		epilogue: EPILOGUE.teacher,
		fallback: 102,
	},{
		name:	"CEO",
		condit:	0.6,
		id:		103,
		bias: 	0.5,
		random: 0.4,// 0.8,
		sex: 	1,
		epilogue: EPILOGUE.ceo,
		fallback: 101,
	},{
		name : "Actress",
		id: 	201,
		condit:	0.6,
		bias:	0.6,
		random: 0.3,//	0.8,
		sex:	1,
		epilogue: EPILOGUE.actress,
	},{
		name : "Guitar player",
		id:		202,
		condit:	0.6,
		bias:	0.4,
		random: 0.4,//	0.8,
		sex:	1,
		epilogue: EPILOGUE.guitar,
		fallback: 201,
	},{
		name : "Wrestler",
		id:		301,
		condit:	0.6,//ASAP
		bias:	0.4,
		random: 0.4,//	0.8,
		sex:	1,
		epilogue: EPILOGUE.wrestler,
	},{
		name : "Policewoman",
		id:		302,
		condit:	0.8,
		bias:	0.4,
		random: 0.4,//	0.8,
		sex:	1,
		epilogue: EPILOGUE.police,
		fallback: 301,
	},{
		name : "Lawyer",
		id:		2001,
		condit:	0.6,
		bias:	-0.5,
		random: 0.3,// 0.8,
		sex: 	0,
		epilogue: EPILOGUE.lawyer
	},{
		name : "Politician",
		id:		2002,
		condit:	0.99, //BOOSTED 
		bias:	-0.4,
		random: 0.3,// 0.8,
		sex: 	0.3,
		epilogue: EPILOGUE.senator,
		fallback: 2001,
	},{
		name : "Mercenary",
		id: 	3001,
		condit: 0.6,
		bias: 	-1,
		random: 0.4,// 0.8,
		sex:	0,
		epilogue: EPILOGUE.mercenary,
	},{
		name : "Policeman",
		id:		3002,
		condit:	0.99,
		bias:	-0.6,
		random: 0.4,//	0.8,
		sex:	0,
		epilogue: EPILOGUE.police,
		fallback: 3001,
	},{
		name : "No Job",
		id: 	1001,
		condit:	0.6,
		bias:	-0.3,
		random: 0.4,//	0.8,
		sex:	0,
		epilogue: EPILOGUE.unemployed,
	},{
		name : "Nerd",
		id: 	1002,
		condit:	0.9,
		bias:	-0.4,
		random: 0.4,//	0.8,
		sex:	0,
		epilogue: EPILOGUE.nerd,
	},{
		name:	"Teacher",
		condit:	0.6,
		id:		1004,
		bias: 	-0.4,
		random: 0.3,// 0.8,
		sex: 	0,
		epilogue: EPILOGUE.teacher,
		fallback: 1001,
	},{
		name : "Intern",
		id: 	9999,
		condit: 0,
		bias:	-0.3,
		random: 0.2,//	0.8,
		sex:	0,
		epilogue: EPILOGUE.intern,
		fallback: 1001
	}
];

*/