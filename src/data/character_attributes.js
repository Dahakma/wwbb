export const att = {
	name: "Anon",
	/*
		the current players official name
	*/
	og_name: "Anon",
	/*
		the name s/he had at the beggining of the game
	*/
	
	sex: 0, 
	/*
		official sex; in official documents & used pronouns 
	*/
	og_sex: 0,
	
	sexuality: 0, 
	/*
		-3 - only attracted to men
		-2 - almost only attracted to men 
		-1 - bisexual, slightly prefers men
		0 - bi
		1 - bi, slightly prefers women
		2 - almost only attracted to women
		3 - only attracted to women 
	*/
	og_sexuality: 0,
	
	archetype: "",
	/*
		id/key of the used archetype/background/job
		see src/data/archetypes.js
	*/
	
	accept: 0,
	/*
		<0 resist being transformed and bimbotized
		>0 enjoys being transformed and bimbotized
	*/
	
	
	money: 0, //TODO - should be attribute or calculated dynamicaly? 
	/*
		finantial status (mostly influenced by prize won in the show)
		-1 - is poor 
		1 - is quite rich
		2 - is very rich
	*/
	
	
//MAIN MENTAL TRANSFORMATIONS	
	sub: 0,
	/*
		submisiveness (mental transformation)
		-1 - dominant
		1 - submissive
		2 - very submisive 
	*/
	
	horny: 0,
	/*
		lust (mental transformation)
		-1 - frigid
		1 - horny
		2 - utter nympho
	*/

	dumb: 0,
	/*
		intelligence (mental transformation)
		-1 - smart
		1 - not smart
		2 - dumb
	*/
	
//MINOR MENTAL TRANSFORMATIONS
	phero: false, 
	milk: false,
	age: false,
	
//MINOR PHYSICAL TRANSFORMATIONS 
	


//BODYPARTS
	vagina: true, //has pussy (i.e. woman, man after genital trasformation, possibly hermaphrodite) 
	penis: false,
	testicles: false,
	
	
	
};

/*

	lips : 0,
			nails: 0,
			eyes : 0,
			
			makeup: false,
			
			pink : 0,
			goth:false,
			
			//name : "",
			//name_changed: false,
			
			milk: false,
			
			phero: false,
			fert: false,
			age: false,
			
			heels : false,
			tan : false,
			hair : 0,
			hairco : {},
			tat : 0,
			jew : 0,
			
			sluty : 0,
			bodysuit : 0,
			
			under : 0,
			femclo : true,
			
			newgirls : false, //pc wasnt into girls but after transformation is
			newboys : false,	
			
			*/