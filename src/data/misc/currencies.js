export const currencies = {
	usd: {
		symbol: "$",
		name: "Dollars",
	},

	eur: {
		symbol: "€",
		name: "Euro",
	},

	cre: {
		symbol: "☼",
		name: "Credits",
	},

	gbp: {
		symbol: "£",
		name: "Pounds",
	},
};


