export const color_schemes = {
	//LIGHT MODE
	light: {
		text:	"#000000", //the default colour of text
		walter:	"hsla(103, 59%, 42%, 1)", //the default colour of Walter's speech, green
		bgr:	"hsla(0, 16%, 94%, 1)", //the main background colour, 90% white with light red tint (#f2eded)
		high:	"hsl(335, 100%, 42%)", //primary, links, highlighted stuff; sharp pink-red (#d60059)
		hoover:	"hsl(342, 100%, 49%)",//background of active links, looks a bit better than using the main font colour (#FB004C;)
	}, 
	
	//DARK MODE
	dark: {
		text: "hsl(12, 25%, 85%)",
		walter: "hsl(103, 59%, 42%)", //Walter stays the same
		bgr: "hsl(0, 10%, 10%)", //the main background colour, nearly black 
		high: "hsl(0, 100%, 50%)", //the main highlight font colour, sharp pink-red
		hoover: "hsl(0, 100%, 50%)",
	},
	
	//BLUE STEEL
	blue_steel: {
		text: "#000000",
		walter: "#20fb00",
		bgr: "#056778",
		high: "#ffffff",
		hoover: "#ffffff",
	}, 
	
	//LOVELY DAY
	lovely_day: {
		text: "#ffffff",
		walter: "#ffffff",
		bgr: "#001b46", 
		high: "#ff7315",
		hoover: "#ff7315",
	},
	
	//EVIL WITCH
	evil_witch: {
		text: "hsl(100, 10%, 90%)",
		walter: "hsl(100, 10%, 90%)",
		bgr: "hsl(100, 7%, 18%)",
		high: "hsl(120, 100%, 50%)",
		hoover: "hsl(120, 100%, 50%)",
	},

	//Sunset Sarsaparilla
	sunset_sarsaparilla: {
			text: "hsla(10, 90%, 20%, 1)", //default color of text //#611405
			high: "hsla(10, 100%, 40%, 1)", //important, buttons, headlines //#c20
			bgr:  "hsla(50, 75%, 75%, 1)", //background (neutral color) //#efdf8f
			//background_2: "hsla(44, 92%, 48%, 1)", //background of the whole page //#ebaf0a
	},
	
};