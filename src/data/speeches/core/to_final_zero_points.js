import {w, pc, txt} from 'System/write';
import {change, rg, ra} from 'System/write';
import {ordinal, number_plural, number, plural} from 'System/write';
import {att, round, session, mode, stats, settings} from 'System/write';


export const wwbb_default = function(){
	w(`You ended with zero points! But since we are so nice, we have an offer for you - we can give you ${2 * settings.style.prize} - in exchange for  ${number_plural(settings.gameplay.final_zero_points_transformations, "transformation")} `);
};



