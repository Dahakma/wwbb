import {w, pc, txt} from 'System/write';
import {change, rg, ra} from 'System/write';
import {ordinal, number_plural, number, plural} from 'System/write';
import {att, round, session, mode, stats, settings} from 'System/write';


function percent(number, base){
	return Math.floor( (number / base) * 100 );
}


export const wwbb_default = function(){
	const rounds = session.round;
	const right = session.right;

	const questions = rounds * mode.questions;
	const sucrate = percent(right, questions)
	const points = session.won_points;
	const money = session.money;
	const moneyrate = percent(money, points);

	function unending(){
		if(sucrate > 90){
			w(`You went through nearly flawless ${rounds} rounds. ${right} of ${questions} questions mean an excellent  ${sucrate}% success rate. `)
		}else if (sucrate > 65){
			w(`You went through ${rounds} with a quite nice success rate ${sucrate}% and  ${right} of ${questions} questions right. `)
		}else if (sucrate > 30){
			w(`Your performance was - I dare to say - quite mediocre. In your ${rounds} rounds you correctly answered correctly ${right} out of ${questions} questions with a success rate   `)
		}else{
			w(`You were not very successful - you answered only ${right} out of ${questions} questions correctly. On the other hand, at least you know your limits. Even with your lousy success rate ${sucrate}% you were able to survive ${rounds} without running out of bimbo transformations. `)
		};
		
	}
	
	function normal(){
		//SHORT GAMES
		if(questions === 1 && right === 1){
			w(`Wow. Only one questions. Well, at least you got it right. `)
		}else if(questions < 6){
			if(sucrate > 49){
				w(`It was rather disappointingly short game with only ${questions} questions but you at least was able to got ${sucrate}% of them right. `)
			}else if(right === 0){
				w(`It was pretty short and embarrassing game: ${questions} questions and none of them was answered correctly. `)
			}else if(questions < 6){
				w(`It was rather disappointingly short game with only ${questions} and ot ${right} of them right. `)
			}
		//GOOD RESULTS 
		}else if( (questions - right) === 0){
			w(`Your game was absolutely flawless. ${questions} questions and all of them were answered correctly! We do not often see such a perfect success. `)
		}else if (questions > 9 && sucrate > 90){
			w(`Your game was nearly flawless. ${questions} questions and only ${questions-right} wrong answers, which means excellent ${sucrate}% success rate. `)
		}else if (sucrate > 85){
			w(`You game was great! You got ${right} question out of ${questions} right which means awesome success rate ${sucrate}%. `)
		}else if (sucrate > 74){
			w(`You game was rather good. You got ${right} question out of ${questions} right which means respectable success rate ${sucrate}%. `)
		}else if (sucrate>56){
			w(`Although our game was not especially great, it was neither especially bad. You got ${right} question out of ${questions} right and we have seen even worse success rates than ${sucrate}%. `)
		
		//BAD RESULTS 
		}else if(right === 0){
			w(`I believe we have a new record. You definitely prove you are true airhead bimbo with zero right answers. I would nearly suspect you were here not for money but to be fully transformed. `)
		}else if (right === 1){
			w(`You game was - and I dare to say it - abysmal. You are maybe the worst contestant we have had on our show. You got only one right answer from ${questions} questions, which means only ${sucrate}% success rate. `)
		}else if (right < 4){
			w(`You game was rather - and I dare to say it - awful. You got only ${right} right answers from ${questions} questions, which means only shameful ${sucrate}% success rate. `)
		}else if (sucrate<40){
			w(`Your game was rather bad. You answered only ${right} question out of ${questions} correctly with  embarrassing success rate ${sucrate}%. `)
		}else{
			w(`Your game was definitely not very good. ${questions} questions and ${right} correct answers, which means rather unimpressive ${sucrate}% success rate. `)
		};
	}


	function final_round(){
		
	//GREAT GAME 
		if( questions === right && moneyrate > 99 ){
			w(`And your unpresidented performance continued even in our Final Round! You completely beated our show and won ${money} @money! Congratulations!`)
		}else if( sucrate > 85 && moneyrate < 1){
			w(`It makes your complete failure in the Final Round even more sad. Even though you won ${points} points you were not able to turn any of them into money. You won nothing. But I think everybody would agree we saw a bold and exciting game and you at least dodged most of the transformations. `) //TODO NOT WITH ALTA
		}else if( sucrate > 85 && moneyrate > 66){
			w(`You were a bit less lucky in the Final Round. However, you still, managed to turn ${points} points into ${money} `);
		}else if( sucrate > 85 && moneyrate > 33){
			w(`It is really a shame you stumbled just before the finish line. You were able to turn only ${money} of your ${points} points to money. You won <i>only</i> ${money} @money! Congratulations. `)
		}else if( sucrate > 85){
			w(`Its such a shame you were not so lucky in the Final Round. You could go home with ${points} @money and it must hurt that you are now leaving only with ${points} @money. Still, better than nothing, congratulations. `)

	//GOOD GAME 	
		}else if ( sucrate > 66 && moneyrate > 99 ){
			w(`Your luck did not run out even in our Final Round where you were able to turn all your ${points} points into money.  You won ${money} @money! Congratulations. `)
		}else if ( sucrate > 66 && moneyrate > 66 ){
			w(`Your luck did not run out even in our Final Round where you were able to turn most of your ${points} points into money.  You won ${money} @money! Congratulations. `)
		}else if( sucrate > 66 && moneyrate < 1 ){
			w(`It is real shame that after such a good game and gaining ${points} points you were not able to turn any of them into money. You are leaving our show with nothing. `)
		}else if ( sucrate > 66 ){
			w(`Sadly your were not so successful in our Final Round. You were able to turn only ${money} of your ${points} points into money.  You won only ${money} @money! Still, congratulations! `)
		 
		
////???
		//BAD GAME & GOOD FINAL 
		}else if( (right < 3 && questions > 10) && moneyrate > 99 ){
			w(`However I am still confused about your perfect success in the Final Round. You turned all your points into ${money} @money! Congratulations! `)
			
		//GOOD FINAL
		}else if ( moneyrate>99 ){
			w(`However you were much more successful in our Final Round where you was able to turn all your ${points} points into ${money} @money! Which means that despite initial troubles you are actually more successful than some contestants who won more points but totally blew in the Final Round. Congratulations. `)
		}else if ( moneyrate>66 ){
			w(`However you were much more successful in our Final Round where you was able to turn most of your ${points} points into ${money} @money! Which means that despite initial troubles you are actually more successful than some contestants who won more points but totally blew in the Final Round. Congratulations. `)
			
		//BAD GAME & BAD FINAL
		}else if (sucrate < 33 && moneyrate < 1){
			w(`Unsurprisingly you were just as bad in the Final Round. Sadly you were not able to turn any of your ${points} points into money and you are leaving our show with nothing. `)
			
		//BAD FINAL
		}else if( moneyrate < 1 ){
			w(`However your Final Round was not very fortunate. You did not turn any of your ${points} points into money and you are leaving our show with nothing. `)
		}else{
			w(`You seriously struggled with our Final Round. Still during the game you gained ${points} points and you were able to turn them into ${money} @money. I believe that is a very nice sum. Congratulations. `)
		};
	
	};


	if(mode.gameplay === "unending"){
		unending();
	}else{
		normal();
	}
	
	final_round();


}


