import {w, pc, txt} from 'System/write';
import {change, rg, ra} from 'System/write';
import {ordinal, number_plural, number, plural} from 'System/write';
import {att, round, session, mode, stats, settings} from 'System/write';


export const initial = {
	condit(){
		return stats.sessions < 3 ? 100 : 1; //TODO 
	},
	text(){
		w(`It is happening! The newest season of the most exciting show on the air is here! `);
		w(`I am Walter Smith, your host, and I hope our show will be fun and enjoyable for both our long-time loyal fans and newcomers watching **Who You Wanna Be A Bimbo?** for the first time. `);
		
		if(mode.gameplay ===  `unending`){
			w(`The rules are fairly simple. There will be rounds consiting of ${mode.questions} questions. ${settings.gameplay.unending_rounds === 1 ? "Each round" : `Each  ${ordinal(settings.gameplay.unending_rounds)}rounds`}  I will ask our contestand if she - or he - wants to continue or whether he or she already won enough points and wants to advance to the final round where could be the points changed into cash. `);
		}else{
			w(`The rules are fairly simple. There will be ${number_plural(mode.rounds, "round")} each consisting of ${number_plural(mode.questions, "question")}. For each correctly answered question our contestant receives @prize points. In the final round, our contestant will try to change his or her points into cash. `);
		}
		
		if(mode.transform === `alta`){
			w(`What makes our game entertaining are the transformation. Our player will be gradualy transformed, right answers allow to chose specific transformations, wrong answer result in randomly selected ones. `);
	
		}else if(mode.transform === `leev`){
			w(`What makes our game original are the transformation. For every wrong answer our player has to chose one of ${number(settings.gameplay.leev_cards)} randomly drawn transformations. `);
					
		}else{
			w(`What makes our game thrilling are the transformation. For every wrong answer our player has to randomly draw a transformation. Each round, ${number_plural(settings.gameplay.exchange_cards, "unwanted transformation")} could be discarded, however, a discarded transformation gets replaced by ${settings.gameplay.exchange_price ? "a new one" : `${number(settings.gameplay.exchange_price)} new ones`}. `);
		}
	},	
};





export const sarc_1 = function(){
	w(`Welcome to **Who You Wanna Be A Bimbo?***, a show where we reward the smart ones and transform the unsuccessful ones into busty bimbos. `);
	w(`Unsuccessful players are transformed in our **Sarcophagus** - a wonderful cutting-edge piece of technology, incorporating recent breakthroughs made by Japanese scientists in fields of neural programming and micromachines, alien technology obtained and reverse-engineered by the United States Space Force and ancient Sumerian magic. `);
	w(``);
	w(`I'm curious how much time will spend in the Sarcophagus our today's contestant:  Welcome to our show`);
};

export const standard_1 = function(){
	w(`Welcome to **Who Wanna Be A Bimbo?*, the most entertaining and lewd show on the air. `);
	w(`Do you watch our show and laugh at dumb people deservedly transformed into bimbos, believing you are way smarter than them? Maybe you are. `) 
	w(`You should definitely apply for the new season. There is nothing to lose. Well, nothing except your body, mind, career, friends and decency, but still, the money is worth the risk, is it not? If you would like to know more, visit our website. `);
	w(``);
	w(`Let's meet a person, who was not afraid to become a player tonight: Welcome to our show`);
};

export const standard_2 = function(){
	w(`Welcome to the greatest *edutainment* show on the air - **Who Wanna Be A Bimbo?**!`);
	w(`Indeed, in our show is more important knowledge than a blind luck. Smart people are rewarded and the dumb ones... well I guess our smart viewers already know the answer. `);
	w(``);
	w(`Let's find out how smart is our today's contestant: Welcome to our show`);
};



/*
	
	
	
SPEECH.start_alta = (function(){ 
	add.w("People often tell me: <i>Walter, I would want to be completely bimbotised but I am afraid to go to your show because I am too smart. </i> Desperate to keep the attention of our viewers, we came up with this new format for more adventurous contestants where are transformations <i>guaranteed</i> and the questions is only whether our players get to pick or will be at the mercy of random chance. ");
	add.w("Please applause for our first bimbo ");
});	

SPEECH.start_unending = (function(){ 
	add.w("People are different. They have different opinions, skill, wishes and desires. Some need only a few bucks, but they do not believe they would be able to survive full "+(con.rounds)+" rounds. Others are bolder (or perhaps more desperate) and they are not afraid to press their luck further. ");
	add.w("<strong>Who Wanna Be A Bimbo?</strong> cannot satisfy everybody, but that does not stop us from trying. And here comes the brand new way to play our game. Now the contestants themselves can decide for how long they want to continue. ");
	//add.write("(this mode is still work in progress)")


	add.w("Let us met the new brave contestant: Welcome to our show ")
});


*/


/*
export const test_3 = {
	condit(){
		return 1;
	},
	text(){
		w( `condition 3`);
	},	
	replies: [
		{
			label: `yes`,
			exec(){
				alert(`yes`);
			},
		},
		{
			label: `no`,
			exec(){
				alert(`no`);
			},
		},
		{
			label: `deep`,
			exec(){
				//
			},
			text(){
				pc(`still herhe`);
			},
			replies: [
				{
					label: `done`,
				},
			],
		},
	],
};
*/