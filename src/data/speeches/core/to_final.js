import {w, pc, txt} from 'System/write';
import {change, rg, ra} from 'System/write';
import {ordinal, number_plural, number, plural} from 'System/write';
import {att, round, session, mode, stats, settings} from 'System/write';


export const wwbb_default = function(){
	w(`During our game you won ${session.points} points. In the Final Round, you will have up to ${settings.gameplay.final_questions} attempts to turn them into money. `);
};



