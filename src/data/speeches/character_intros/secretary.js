import {w, pc, txt} from 'System/write';
import {att} from 'System/write';

export const secretary = {
	condit(){
		return att.archetype === "secretary";
	},
	text(){
		pc(`I'm a personal assistant. `);
		w(`So a secretary? Are you here to fulfill the stereotype about busty bimbo secretary, always *eager* to *please* her boss?`);	
	},
	replies: [
		{
			label: `Well, a little bigger breasts might finally get me the promotion... `, 
			accept: 1,
		},
		{
			label: `Nope, if I win I will quit my job and spend the rest of my days sunbathing somewhere in the Caribbean. `, 
		},
		{
			label: `Excuse me! I'm highly-qualified PA, not some kind of useless bimbo, and I'm here to win a lot of money!  `, 
			accept: -1,
		},
	],
};


