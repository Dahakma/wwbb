import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

import path from "path";
import eslint from 'vite-plugin-eslint'
import htmlTemplate from 'rollup-plugin-generate-html-template';

	
export default defineConfig({ //https://vitejs.dev/config/
	base: '', //https://stackoverflow.com/questions/69744253/vite-build-always-using-static-paths
	
	define: { 
        __VERSION__: JSON.stringify(process.env.npm_package_version), //takes version number from package.json (accessed window.__VERSION__)  //https://stackoverflow.com/questions/67194082/how-can-i-display-the-current-app-version-from-package-json-to-the-user-using-vi
    },

	resolve:{
		alias:{
			'Root' : path.resolve(__dirname, './'), 
			'Src' : path.resolve(__dirname, './src'),
			'Data' : path.resolve(__dirname, './src/data'),
			'System' : path.resolve(__dirname, './src/system'),
			'Libraries' : path.resolve(__dirname, './src/libraries'),
			'Gui' : path.resolve(__dirname, './src/gui'),
			'Comp' : path.resolve(__dirname, './src/gui/components'),
			'Icons' : path.resolve(__dirname, './src/gui/icons'),
			'Style' : path.resolve(__dirname, './src/gui/style'),
			
			
			'Game' : path.resolve(__dirname, './src/system/game/game'),
			'Variables' : path.resolve(__dirname, './src/variables'),
			'Settings' : path.resolve(__dirname, './src/system/settings'),
			'Write' : path.resolve(__dirname, './src/system/write/index'),
			'Dh' : path.resolve(__dirname, './src/libraries/dh/index'),
			'Random' : path.resolve(__dirname, './src/libraries/random/index'),
			'Questions' : path.resolve(__dirname, './src/data/questions.json'), //json with questions
			'Tags' : path.resolve(__dirname, './src/data/tags.json'), 
		},
	  },
  
	server: {
		open: '/index.html'
	},
	

	build: {
		rollupOptions: {
			output: {
				format: "iife", //could be run without server
			},
			input: '/src/index.js', //this disables the default creation of index.html
			plugins: [
				htmlTemplate({ //creates index.html and puts the link to the script after the "body" of html instead into the "head"
					template: 'index.html',
					target: 'index.html',
				}),
			],
		},
	},

	plugins: [
		{
			...eslint({
				fix: false,
				exclude:  [
					//`src/libraries/*.js`,  - in eslint config
					`src/**/*.css`,
					 `src/**/*.svelte`, //TODO - SVELTE LINTING!!
				],
			}),
			//enforce: 'pre',
			//apply: 'build',
		},
		svelte(),
	]
})