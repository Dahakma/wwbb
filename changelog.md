# Changelog

## 0.9.0
- everything reworked
- settings reorganized
- new tools to display and analyze questions
- removed normal start; only the advanced start left
- *CONTINUE* menu button can continue at any point of the game, even after reload
